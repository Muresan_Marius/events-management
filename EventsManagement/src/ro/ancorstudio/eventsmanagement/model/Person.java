package ro.ancorstudio.eventsmanagement.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ro.ancorstudio.eventsmanagement.ModelConstants;

/**
 * This class will store data about a person.
 * 
 * @author Muresan Marius
 *
 */
public class Person implements Comparable<Person>, Serializable
{

	private static final long	serialVersionUID	= 1L;
	private long				_id					= ModelConstants.NEW_PERSON_ID;
	private String				_firstName;
	private String				_lastName;
	private String				_locality;
	private String 				_address;
	private String 				_cnp;
	private String 				_series;
	private String				_email;
	private String				_phone;
	private String				_sex;
	protected Date				_birthDate;
	protected String			_birthDateStr;
	private Date				_inregistrationDate;
	private String				_inregistrationDateStr;
	private String				_generalObservations;


	/**
	 * @return the _id
	 */
	public long getId() {
		return _id;
	}

	/**
	 * @param _id the _id to set
	 */
	public void setId(long id) {
		this._id = id;
	}

	/**
	 * @return the _firstName
	 */
	public String getFirstName() {
		return _firstName;
	}

	/**
	 * @param _firstName the _firstName to set
	 */
	public void setFirstName(String firstName) {
		this._firstName = firstName;
	}

	/**
	 * @return the _lastName
	 */
	public String getLastName() {
		return _lastName;
	}

	/**
	 * @param _lastName the _lastName to set
	 */
	public void setLastName(String lastName) {
		this._lastName = lastName;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate()
	{
		return _birthDate;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate( Date birthDate )
	{
		_birthDate = birthDate;
	}

	/**
	 * @return the birthDateStr
	 */
	public String getBirthDateStr()
	{
		return _birthDateStr;
	}
	/**
	 * @return the _email
	 */
	public String getEmail() {
		return _email;
	}

	/**
	 * @param _email the _email to set
	 */
	public void setEmail(String email) {
		this._email = email;
	}

	/**
	 * @return the _phone
	 */
	public String getPhone() {
		return _phone;
	}

	/**
	 * @param _phone the _phone to set
	 */
	public void setPhone(String phone) {
		this._phone = phone;
	}

	/**
	 * @return the _sex
	 */
	public String getSex() {
		return _sex;
	}

	/**
	 * @param _sex the _sex to set
	 */
	public void setSex(String sex) {
		this._sex = sex;
	}
	
	/**
	 * @param birthDateStr the birthDateStr to set
	 */
	public void setBirthDateStr( String birthDateStr )
	{
		_birthDateStr = birthDateStr;
	}
	
	/**
	 * @return the _generalObservations
	 */
	public String getGeneralObservations() {
		return _generalObservations;
	}

	/**
	 * @param _generalObservations the _generalObservations to set
	 */
	public void setGeneralObservations(String generalObservations) {
		this._generalObservations = generalObservations;
	}

	/**
	 * @return the _address
	 */
	public String getAddress() {
		return _address;
	}

	/**
	 * @param _address the _address to set
	 */
	public void setAddress(String address) {
		this._address = address;
	}


	/**
	 * @return the _locality
	 */
	public String getLocality() {
		return _locality;
	}

	/**
	 * @param _locality the _locality to set
	 */
	public void setLocality(String locality) {
		this._locality = locality;
	}
	/**
	 * @return the _cnp
	 */
	public String getCnp() {
		return _cnp;
	}

	/**
	 * @param _cnp the _cnp to set
	 */
	public void setCnp(String cnp) {
		this._cnp = cnp;
	}

	/**
	 * @return the _seriesNr_ci
	 */
	public String getSeries() {
		return _series;
	}

	/**
	 * @param _seriesNr_ci the _seriesNr_ci to set
	 */
	public void setSeries(String series) {
		this._series = series;
	}


	/**
	 * @return the _inregistrationDate
	 */
	public Date getInregistrationDate() {
		return _inregistrationDate;
	}

	/**
	 * @param _inregistrationDate the _inregistrationDate to set
	 */
	public void setInregistrationDate(Date inregistrationDate) {
		this._inregistrationDate = inregistrationDate;
	}

	/**
	 * @return the _inregistrationDateStr
	 */
	public String getInregistrationDateStr() {
		return _inregistrationDateStr;
	}

	/**
	 * @param _inregistrationDateStr the _inregistrationDateStr to set
	 */
	public void setInregistrationDateStr(String inregistrationDateStr) {
		this._inregistrationDateStr = inregistrationDateStr;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((_age == null) ? 0 : _age.hashCode());
		result = prime * result + ((_firstName == null) ? 0 : _firstName.hashCode());
		result = prime * result + ((_lastName == null) ? 0 : _lastName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		Person other = (Person) obj;
		/*if (_age == null) {
			if (other._age != null)
				return false;
		} else if (!_age.equals(other._age))
			return false;*/
		if ( _firstName == null )
		{
			if ( other._firstName != null )
				return false;
		}
		else if ( !_firstName.equals( other._firstName ) )
			return false;
		if ( _lastName == null )
		{
			if ( other._lastName != null )
				return false;
		}
		else if ( !_lastName.equals( other._lastName ) )
			return false;
		return true;
	}

	@Override
	public int compareTo( Person pers )
	{
		if ( this._lastName == null || pers._lastName == null )
		{
			return 0;
		}

		int result = this._lastName.compareTo( pers._lastName );
		if ( result == 0 )
		{
			if ( this._firstName == null || pers._firstName == null )
			{
				return 0;
			}
			result = this._firstName.compareTo( pers._firstName );
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Person [_lastName=" + _lastName + ", _firstName=" + _firstName + ", _phone=" + _phone + ", _email=" + _email + ", _birthDate=" + _birthDate + ", "+
				 " _sex=" + _sex + ", _generalObservations" + _generalObservations + ", _address=" + _address +", _locality=" + _locality + ", _cnp=" + _cnp + ", "+
				    " _series=" + _series +", _inregistrationDate=" + _inregistrationDate + "]";
	}

}
