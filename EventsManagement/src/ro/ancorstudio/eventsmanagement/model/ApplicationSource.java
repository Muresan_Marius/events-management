package ro.ancorstudio.eventsmanagement.model;


/**
 * This class will store data about the application sources 
 * @author Muresan Marius
 * @version 1.0
 */

public class ApplicationSource
{

	private long	_id;
	private String	_name;

	/**
	 * @return the id
	 */
	public long getId()
	{
		return _id;
	}

	/**
	 * @param locationId the id to set
	 */
	public void setId( long id )
	{
		_id = id;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return _name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName( String name )
	{
		_name = name;
	}
}
