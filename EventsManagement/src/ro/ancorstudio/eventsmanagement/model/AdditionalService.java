package ro.ancorstudio.eventsmanagement.model;

import java.io.Serializable;

/**
 * @author Muresan Marius
 * @version 1.0
 */

public class AdditionalService implements Serializable, Comparable<AdditionalService>
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private long				_id;
	private String				_description;
	private int					_price;

	/**
	 * @return the id
	 */
	public long getId()
	{
		return _id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId( long id )
	{
		_id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return _description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription( String description )
	{
		_description = description;
	}

	public void setPrice( int price )
	{
		this._price = price;
	}

	public int getPrice()
	{
		return _price;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (_id ^ (_id >>> 32));
		return result;
	}

	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		AdditionalService other = (AdditionalService) obj;
		if ( _id != other._id )
			return false;
		return true;
	}

	@Override
	public int compareTo( AdditionalService other )
	{
		if ( this._price < other._price )
		{
			return -1;
		}
		else if ( this._price > other._price )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}
