package ro.ancorstudio.eventsmanagement.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.directwebremoting.annotations.DataTransferObject;

import ro.ancorstudio.eventsmanagement.ModelConstants;

/**
 * This class will store data about an event.
 * 
 * @author Muresan Marius
 *
 */
@DataTransferObject
public class Event implements Serializable
{
	private static final long		serialVersionUID	= 1L;

	private long					_id			= ModelConstants.NEW_ID;
	private int 					_eventId;
	private int 					_descriptionId;
	private String 					_description;
	private Date 					_eventDate;
	private String 					_eventDateStr;
	private int						_appliedFromId;
	private String					_appliedFromName;
	private String 					_eventLocation;
	private Position 				_position;
	private int 					_positionId;
	private Set<AdditionalService> 	_additionalServices = new HashSet<AdditionalService>();
	private Set<Integer> 			_additionalServicesIds = new HashSet<Integer>();
	private String 					_hd;
	private String 					_formatVideo;
	private Set<User> 				_users 				= new HashSet<User>();
	private String 					_advance;
	private String 					_price;
	private String 					_details;
	private Status 					_status;
	private String 					_observations;
	private String 					_delivered;

	/**
	 * @return the _id
	 */
	public long getId() {
		return _id;
	}
	/**
	 * @param _id the _id to set
	 */
	public void setId(long id) {
		this._id = id;
	}
	/**
	 * @return the _appliedFromId
	 */
	public int getAppliedFromId() {
		return _appliedFromId;
	}
	/**
	 * @param _appliedFromId the _appliedFromId to set
	 */
	public void setAppliedFromId(int appliedFromId) {
		this._appliedFromId = appliedFromId;
	}
	/**
	 * @return the _appliedFromName
	 */
	public String getAppliedFromName() {
		return _appliedFromName;
	}
	/**
	 * @param _appliedFromName the _appliedFromName to set
	 */
	public void setAppliedFromName(String appliedFromName) {
		this._appliedFromName = appliedFromName;
	}
	/**
	 * @return the _eventId
	 */
	public int getEventId() {
		return _eventId;
	}
	/**
	 * @param _eventId the _eventId to set
	 */
	public void setEventId(int _eventId) {
		this._eventId = _eventId;
	}
	/**
	 * @return the _typeId
	 */
	public int getDescriptionId() {
		return _descriptionId;
	}
	/**
	 * @param i the _typeId to set
	 */
	public void setDescriptionId(int i) {
		this._descriptionId = i;
	}
	/**
	 * @return the _typeDescription
	 */
	public String getDescription() {
		return _description;
	}
	/**
	 * @param _typeDescription the _typeDescription to set
	 */
	public void setDescription(String description) {
		this._description = description;
	}
	/**
	 * @return the date
	 */
	public Date getEventDate() {
		return _eventDate;
	}
	/**
	 * @param date the date to set
	 */
	public void setEventDate(Date eventDate) {
		this._eventDate = eventDate;
	}
	/**
	 * @return the _eventDateStr
	 */
	public String getEventDateStr() {
		return _eventDateStr;
	}
	/**
	 * @param _eventDateStr the _eventDateStr to set
	 */
	public void setEventDateStr(String eventDateStr) {
		this._eventDateStr = eventDateStr;
	}
	/**
	 * @return the _eventLocation
	 */
	public String getEventLocation() {
		return _eventLocation;
	}
	/**
	 * @param _eventLocation the _eventLocation to set
	 */
	public void setEventLocation(String eventLocation) {
		this._eventLocation = eventLocation;
	}
	/**
	 * @return the package
	 */
	public Position getPosition() {
		return _position;
	}
	/**
	 * @param pachet the pachet to set
	 */
	public void setPosition(Position position) {
		this._position = position;
	}
	/**
	 * @return the additionalServices
	 */
	public Set<AdditionalService> getAdditionalServices() {
		return _additionalServices;
	}
	/**
	 * @param additionalServices the additionalServices to set
	 */
	public void setAdditionalServices(Set<AdditionalService> additionalServices) {
		this._additionalServices = additionalServices;
	}
	/**
	 * @return the hd
	 */
	public String getHd() {
		return _hd;
	}
	/**
	 * @param hd the hd to set
	 */
	public void setHd(String hd) {
		this._hd = hd;
	}
	/**
	 * @return the formatVideo
	 */
	public String getFormatVideo() {
		return _formatVideo;
	}
	/**
	 * @param formatVideo the formatVideo to set
	 */
	public void setFormatVideo(String formatVideo) {
		this._formatVideo = formatVideo;
	}
	/**
	 * @return the users
	 */
	public Set<User> getUsers() {
		return _users;
	}
	/**
	 * @param users the users to set
	 */
	public void setUsers(Set<User> users) {
		this._users = users;
	}
	/**
	 * @return the advance
	 */
	public String getAdvance() {
		return _advance;
	}
	/**
	 * @param advance the advance to set
	 */
	public void setAdvance(String advance) {
		this._advance = advance;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return _price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this._price = price;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return _details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this._details = details;
	}
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return _status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this._status = status;
	}
	/**
	 * @return the observations
	 */
	public String getObservations() {
		return _observations;
	}
	/**
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		this._observations = observations;
	}
	
	/**
	 * @return the _positionId
	 */
	public int get_positionId() {
		return _positionId;
	}
	/**
	 * @param _positionId the _positionId to set
	 */
	public void set_positionId(int _positionId) {
		this._positionId = _positionId;
	}
	
	/**
	 * @return the _additionalServicesIds
	 */
	public Set<Integer> getAdditionalServicesIds() {
		return _additionalServicesIds;
	}
	/**
	 * @param _additionalServicesIds the _additionalServicesIds to set
	 */
	public void setAdditionalServicesIds(Set<Integer> additionalServicesIds) {
		this._additionalServicesIds = additionalServicesIds;
	}
	
	/**
	 * @return the _usersIds
	 */
	public Set<Integer> get_usersIds() {
		return _usersIds;
	}
	/**
	 * @param _usersIds the _usersIds to set
	 */
	public void set_usersIds(Set<Integer> _usersIds) {
		this._usersIds = _usersIds;
	}
	private Set<Integer> 				_usersIds 				= new HashSet<Integer>();
	
	/**
	 * @return the delivered
	 */
	public String getDelivered() {
		return _delivered;
	}
	/**
	 * @param delivered the delivered to set
	 */
	public void setDelivered(String delivered) {
		this._delivered = delivered;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
