package ro.ancorstudio.eventsmanagement.model;

import org.directwebremoting.annotations.DataTransferObject;

/**
 * @author Muresan Marius
 *
 */
@DataTransferObject
public class PersonExtendedRow extends Person
{
	private static final long	serialVersionUID	= 1L;

	private Event			_event;

	/**
	 * @param position the position to set
	 */
	public void setEvent( Event event )
	{
		_event = event;
	}

	/**
	 * @return the position
	 */
	public Event getEvent()
	{
		return _event;
	}

}
