package ro.ancorstudio.eventsmanagement.model;

/**
 * @author Muresan Marius
 * @version 1.0
 * 
 * This class maps table USERS from database
 * 
 */

public class User
{

	private long	_userID;
	private String	_firstName;
	private String	_lastName;
	private String	_username;
	private String	_password;
	//2 = read only, 1 = full rights
	private int		_userType;
	private String	_lastUpdate;

	//Default Constructor
	public User()
	{
	}

	//Constructor
	public User( long userID, String firstName, String lastName, String username, String password, int userType, String lastUpdate )
	{
		this._userID = userID;
		this._firstName = firstName;
		this._lastName = lastName;
		this._username = username;
		this._userType = userType;
		this._password = password;
		this._lastUpdate = lastUpdate;
	}

	//Setters and getters

	/**
	 * @return the _userID
	 */
	public long getUserID()
	{
		return _userID;
	}

	/**
	 * @param _userID the _userID to set
	 */
	public void setUserID( long userID )
	{
		this._userID = userID;
	}

	/**
	 * @return the _firstName
	 */
	public String getFirstName()
	{
		return _firstName;
	}

	/**
	 * @param _firstName the _firstName to set
	 */
	public void setFirstName( String firstName )
	{
		this._firstName = firstName;
	}

	/**
	 * @return the _lastName
	 */
	public String getLastName()
	{
		return _lastName;
	}

	/**
	 * @param _lastName the _lastName to set
	 */
	public void setLastName( String lastName )
	{
		this._lastName = lastName;
	}

	/**
	 * @return the _username
	 */
	public String getUsername()
	{
		return _username;
	}

	/**
	 * @param _username the _username to set
	 */
	public void setUsername( String username )
	{
		this._username = username;
	}

	/**
	 * @return the _password
	 */
	public String getPassword()
	{
		return _password;
	}

	/**
	 * @param _username the _username to set
	 */
	public void setPassword( String password )
	{
		this._password = password;
	}

	/**
	 * @return the user_type
	 */
	public int getUserType()
	{
		return _userType;
	}

	/**
	 * @param user_type the user_type to set
	 */
	public void setUserType( int userType )
	{
		this._userType = userType;
	}

	/**
	 * @return the _date
	 */
	public String getLastUpdate()
	{
		return _lastUpdate;
	}

	/**
	 * @param _date the _date to set
	 */
	public void setLastUpdate( String lastUpdate )
	{
		this._lastUpdate = lastUpdate;
	}

}
