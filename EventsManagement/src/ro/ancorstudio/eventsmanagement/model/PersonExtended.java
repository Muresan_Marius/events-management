package ro.ancorstudio.eventsmanagement.model;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 
 * @author Muresan Marius
 *
 */

public class PersonExtended extends Person
{
	private static final long		serialVersionUID	= 1L;
	private Set<Event> 				_events				= new HashSet<Event>();
	
	/**
	 * @return the _events
	 */
	public Set<Event> getEvents() {
		return _events;
	}

	/**
	 * @param _events the _events to set
	 */
	public void setEvents(Set<Event> events) {
		this._events = events;
	}

}
