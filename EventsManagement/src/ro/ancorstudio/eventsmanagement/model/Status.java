package ro.ancorstudio.eventsmanagement.model;

/**
 * This class will store data about the status of the event, ex:delivered,in progress,etc.
 * @author Muresan Marius
 * @version 1.0
 */

public class Status {
	
	private long	_id;
	private String	_description;
	
	/**
	 * @return the id
	 */
	public long getId()
	{
		return _id;
	}

	/**
	 * @param locationId the id to set
	 */
	public void setId( long id )
	{
		_id = id;
	}

	/**
	 * @return the name
	 */
	public String getDescription()
	{
		return _description;
	}

	/**
	 * @param name the name to set
	 */
	public void setDescription( String description )
	{
		_description = description;
	}
}
