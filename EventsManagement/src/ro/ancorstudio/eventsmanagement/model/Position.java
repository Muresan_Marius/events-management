package ro.ancorstudio.eventsmanagement.model;

import java.util.Date;

import org.directwebremoting.annotations.DataTransferObject;

/**
 * This class stores data about a service package
 * @author Muresan Marius
 * @version 1.0
 *
 */

@DataTransferObject
public class Position
{
	private long	_id;
	private String	_name;
	private String	_description;
	private String	_price;
	
	public long getId() {
		return _id;
	}
	public void setId(long _id) {
		this._id = _id;
	}
	public String getName() {
		return _name;
	}
	public void setName(String _name) {
		this._name = _name;
	}
	public String getDescription() {
		return _description;
	}
	public void setDescription(String _description) {
		this._description = _description;
	}
	public String getPrice() {
		return _price;
	}
	public void setPrice(String _price) {
		this._price = _price;
	}



}
