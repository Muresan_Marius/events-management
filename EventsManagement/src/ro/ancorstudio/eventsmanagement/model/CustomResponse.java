package ro.ancorstudio.eventsmanagement.model;

import java.util.List;

/**
* A simple POJO that maps to the JSON structure of a JqGrid.

*
*/

public class CustomResponse<T>
{
	/**
	* Current page of the query
	*/
	private String	page;

	/**
	* Total pages for the query
	*/
	private String	total;

	/**
	* Total number of records for the query
	*/
	private String	records;

	/**
	* Return name of excel file that was created
	*/
	private String	export;

	/**
	* An array that contains the actual objects
	*/
	private List<T>	rows;

	public CustomResponse()
	{
	}

	/**
	 * @return the page
	 */
	public String getPage()
	{
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage( String page )
	{
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public String getTotal()
	{
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal( String total )
	{
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public String getRecords()
	{
		return records;
	}

	/**
	 * @param records the records to set
	 */
	public void setRecords( String records )
	{
		this.records = records;
	}

	/**
	 * @param export the export to set
	 */
	public void setExport( String export )
	{
		this.export = export;
	}

	/**
	 * @return the export
	 */
	public String getExport()
	{
		return export;
	}

	/**
	 * @return the rows
	 */
	public List<T> getRows()
	{
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows( List<T> rows )
	{
		this.rows = rows;
	}

}
