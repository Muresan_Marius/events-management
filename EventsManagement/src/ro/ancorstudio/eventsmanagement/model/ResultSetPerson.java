package ro.ancorstudio.eventsmanagement.model;

import java.util.Date;

/**
 * This class will store data about a person.
 * It is mapping a row from multiple tables join 
 * a person have multiple rows.
 * 
 * @author Muresan Marius
 *
 */

public class ResultSetPerson
{
	private long	_id;
	private String	_firstName;
	private String	_lastName;
	private String	_phone;

	private String	_eventDescription;
	private String	_positionName;

	private Date	_eventDate;
	private String	_eventDateStr;
	private String	_location;
	private Integer	_index;

	/**
	 * @return the id
	 */
	public long getId()
	{
		return _id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId( long id )
	{
		_id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return _firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName( String firstName )
	{
		_firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return _lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName( String lastName )
	{
		_lastName = lastName;
	}

	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return _phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone( String phone )
	{
		_phone = phone;
	}

	/**
	 * @return the _eventDescription
	 */
	public String getEventDescription() {
		return _eventDescription;
	}

	/**
	 * @param _eventDescription the _eventDescription to set
	 */
	public void setEventDescription(String eventDescription) {
		this._eventDescription = eventDescription;
	}

	/**
	 * @return the _positionName
	 */
	public String getPositionName() {
		return _positionName;
	}

	/**
	 * @param _positionName the _positionName to set
	 */
	public void setPositionName(String positionName) {
		this._positionName = positionName;
	}

	/**
	 * @return the _eventDate
	 */
	public Date getEventDate() {
		return _eventDate;
	}

	/**
	 * @param _eventDate the _eventDate to set
	 */
	public void setEventDate(Date eventDate) {
		this._eventDate = eventDate;
	}

	/**
	 * @return the _eventDateStr
	 */
	public String getEventDateStr() {
		return _eventDateStr;
	}

	/**
	 * @param _eventDateStr the _eventDateStr to set
	 */
	public void setEventDateStr(String eventDateStr) {
		this._eventDateStr = eventDateStr;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return _location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this._location = location;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex( Integer index )
	{
		_index = index;
	}

	/**
	 * @return the index
	 */
	public Integer getIndex()
	{
		return _index;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (_id ^ (_id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		ResultSetPerson other = (ResultSetPerson) obj;
		if ( _id != other._id )
			return false;
		return true;
	}

}
