package ro.ancorstudio.eventsmanagement.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import ro.ancorstudio.eventsmanagement.service.dao.intf.EventManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.UserManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.intf.DiskStorageManager;
import ro.ancorstudio.eventsmanagement.service.dao.intf.AdditionalServiceManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.PersonExtendedManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.ApplicationSourceManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.PersonManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.CvManagerDaoIntf;
//import ro.ancorstudio.eventsmanagement.service.PositionsService;
import ro.ancorstudio.eventsmanagement.service.dao.intf.PositionManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.intf.ResultSetPersonManagerDaoIntf;

/**
 * 
 * 
 * Class for storing managers to access MySQL database. The class is singleton
 * and the managers are Autowired by Spring It can be used like that:
 * Managers.getInstance().getUserManager() .getUserGroups(userID) to obtain the
 * list of groups the user belong to
 * 
 * @author Marius Muresan
 * 
 */

@Component
public class Managers implements  ResourceLoaderAware
{
	public static final String				CLASSPATH			= "classpath:";
	public static final String				PROJECT_PROPERTIES	= "projectconfig.properties";
	// logger for this class
	private static Logger					_log				= LoggerFactory.getLogger( Managers.class.getName() );

	// store the instance of this class
	private static Managers					_managers			= null;
	
	// person manager - loads (and works with) Person objects from database
	private PersonManagerDaoIntf			_personManager;

	// Position Manager - loads Location objects from database
	private AdditionalServiceManagerDaoIntf			_additionalServiceManager;
	
	// person manager - loads (and works with) Person objects from database joined with 
	// all tables in which person_id is foreign key
	private PersonExtendedManagerDaoIntf	_personExtendedManager;
	
	// ApplicationSource Manager - loads ApplicationSource objects from database
	private ApplicationSourceManagerDaoIntf	_applicationSourceManager;

	// User manager - loads User objects from database
	private UserManagerDaoIntf				_userManager;
	
	// Domain Manager - loads ResultSetPerson objects from database
	private ResultSetPersonManagerDaoIntf	_resultSetPersonManager;

	private static ResourceLoader			_resourceLoader;
	
	// Event Manager - loads Event objects from database
	private EventManagerDaoIntf			_eventManager;
	
	// Error Manager - loads Error objects from database
	private CvManagerDaoIntf				_cvManager;
	
	private PositionsService				_positionsService	= null;
	
	// Position Manager - loads Location objects from database
	private PositionManagerDaoIntf			_positionManager;
	

	// a protected constructor. No one but Spring can instantiate this class
	// outside the class or children
	protected Managers()
	{
		_managers = this;
	}

	/**
	 * Gets the instance of this class
	 * 
	 * @return the instance of Managers class
	 */
	public static Managers getInstance()
	{
		if ( _managers == null )
		{
			_log.info( "About to instantiate managers!" );
			_managers = new Managers();
		}
		return _managers;
	}

	@Autowired
	public void setPersonManagerDaoIntf( PersonManagerDaoIntf personManager )
	{
		_log.trace( "About to set PersonManagerDaoIntf / Impl!" );
		this._personManager = personManager;
	}

	public PersonManagerDaoIntf getPersonManagerDaoIntf()
	{
		return this._personManager;
	}
	
	@Autowired
	public void setPersonExtendedManagerDaoIntf( PersonExtendedManagerDaoIntf personExtendedManager )
	{
		_log.trace( "About to set PersonExtendedManagerDaoIntf / Impl!" );
		this._personExtendedManager = personExtendedManager;
	}

	public PersonExtendedManagerDaoIntf getPersonExtendedManagerDaoIntf()
	{
		return this._personExtendedManager;
	}
	
	/**
	 * @return the eventManager
	 */
	public EventManagerDaoIntf getEventManagerDaoIntf()
	{
		return _eventManager;
	}

	/**
	 * @param positionManager the positionManager to set
	 */
	@Autowired
	public void setEventManagerDaoIntf( EventManagerDaoIntf eventManager )
	{
		_log.trace( "About to set PositionManagerDaoIntf / Impl!" );
		_eventManager = eventManager;
	}
	
	@Autowired
	public void setUserManagerDaoIntf( UserManagerDaoIntf userManager )
	{
		_log.trace( "About to set UserManagerDaoIntf / Impl!" );
		this._userManager = userManager;
	}

	public UserManagerDaoIntf getUserManagerDaoIntf()
	{
		return this._userManager;
	}

	/**
	 * @return the applicationSourceManager
	 */
	public ApplicationSourceManagerDaoIntf getApplicationSourceManagerDaoIntf()
	{
		return _applicationSourceManager;
	}

	/**
	 * @param applicationSourceManager the applicationSourceManager to set
	 */
	@Autowired
	public void setApplicationSourceManagerDaoIntf( ApplicationSourceManagerDaoIntf applicationSourceManager )
	{
		_log.trace( "About to set ApplicationSourceManagerDaoIntf / Impl!" );
		_applicationSourceManager = applicationSourceManager;
	}
	
	/**
	 * @param cvManager the cvManager to set
	 */
	@Autowired
	public void setCvManagerDaoIntf( CvManagerDaoIntf cvManager )
	{
		_log.trace( "About to set CvManagerDaoIntf / Impl!" );
		_cvManager = cvManager;
	}

	/**
	 * @return the cvManager
	 */
	public CvManagerDaoIntf getCvManagerDaoIntf()
	{
		return _cvManager;
	}
	
	@Autowired
	public void setPositionsService( PositionsService positionsService )
	{
		_log.trace( "About to set PositionsService / Impl!" );
		this._positionsService = positionsService;
	}

	public PositionsService getPositionsService()
	{
		return this._positionsService;
	}
	
	/**
	 * @return the positionManager
	 */
	public PositionManagerDaoIntf getPositionManagerDaoIntf()
	{
		return _positionManager;
	}

	/**
	 * @param positionManager the positionManager to set
	 */
	@Autowired
	public void setPositionManagerDaoIntf( PositionManagerDaoIntf positionManager )
	{
		_log.trace( "About to set PositionManagerDaoIntf / Impl!" );
		_positionManager = positionManager;
	}
	
	/**
	 * @return the ResultSetPersonManager
	 */
	public ResultSetPersonManagerDaoIntf getResultSetPersonManagerDaoIntf()
	{
		return _resultSetPersonManager;
	}

	/**
	 * @param technologyManager the technologyManager to set
	 */
	@Autowired
	public void setResultSetPersonManagerDaoIntf( ResultSetPersonManagerDaoIntf ResultSetPersonManager )
	{
		_log.trace( "About to set ResultSetPersonManagerDaoIntf / Impl!" );
		_resultSetPersonManager = ResultSetPersonManager;
	}
	
	/**
	 * @return the _additionalServiceManager
	 */
	public AdditionalServiceManagerDaoIntf getAdditionalServiceManager() {
		return _additionalServiceManager;
	}

	/**
	 * @param _additionalServiceManager the _additionalServiceManager to set
	 */
	@Autowired
	public void setAdditionalServiceManager( AdditionalServiceManagerDaoIntf additionalServiceManager) {
		this._additionalServiceManager = additionalServiceManager;
	}
	
	/**
	  * Loads a specifiv property from the usual config file
	  * @param propertyName
	  * @return property value
	  */
	public String loadProperty( String propertyName )
	{
		String result = "";
		Properties properties = new Properties();
		InputStream inputStream = null;
		try
		{
			Resource resource = _resourceLoader.getResource( CLASSPATH + PROJECT_PROPERTIES );
			inputStream = resource.getInputStream();
			properties.load( inputStream );

			result = properties.getProperty( propertyName );
			if ( result != null )
			{
				result = result.trim();
			}
			else
			{
				//never returns null
				result = "";
			}
		}
		catch ( Exception e )
		{
			_log.error( "Couldn't load values from properties! Exiting... (Sorry :) )" );
		}
		finally
		{
			if ( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch ( IOException e )
				{
				}
			}
		}
		return result;
	}

	/**
	  * Loads a specifiv property from the usual propertiesFileInClasspath config file
	  * @param propertyName
	  * @return property value
	  */
	public String loadProperty( String propertyName, String propertiesFileInClasspath )
	{
		String result = "";
		Properties properties = new Properties();
		InputStream inputStream = null;
		try
		{
			Resource resource = _resourceLoader.getResource( CLASSPATH + propertiesFileInClasspath );
			inputStream = resource.getInputStream();
			properties.load( inputStream );

			result = properties.getProperty( propertyName );
			if ( result != null )
			{
				result = result.trim();
			}
			else
			{
				//never returns null
				result = "";
			}
		}
		catch ( Exception e )
		{
			_log.error( "Couldn't load values from properties! Exiting... (Sorry :) )" );
		}
		finally
		{
			if ( inputStream != null )
			{
				try
				{
					inputStream.close();
				}
				catch ( IOException e )
				{
				}
			}
		}
		return result;
	}

	@Override
	public void setResourceLoader( ResourceLoader resourceLoader )
	{
		if ( resourceLoader != null )
		{
			_resourceLoader = resourceLoader;
		}

	}

}
