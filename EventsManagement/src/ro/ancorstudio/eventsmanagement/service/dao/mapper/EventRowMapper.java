package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.EventRow;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

public class EventRowMapper implements ParameterizedRowMapper<EventRow>
{

	@Override
	public EventRow mapRow( ResultSet rs, int numRows )
			throws SQLException
	{
		EventRow result = new EventRow();

		try{
			result.setEventDate(rs.getDate( SqlNames.SQL_EVENT_DATE ) ); 
			result.setAppliedFromId( rs.getInt( SqlNames.SQL_EVENT_APPLIED_FROM_ID ) );
			result.setEventId( rs.getInt( SqlNames.SQL_EVENT_ID ) );
			result.setId( rs.getInt( SqlNames.SQL_PERSONS_EVENTS_ID ) );
			result.setDescription( rs.getString( SqlNames.SQL_EVENT_DESCRIPTION ) );
		}catch(Exception e){
			
		}
		return result;
	}

}
