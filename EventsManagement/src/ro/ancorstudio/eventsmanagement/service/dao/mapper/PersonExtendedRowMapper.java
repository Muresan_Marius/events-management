package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.PersonExtendedRow;
import ro.ancorstudio.eventsmanagement.model.Event;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a PersonExtended Object
 * 
 * @author Muresan Marius
 *
 */

public class PersonExtendedRowMapper implements ParameterizedRowMapper<PersonExtendedRow>
{

	@Override
	public PersonExtendedRow mapRow( ResultSet rs, int rowNum )
			throws SQLException
	{
		PersonExtendedRow per = new PersonExtendedRow();
		per.setId( rs.getLong( SqlNames.SQL_GET_PERSON_EXTENDED_ID ) );
		per.setLastName( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_LAST_NAME ) );
		per.setFirstName( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_FIRST_NAME ) );
		per.setPhone( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_PHONE ) );
		per.setEmail( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_EMAIL ) );
		per.setBirthDate( rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_BIRTH_DATE ) );

		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
		per.setBirthDateStr( sdf.format( rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_BIRTH_DATE ) ) );

		per.setSex( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_SEX ) );
		per.setGeneralObservations( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_GENERAL_OBSERVATIONS ) );
		per.setAddress(rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_ADDRESS ) );
		per.setLocality(rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_LOCALITY ));
		per.setCnp(rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_CNP ));
		per.setSeries(rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_SERIES ));
		per.setInregistrationDate(rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_INREGISTRATION_DATE ));
		per.setInregistrationDateStr( sdf.format( rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_INREGISTRATION_DATE ) ) );

		per.setEvent( this.createEvent( rs ) );
		return per;
	}

	Event createEvent( ResultSet rs )
			throws SQLException
	{
		Event ev = new Event();
		int id = rs.getInt( SqlNames.SQL_GET_PERSON_EXTENDED_PERSON_EVENT_ID );

		if ( 0 == id )
		{
			return null;
		}

		ev.setId( id );
		ev.setEventId( rs.getInt( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_ID ) );
		ev.setDescription( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_DESCRIPTION ) );
		ev.setEventDate( rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_DATE ) );
		ev.setEventLocation( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_LOCATION ) );

		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
		ev.setEventDateStr( sdf.format( rs.getDate( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_DATE ) ) );

		ev.setAppliedFromId( rs.getInt( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_APPLIED_FROM_ID ) );
		ev.setAppliedFromName( rs.getString( SqlNames.SQL_GET_PERSON_EXTENDED_EVENT_APPLIED_FROM_NAME ) );
		return ev;
	}

}
