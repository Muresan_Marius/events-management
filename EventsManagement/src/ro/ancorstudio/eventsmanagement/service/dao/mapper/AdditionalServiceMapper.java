package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a AdditionalService Object
 * 
 * @author Muresan Marius
 *
 */

public class AdditionalServiceMapper implements ParameterizedRowMapper<AdditionalService>
{

	@Override
	public AdditionalService mapRow( ResultSet rs, int numRows )
			throws SQLException
	{
		AdditionalService result = new AdditionalService();
		result.setId( rs.getLong( SqlNames.SQL_ADDITIONAL_SERVICE_ID ) );
		result.setDescription( rs.getString( SqlNames.SQL_ADDITIONAL_SERVICE_DESCRIPTION ) );
		result.setPrice (rs.getInt( SqlNames.SQL_ADDITIONAL_SERVICE_ID ) );
		return result;
	}

}
