package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.Event;
//import ro.ancorstudio.eventsmanagement.model.EventType;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a EventType Object
 * 
 * @author Muresan Marius
 *
 */

public class EventMapper implements ParameterizedRowMapper<Event>
{

	@Override
	public Event mapRow( ResultSet rs, int numRows )
			throws SQLException
	{
		Event result = new Event();
		result.setId( rs.getInt( SqlNames.SQL_EVENT_EVENTID ) );
		result.setDescription( rs.getString( SqlNames.SQL_EVENT_EVENTNAME ) );	
		
		return result;
	}

}