package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Maps a row from USERS table to a User object.
 * 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

public class UserMapper implements ParameterizedRowMapper<User>
{

	@Override
	public User mapRow( final java.sql.ResultSet rs, final int rowNum )
			throws SQLException
	{

		final User user = new User();

		user.setUserID( rs.getLong( SqlNames.SQL_USERS_USER_ID ) );
		user.setFirstName( rs.getString( SqlNames.SQL_USERS_FIRST_NAME ) );
		user.setLastName( rs.getString( SqlNames.SQL_USERS_LAST_NAME ) );
		user.setUsername( rs.getString( SqlNames.SQL_USERS_USERNAME ) );
		user.setUserType( rs.getInt( SqlNames.SQL_USERS_USER_TYPE ) );


		Timestamp timestamp = rs.getTimestamp( SqlNames.SQL_USERS_LAST_UPDATE );
		Date data = new Date( timestamp.getTime() );
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
		String lastUpdate = sdf.format( data );
		user.setLastUpdate( lastUpdate );

		return user;
	}

}
