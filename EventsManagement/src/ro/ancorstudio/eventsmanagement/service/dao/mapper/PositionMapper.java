package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.Position;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a Position Object
 * 
 * @author Muresan Marius
 *
 */

public class PositionMapper implements ParameterizedRowMapper<Position>
{

	@Override
	public Position mapRow( ResultSet rs, int numRows )
			throws SQLException
	{
		Position result = new Position();
		result.setName(rs.getString( SqlNames.SQL_PACKAGE_NAME ));
		result.setPrice(rs.getString( SqlNames.SQL_PACKAGE_PRICE ));
		result.setDescription( rs.getString( SqlNames.SQL_PACKAGE_DESCRIPTION ) );
		result.setId( rs.getLong( SqlNames.SQL_PACKAGE_ID ) );
		return result;
	}

}