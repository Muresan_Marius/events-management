package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.ResultSetPerson;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a ResultSetPerson Object
 * 
 * @author Muresan Marius
 *
 */

public class ResultSetPersonMapper implements ParameterizedRowMapper<ResultSetPerson>
{
	@Override
	public ResultSetPerson mapRow( ResultSet rs, int rrowNum )
			throws SQLException
	{

		ResultSetPerson result = new ResultSetPerson();
		result.setId( rs.getLong( SqlNames.SQL_RESULTSETPERSON_ID ) );
		result.setLastName( rs.getString( SqlNames.SQL_RESULTSETPERSON_LASTNAME ) );
		result.setFirstName( rs.getString( SqlNames.SQL_RESULTSETPERSON_FIRSTNAME ) );
		result.setPhone( rs.getString( SqlNames.SQL_RESULTSETPERSON_PHONE ) );
		result.setEventDescription(rs.getString( SqlNames.SQL_RESULTSETPERSON_EVENTS ) );
		result.setPositionName(rs.getString( SqlNames.SQL_RESULTSETPERSON_PACKAGE ) );
		result.setEventDate(rs.getDate( SqlNames.SQL_RESULTSETPERSON_EVENT_DATE ) );
		result.setLocation(rs.getString( SqlNames.SQL_RESULTSETPERSON_LOCATION ) );

		SimpleDateFormat dt = new SimpleDateFormat( "dd/MM/yyyy" );
		String eventDateStr = dt.format( rs.getDate( SqlNames.SQL_RESULTSETPERSON_EVENT_DATE ) );
		result.setEventDateStr(eventDateStr);

		return result;
	}
}
