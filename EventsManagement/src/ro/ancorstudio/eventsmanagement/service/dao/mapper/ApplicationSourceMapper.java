package ro.ancorstudio.eventsmanagement.service.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import ro.ancorstudio.eventsmanagement.model.ApplicationSource;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;

/**
 * Mapper class to map a row for a SQL select into a ApplicationSource Object
 * 
 * @author Muresan Marius
 *
 */

public class ApplicationSourceMapper implements ParameterizedRowMapper<ApplicationSource>
{

	@Override
	public ApplicationSource mapRow( ResultSet rs, int numRows )
			throws SQLException
	{
		ApplicationSource result = new ApplicationSource();
		result.setName( rs.getString( SqlNames.SQL_APPLIED_FROM_NAME ) );
		result.setId( rs.getLong( SqlNames.SQL_APPLIED_FROM_ID ) );
		return result;
	}

}