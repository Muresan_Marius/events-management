package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.ApplicationSource;

/**
 * This interface defines database operations related to ApplicationSource objects
 * 
 * @author Muresan Marius
 *
 */

public interface ApplicationSourceManagerDaoIntf
{
	public List<ApplicationSource> getAllApplicationSources();

	public int countApplicationSources();
}
