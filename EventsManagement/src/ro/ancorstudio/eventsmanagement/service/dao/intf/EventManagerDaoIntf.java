package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.Event;
//import ro.bgsoft.cvmanagement.model.PositionRow;

/**
 * This interface defines database operations related 
 * to Event objects
 * 
 * @author Marius Muresan
 *
 */

public interface EventManagerDaoIntf
{
	public List<Event> getAllEventsTypes();

	//public int countEvents();

	public void insertEvent( long id, Event e );
	
	public List<Event> getAllPersonEvents( long personId );
	public void deleteEvent( long id );
	public void updateEvent( Event event );
	public void updateEventSelection( Event event );

}
