package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;




import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.model.User;

/**
 * This interface defines database operations related to user objects.
 * 
 * @author Muresan Marius
 *
 */

public interface UserManagerDaoIntf
{

	public User authenticate( String username, String password );

	public void updateUser( User modifiedUser );

	public void updateCheckTypeUser( long userId, int userType );
	
	public List<Integer> getPersonUsersIds( Long personEventId );

	public void insertUser( User newUser );
	
	public List<User> getAllUsers();

	public int countUsers();
	
	public void insertUserEvent( long id, User user );

	public void deleteUserEvent( Integer id );
}
