package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.Person;

/**
 * This interface defines database operations related to person objects.
 * @author Muresan Marius
 *
 */

public interface PersonManagerDaoIntf
{

	public void updatePerson( Person modifiedPerson );

	public void insertPerson( Person newPerson );

	public List<String> getLastNames( String lastName, String firstName );

	public List<String> getFirstNames( String lastName, String firstName );

	public long getPersonId( String lastName, String firstName, String email, String phone );

}
