package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.ResultSetPerson;

/**
 * This interface defines database operations related to Person objects for Search CV
 * 
 * @author Muresan Marius
 *
 */

public interface ResultSetPersonManagerDaoIntf
{

	public List<ResultSetPerson> getAllPersons( String sqlSort, String sqlLimit );

	public List<ResultSetPerson> getFilteredPersons( String sqlFilters, String sqlSort, String sqlLimit );

	public int countPersons( String sqlFilters );

	public void executeTransactionalQueryList( List<String> queries );

}
