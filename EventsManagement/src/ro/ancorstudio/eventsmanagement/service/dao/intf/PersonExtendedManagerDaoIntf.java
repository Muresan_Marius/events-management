package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.Person;
//import ro.bgsoft.cvmanagement.model.PersonExtendedRow;
import ro.ancorstudio.eventsmanagement.model.PersonExtended;

/**
 * This interface defines database operations related to Person objects
 * 
 * @author Muresan Marius
 *
 */

public interface PersonExtendedManagerDaoIntf
{

	public PersonExtended getPersonExtended( long personID );
	public void updatePersonExtended( PersonExtended modifiedPersonExtended );
	
	public void insertPersonExtended( PersonExtended newPersonExtended );
}
