package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.AdditionalService;;

/**
 * This interface defines database operations related 
 * to Additional Service objects
 * 
 * @author Muresan Marius
 *
 */

public interface AdditionalServiceManagerDaoIntf
{
	public List<AdditionalService> getAllAdditionalServices();

	public List<Integer> getPersonAdditionalServicesIds( Long personEventId );

	public int countAdditionalServices();

	public void insertAdditionalService( long id, AdditionalService additionalService );

	public void deleteAdditionalService( Integer id );

}
