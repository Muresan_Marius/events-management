package ro.ancorstudio.eventsmanagement.service.dao.intf;

/**
 * This interface defines database operations related 
 * to Cv objects
 * 
 * @author Muresan Marius
 *
 */
public interface CvManagerDaoIntf
{

	public String getCVPathByPersonID( long personID );

	public void uploadCVPathByPersonID( long personID, String cvPath );
}
