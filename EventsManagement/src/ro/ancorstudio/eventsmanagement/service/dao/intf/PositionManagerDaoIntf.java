package ro.ancorstudio.eventsmanagement.service.dao.intf;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.Position;
//import ro.ancorstudio.eventsmanagement.model.PositionRow;

/**
 * This interface defines database operations related 
 * to Position objects
 * 
 * @author Marius Muresan
 *
 */

public interface PositionManagerDaoIntf
{
	public List<Position> getAllPositions();

	public int countPositions();
	public void insertOnlyPosition( Position newPosition );

	public void updateOnlyPosition( Position modifiedPosition );

}
