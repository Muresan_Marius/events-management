package ro.ancorstudio.eventsmanagement.service.dao;

/**
 * This class stores constants related to database tables (names ...)
 * 
 * @author Muresan Marius
 *
 */
public class SqlNames
{ // names for the table USERL - just for tests
	public static final String	SQL_USERL_ID												= "PERSON_ID";
	public static final String	SQL_USERL_FIRSTNAME											= "FIRST_NAME";
	public static final String	SQL_USERL_LASTNAME											= "LAST_NAME";
	public static final String	SQL_USERL_BIRTHDATE											= "BIRTH_DATE";
	public static final String	SQL_USERL_EMAIL												= "EMAIL";
	public static final String	SQL_USERL_PHONE												= "PHONE";
	public static final String	SQL_USERL_ADDRESS											= "ADRESS";
	public static final String	SQL_USERL_LOCATION											= "LOCATION_ID";
	public static final String	SQL_USERL_SEX												= "SEX";
	public static final String	SQL_USERL_DATESTART											= "DATE_START";
	public static final String	SQL_USERL_FIDELITY											= "FIDELITY";
	public static final String	SQL_USERL_GENERALOBSERVATIONS								= "GENERAL_OBSERVATIONS";
	public static final String	SQL_USERL_LASTUPDATE										= "LAST_UPDATE";

	// names for the table DESC_USERS
	public static final String	SQL_USERS_USER_ID											= "USER_ID";
	public static final String	SQL_USERS_FIRST_NAME										= "FIRST_NAME";
	public static final String	SQL_USERS_LAST_NAME											= "LAST_NAME";
	public static final String	SQL_USERS_USERNAME											= "USERNAME";
	public static final String	SQL_USERS_PASSWORD_APP										= "PASSWORD_APP";
	public static final String	SQL_USERS_USER_TYPE											= "USER_TYPE";
	public static final String	SQL_USERS_LAST_UPDATE										= "LAST_UPDATE";

	public static final String	SQL_CVS_PERSON_LASTNAME										= "LAST_NAME";
	public static final String	SQL_CVS_PERSON_FIRSTNAME									= "FIRST_NAME";
	public static final String	SQL_CVS_PERSON_ID											= "PERSON_ID";
	public static final String	SQL_CVS_CV_PATH												= "CV_PATH";

	public static final String	SQL_RESULTSETPERSON_ID										= "PERSON_ID";
	public static final String	SQL_RESULTSETPERSON_FIRSTNAME								= "FIRST_NAME";
	public static final String	SQL_RESULTSETPERSON_LASTNAME								= "LAST_NAME";
	public static final String	SQL_RESULTSETPERSON_PHONE									= "PHONE";
	public static final String	SQL_RESULTSETPERSON_EVENTS									= "EVENT_DESCRIPTION";
	public static final String	SQL_RESULTSETPERSON_PACKAGE									= "NAME";
	public static final String	SQL_RESULTSETPERSON_EVENT_DATE								= "DATE_EVENT";
	public static final String	SQL_RESULTSETPERSON_LOCATION								= "LOCATION";
	
	
	public static final String	SQL_ADDITIONAL_SERVICE_ID									= "ADDITIONAL_SERVICE_ID";
	public static final String	SQL_USER_ID													= "USER_ID";
	public static final String	SQL_ADDITIONAL_SERVICE_DESCRIPTION							= "ADDITIONAL_SERVICE_DESCRIPTION";
	public static final String	SQL_ADDITIONAL_SERVICE_PRICE								= "ADDITIONAL_SERVICE_PRICE";

	// Names for the table APPLIED_FROM
	public static final String	SQL_APPLIED_FROM_ID											= "APPLIED_FROM_ID";
	public static final String	SQL_APPLIED_FROM_NAME										= "APPLIED_FROM_NAME";

	// Names for the table POSITION
	public static final String	SQL_POSITION_POSITIONID										= "POS_ID";
	public static final String	SQL_POSITION_POSITIONNAME									= "POSITION_DESC";

	// Names for the table PACKAGE
	public static final String	SQL_PACKAGE_ID												= "PACKAGE_ID";
	public static final String	SQL_PACKAGE_NAME											= "NAME";
	public static final String	SQL_PACKAGE_DESCRIPTION										= "DESCRIPTION";
	public static final String	SQL_PACKAGE_PRICE											= "PRICE";
	
	// Names for the table EVENT
	public static final String	SQL_EVENT_EVENTID											= "EVENT_ID";
	public static final String	SQL_EVENT_EVENTNAME											= "EVENT_DESCRIPTION";


	// Namer for PersonExtended Model (insert)
	public static final String	SQL_SET_PERSONS_EVENTS_ID									= "ID";
	public static final String	SQL_SET_PERSONS_EVENTS_ID_ADDITIONAL_SERVICES				= "PERSONS_EVENTS_ID";
	public static final String	SQL_SET_PERSONS_EVENTS_ADDITIONAL_SERVICE_ID				= "ADDITIONAL_SERVICE_ID";
	public static final String	SQL_SET_PERSONS_EVENTS_USER_ID								= "USER_ID";

	public static final String	SQL_SET_PERSON_EXTENDED_ID									= "PERSON_ID";
	public static final String	SQL_SET_PERSON_EXTENDED_LAST_NAME							= "LAST_NAME";
	public static final String	SQL_SET_PERSON_EXTENDED_FIRST_NAME							= "FIRST_NAME";
	public static final String	SQL_SET_PERSON_EXTENDED_BIRTH_DATE							= "BIRTH_DATE";
	public static final String	SQL_SET_PERSON_EXTENDED_EMAIL								= "EMAIL";
	public static final String	SQL_SET_PERSON_EXTENDED_PHONE								= "PHONE";
	public static final String	SQL_SET_PERSON_EXTENDED_ADDRESS								= "ADDRESS";
	public static final String	SQL_SET_PERSON_EXTENDED_SEX									= "SEX";
	public static final String	SQL_SET_PERSON_EXTENDED_LOCALITY							= "LOCALITY";
	public static final String	SQL_SET_PERSON_EXTENDED_CNP									= "CNP";
	public static final String	SQL_SET_PERSON_EXTENDED_SERIES								= "SERIES";
	public static final String	SQL_SET_PERSON_EXTENDED_INREGISTRATION_DATE					= "INREGISTRATION_DATE";
	public static final String	SQL_SET_PERSON_EXTENDED_DISPONIBILITY_MISSION				= "DISPONIBILITY_MISSION";
	public static final String	SQL_SET_PERSON_EXTENDED_GENERAL_OBSERVATIONS				= "GENERAL_OBSERVATIONS";

	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_ID							= "ID";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_DESCRIPTION_ID				= "EVENT_ID";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_DESCRIPTION					= "EVENT_DESCRIPTION";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_APPLIED_FROM_ID				= "APPLIED_FROM_ID";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_DATE							= "DATE_EVENT";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_LOCATION						= "LOCATION";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_PACKAGE_ID					= "PACKAGE_ID";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_HD							= "HD";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_ADVANCE						= "ADVANCE";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_VIDEO_FORMAT					= "VIDEO_FORMAT";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_PRICE							= "PRICE";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_DETAILS						= "DETAILS";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_STATUS_ID						= "STATUS_ID";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_OBSERVATIONS					= "OBSERVATIONS";
	public static final String	SQL_SET_PERSON_EXTENDED_EVENT_DELIVERED						= "DELIVERED";


	// Namer for PersonExtended Model (update)
	public static final String	SQL_GET_PERSON_EXTENDED_ID									= "PERSON_ID";
	public static final String	SQL_GET_PERSON_EXTENDED_LAST_NAME							= "PERSON_LAST_NAME";
	public static final String	SQL_GET_PERSON_EXTENDED_FIRST_NAME							= "PERSON_FIRST_NAME";
	public static final String	SQL_GET_PERSON_EXTENDED_PHONE								= "PERSON_PHONE";
	public static final String	SQL_GET_PERSON_EXTENDED_EMAIL								= "PERSON_EMAIL";
	public static final String	SQL_GET_PERSON_EXTENDED_BIRTH_DATE							= "PERSON_BIRTH_DATE";
	public static final String	SQL_GET_PERSON_EXTENDED_SEX									= "PERSON_SEX";
	public static final String	SQL_GET_PERSON_EXTENDED_JOB_START							= "DATE_START";
	public static final String	SQL_GET_PERSON_EXTENDED_FIDELITY							= "FIDELITY";
	public static final String	SQL_GET_PERSON_EXTENDED_GENERAL_OBSERVATIONS				= "PERSON_GENERAL_OBSERVATIONS";
	public static final String	SQL_GET_PERSON_EXTENDED_ADDRESS								= "PERSON_ADDRESS";
	public static final String	SQL_GET_PERSON_EXTENDED_LOCALITY							= "PERSON_LOCALITY";
	public static final String	SQL_GET_PERSON_EXTENDED_CNP									= "PERSON_CNP";
	public static final String	SQL_GET_PERSON_EXTENDED_SERIES								= "PERSON_SERIES";
	public static final String	SQL_GET_PERSON_EXTENDED_INREGISTRATION_DATE					= "PERSON_INREGISTRATION_DATE";

	public static final String	SQL_GET_PERSON_EXTENDED_POSITION_ID							= "POSITION_ID";
	public static final String	SQL_GET_PERSON_EXTENDED_POSITION_DESCRIPTION				= "POSITION_DESCRIPTION";
	public static final String	SQL_GET_PERSON_EXTENDED_PERSON_POSITION_ID					= "PERSON_POSITION_ID";
	
	//GET PERS EXT
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_LOCATION						= "LOCATION";
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_DATE						 	= "DATE_EVENT";
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_APPLIED_FROM_ID				= "APPLIED_FROM_ID";
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_APPLIED_FROM_NAME				= "APPLIED_FROM_NAME";
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_ID							= "EVENT_ID";
	public static final String	SQL_GET_PERSON_EXTENDED_EVENT_DESCRIPTION					= "EVENT_DESCRIPTION";
	public static final String	SQL_GET_PERSON_EXTENDED_PERSON_EVENT_ID						= "PERSON_EVENT_ID";
	
	//EVENT
	public static final String	SQL_PERSONS_EVENTS_ID										= "ID";
	public static final String	SQL_EVENT_ID												= "EVENT_ID";
	public static final String	SQL_EVENT_DESCRIPTION										= "EVENT_DESCRIPTION";
	public static final String	SQL_EVENT_DATE												= "DATE_EVENT";
	public static final String	SQL_EVENT_APPLIED_FROM_ID									= "EVENT_APPLIED_FROM_ID";

	/* UPDATE */
	public static final String	SQL_SET_EVENT_ID											= "ID";
	public static final String	SQL_SET_EVENT_APPLIED_FROM_ID								= "APPLIED_FROM_ID";
	public static final String	SQL_SET_EVENT_DATE											= "DATE_EVENT";
	public static final String	SQL_SET_EVENT_LOCATION										= "LOCATION";
	public static final String	SQL_SET_EVENT_PRICE											= "PRICE";
	public static final String	SQL_SET_EVENT_ADVANCE										= "ADVANCE";
	public static final String	SQL_SET_EVENT_DETAILS										= "DETAILS";
	public static final String	SQL_SET_EVENT_POSITION										= "PACKAGE_ID";

}
