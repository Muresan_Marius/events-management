package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.ModelConstants;
import ro.ancorstudio.eventsmanagement.model.Person;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.PersonManagerDaoIntf;

/**
 * MySql implementation for PersonManagerDaoIntf
 * 
 * @author Muresan Marius
 *
 */

@Repository("_personManager")
public class PersonManagerDaoImpl extends NamedParameterJdbcDaoSupport implements PersonManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( PersonManagerDaoImpl.class.getName() );

	@Override
	public void updatePerson( Person modifiedPerson )
	{
		_log.info( "Inside updatePerson method from: " + this.getClass().getName() );

		if ( modifiedPerson == null || ModelConstants.NEW_PERSON_ID == modifiedPerson.getId() )
		{
			_log.warn( "Bad parameter! (null person or no ID)" );
			return;
		}

		MapSqlParameterSource params = new MapSqlParameterSource();

		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_ID, modifiedPerson.getId() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_FIRST_NAME, modifiedPerson.getFirstName() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_LAST_NAME, modifiedPerson.getLastName() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_BIRTH_DATE, modifiedPerson.getBirthDate() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_EMAIL, modifiedPerson.getEmail() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_PHONE, modifiedPerson.getPhone() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_SEX, modifiedPerson.getSex() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_GENERAL_OBSERVATIONS, modifiedPerson.getGeneralObservations() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_ADDRESS, modifiedPerson.getAddress() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_LOCALITY, modifiedPerson.getLocality() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_CNP, modifiedPerson.getCnp() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_SERIES, modifiedPerson.getSeries() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_INREGISTRATION_DATE, modifiedPerson.getInregistrationDate() );
		
		getNamedParameterJdbcTemplate().update( SqlStatements.SQL_UPDATE_PERSON, params );
	}

	@Override
	public void insertPerson( Person newPerson )
	{
		if ( newPerson == null || ModelConstants.NEW_PERSON_ID != newPerson.getId() )
		{
			_log.warn( "Bad parameter! (null person or no new person ID)" );
			return;
		}

		String sql = SqlStatements.SQL_USERL_INSERT_PERSON;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_USERL_ID, null, java.sql.Types.NULL );
		params.addValue( SqlNames.SQL_USERL_FIRSTNAME, newPerson.getFirstName() );
		params.addValue( SqlNames.SQL_USERL_LASTNAME, newPerson.getLastName() );
		params.addValue( SqlNames.SQL_USERL_BIRTHDATE, newPerson.getBirthDate() );
		params.addValue( SqlNames.SQL_USERL_EMAIL, newPerson.getEmail() );
		params.addValue( SqlNames.SQL_USERL_PHONE, newPerson.getPhone() );
		params.addValue( SqlNames.SQL_USERL_SEX, newPerson.getSex() );
		params.addValue( SqlNames.SQL_USERL_GENERALOBSERVATIONS, newPerson.getGeneralObservations() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
		}
	}

	@Override
	public List<String> getLastNames( String lastName, String firstName )
	{
		List<String> result = new ArrayList<String>();

		String sql = SqlStatements.GET_PERSON_LAST_NAMES;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			sql = sql.replace( "${LAST_NAME}", lastName );
			sql = sql.replace( "${FIRST_NAME}", firstName );

			result = this.getNamedParameterJdbcTemplate().queryForList( sql, params, String.class );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting lastNames list. Exception is: " + e.getMessage() );
		}

		return result;
	}

	@Override
	public List<String> getFirstNames( String lastName, String firstName )
	{
		List<String> result = new ArrayList<String>();

		String sql = SqlStatements.GET_PERSON_FIRST_NAMES;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			sql = sql.replace( "${LAST_NAME}", lastName );
			sql = sql.replace( "${FIRST_NAME}", firstName );

			result = this.getNamedParameterJdbcTemplate().queryForList( sql, params, String.class );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting firstNames list. Exception is: " + e.getMessage() );
		}

		return result;
	}

	@Override
	public long getPersonId( String lastName, String firstName, String email, String phone )
	{
		long result = -1;

		String sql = SqlStatements.SQL_PERSONS_GET_PERSON_ID;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			sql = sql.replace( "${LAST_NAME}", lastName );
			sql = sql.replace( "${FIRST_NAME}", firstName );
			sql = sql.replace( "${EMAIL}", email );
			sql = sql.replace( "${PHONE}", phone );

			result = this.getNamedParameterJdbcTemplate().queryForLong( sql, params );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting person id. Exception is: " + e.getMessage() );
		}

		return result;
	}

}
