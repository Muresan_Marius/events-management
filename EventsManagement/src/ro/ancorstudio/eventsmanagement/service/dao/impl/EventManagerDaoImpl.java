package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.h2.engine.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.model.Event;
import ro.ancorstudio.eventsmanagement.model.EventRow;
import ro.ancorstudio.eventsmanagement.model.PersonExtended;
import ro.ancorstudio.eventsmanagement.service.Managers;
//import ro.bgsoft.cvmanagement.model.PositionRow;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.EventManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.EventMapper;
//import ro.ancorstudio.eventsmanagement.service.dao.mapper.PositionRowMapper;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.EventRowMapper;

/**
 * MySql implementation for EventManagerDaoIntf
 * 
 * @author Muresan Marius
 * 
 */

@Repository("_eventManager")
public class EventManagerDaoImpl extends NamedParameterJdbcDaoSupport implements EventManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( EventManagerDaoImpl.class.getName() );

	@Override
	public List<Event> getAllEventsTypes()
	{
		List<Event> result = new ArrayList<Event>();

		try
		{
			result = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_EVENT_GET_ALL_EVENTS, new MapSqlParameterSource(), new EventMapper() );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}
	
	public void insertEvent( long personId, Event ev )
	{

		String sql = SqlStatements.CVS_INSERT_EVENTS;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_ID, personId );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_EVENT_DESCRIPTION_ID, ev.getDescriptionId() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_EVENT_DATE, ev.getEventDate() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_EVENT_APPLIED_FROM_ID, ev.getAppliedFromId());

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}

	}
	
	@Override
	public List<Event> getAllPersonEvents( long personId )
	{
		_log.trace( "Entering method PositionManagerDaoImpl.getAllPersonEvents()" );
		List<Event> result = new ArrayList<Event>();
		try
		{
			List<EventRow> partialResult = new ArrayList<EventRow>();
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue( "PERSON_ID", personId );
			partialResult = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_EVENT_GET_BY_PERSON_ID, params, new EventRowMapper() );
			result = this.toEvent( partialResult );
			
		}
		catch ( Exception e )
		{
			_log.debug( "Exception occured : " + e.getMessage() );
		}
		_log.trace( "Exiting method PositionManagerDaoImpl.getAllPersonEvents()" );
		return result;
	}
	
	private List<Event> toEvent( List<EventRow> eventSet )
	{
		_log.trace( "Entering method ResultSetPersonManagerDaoImpl.toResultSetPerson()" );

		List<Event> result = new ArrayList<Event>();
		if ( eventSet == null || eventSet.isEmpty() )
		{
			return result;
		}

		for ( EventRow currentRow : eventSet )
		{
			Event event = this.createEvent( currentRow );
			result.add( event );
		}
		return result;
	}

	private Event createEvent( EventRow currentPartialResult )
	{
		_log.trace( "Entering method PositionManagerDaoImpl.createPosition()" );

		Event ev = new Event();
		ev.setId( currentPartialResult.getId() );
		ev.setEventId(currentPartialResult.getEventId() );
		ev.setAppliedFromId( currentPartialResult.getAppliedFromId() );
		ev.setEventDate(currentPartialResult.getEventDate() );
		ev.setDescription( currentPartialResult.getDescription() );
		
		return ev;
	}
	
	private int getIndexForEvent( List<Event> eventList, Event eventToFind )
	{
		int res = -1;
		try
		{
			int index = 0;
			for ( Event pos : eventList )
			{
				if ( pos.equals( eventToFind ) )
				{
					res = index;
					break;
				}
				index++;
			}
		}
		catch ( Exception e )
		{
		}
		return res;
	}
	
	@Override
	public void deleteEvent( long id )
	{
		String sql = new String();
		sql = SqlStatements.SQL_DELETE_EVENT;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_EVENT_ID, id );
		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}

	}
	
	@Override
	public void updateEvent( Event event )
	{
		String sql = new String();
		sql = SqlStatements.SQL_UPDATE_EVENT;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_EVENT_ID, event.getId() );
		params.addValue( SqlNames.SQL_SET_EVENT_APPLIED_FROM_ID, event.getAppliedFromId() );
		params.addValue( SqlNames.SQL_SET_EVENT_DATE, event.getEventDate() );
		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}
	
	public void updateEvent( long personId, Event p )
	{
		String sql = SqlStatements.CVS_INSERT_ONLY_POSITION;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_POSITION_DESCRIPTION, p.getDescription() );
		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}
	
	@Override
	public void updateEventSelection( Event event )
	{
		String sql = new String();
		sql = SqlStatements.SQL_UPDATE_EVENT_SELECTION;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_EVENT_ID, event.getId() );
		params.addValue( SqlNames.SQL_SET_EVENT_LOCATION, event.getEventLocation() );
		params.addValue( SqlNames.SQL_SET_EVENT_PRICE, event.getPrice() );
		params.addValue( SqlNames.SQL_SET_EVENT_ADVANCE, event.getAdvance() );
		params.addValue( SqlNames.SQL_SET_EVENT_DETAILS, event.getDetails() );
		params.addValue( SqlNames.SQL_SET_EVENT_POSITION, event.getPosition() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
		
		List<Integer> db_additionalServices = Managers.getInstance().getAdditionalServiceManager().getPersonAdditionalServicesIds(event.getId() );
		for (Integer id : db_additionalServices ){
			if (!UpdateClass.compareWithAdditionalService(id, event)){
				Managers.getInstance().getAdditionalServiceManager().deleteAdditionalService(id);
			}			
		}
		for (AdditionalService additionalService : event.getAdditionalServices() ){
			if ( !UpdateClass.compareFromAdditionalService( (int) additionalService.getId(), db_additionalServices ) ){
				Managers.getInstance().getAdditionalServiceManager().insertAdditionalService(event.getId(), additionalService);
			}
		}
		
		List<Integer> db_users = Managers.getInstance().getUserManagerDaoIntf().getPersonUsersIds(event.getId());
		for (Integer id : db_users ){
			if (!UpdateClass.compareWithUser(id, event)){
				Managers.getInstance().getUserManagerDaoIntf().deleteUserEvent(id);
			}			
		}
		for ( ro.ancorstudio.eventsmanagement.model.User user : event.getUsers() ){
			if ( !UpdateClass.compareFromUser( (int) user.getUserID(), db_users ) ){
				Managers.getInstance().getUserManagerDaoIntf().insertUserEvent(event.getId(), user);
			}
		}
	}
	
	static class UpdateClass
	{

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		
		public static boolean compareFromAdditionalService( int id, List<Integer> db_additionalServices )
		{
			for ( Integer dbId : db_additionalServices )
			{
				if ( dbId == id )
				{
					return true;
				}
			}
			return false;
		}

		public static boolean compareWithAdditionalService( int id, Event event)
		{

			for ( AdditionalService additionalService : event.getAdditionalServices())
			{
				if ( id == additionalService.getId() )
				{
					return true;
				}
			}
			return false;
		}
		
		public static boolean compareFromUser( int id, List<Integer> db_users )
		{
			for ( Integer dbId : db_users )
			{
				if ( dbId == id )
				{
					return true;
				}
			}
			return false;
		}

		public static boolean compareWithUser( int id, Event event)
		{

			for ( ro.ancorstudio.eventsmanagement.model.User user : event.getUsers() )
			{
				if ( id == user.getUserID() )
				{
					return true;
				}
			}
			return false;
		}
	}
}

