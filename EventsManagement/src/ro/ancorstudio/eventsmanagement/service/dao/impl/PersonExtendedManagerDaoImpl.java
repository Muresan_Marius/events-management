package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.Person;
import ro.ancorstudio.eventsmanagement.model.Event;
import ro.ancorstudio.eventsmanagement.model.PersonExtended;
import ro.ancorstudio.eventsmanagement.model.PersonExtendedRow;
import ro.ancorstudio.eventsmanagement.service.Managers;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
/*import ro.bgsoft.cvmanagement.model.CandidatRow;
import ro.bgsoft.cvmanagement.model.Language;
import ro.bgsoft.cvmanagement.model.Person;
import ro.bgsoft.cvmanagement.model.PersonExtended;
import ro.bgsoft.cvmanagement.model.PersonExtendedRow;
import ro.bgsoft.cvmanagement.model.Position;
import ro.bgsoft.cvmanagement.model.Technology;
import ro.bgsoft.cvmanagement.service.Managers;
import ro.bgsoft.cvmanagement.service.dao.SqlNames;
import ro.bgsoft.cvmanagement.service.dao.SqlStatements;

import ro.bgsoft.cvmanagement.service.dao.mapper.CandidatRowMapper;
import ro.bgsoft.cvmanagement.service.dao.mapper.PersonExtendedRowMapper;
import ro.bgsoft.cvmanagement.service.dao.mapper.PositionSelectedMapper;*/
import ro.ancorstudio.eventsmanagement.service.dao.intf.PersonExtendedManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.PersonExtendedRowMapper;
/**
 * MySql implementation for PersonManagerDaoIntf
 * 
 * @author Muresan Marius
 * @version 1.0
 *
 */

@Repository("_personExtendedManager")
public class PersonExtendedManagerDaoImpl extends NamedParameterJdbcDaoSupport implements PersonExtendedManagerDaoIntf
{
	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( PersonExtendedManagerDaoImpl.class.getName() );

	/**
	 * Convert ResultSetPerson list to PersonExtended list
	 * @param SqlFilters
	 * @param SqlSort
	 * @return
	 */

	public PersonExtended getPersonExtended( long personId )
	{
		_log.trace( "Entering method PersonExtendedManagerDaoImpl.getPersonExtended()" );
		PersonExtended result = new PersonExtended();
		try
		{
			List<PersonExtendedRow> partialResult = new ArrayList<PersonExtendedRow>();
			partialResult = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_PERSONEXTENDED_GET_PERSON + " where PERSONS.PERSON_ID =" + String.valueOf( personId ), new MapSqlParameterSource(), new PersonExtendedRowMapper() );
			result = toPersonExtended( partialResult );
		}
		catch ( Exception e )
		{
			_log.debug( "Exception occured : " + e.getMessage() );
		}
		_log.trace( "Exiting method PersonExtendedManagerDaoImpl.getPersonExtended()" );
		return result;
	}
	
	private PersonExtended toPersonExtended( List<PersonExtendedRow> personSet )
	{
		PersonExtended person = this.createPerson( personSet.get( 0 ) );
		for ( PersonExtendedRow currentPartialResult : personSet )
		{
			this.addToPerson( person, currentPartialResult );
		}
		return person;
	}
	
	private PersonExtended createPerson( PersonExtendedRow currentPartialResult )
	{
		PersonExtended pe = new PersonExtended();
		pe.setId( currentPartialResult.getId() );
		pe.setLastName( currentPartialResult.getLastName() );
		pe.setFirstName( currentPartialResult.getFirstName() );
		pe.setPhone( currentPartialResult.getPhone() );
		pe.setEmail( currentPartialResult.getEmail() );
		pe.setBirthDate( currentPartialResult.getBirthDate() );
		pe.setBirthDateStr( currentPartialResult.getBirthDateStr() );
		pe.setGeneralObservations( currentPartialResult.getGeneralObservations() );
		pe.setSex( currentPartialResult.getSex() );
		pe.setAddress(currentPartialResult.getAddress());
		pe.setLocality(currentPartialResult.getLocality());
		pe.setCnp(currentPartialResult.getCnp());
		pe.setSeries(currentPartialResult.getSeries());
		pe.setInregistrationDate(currentPartialResult.getInregistrationDate());
		pe.setInregistrationDateStr(currentPartialResult.getInregistrationDateStr());
		return pe;
	}

	
	private void addToPerson( PersonExtended person, PersonExtendedRow currentPartialResult )
	{
		if ( currentPartialResult.getEvent() != null )
			person.getEvents().add( currentPartialResult.getEvent() );
	}

	@Override
	public void insertPersonExtended( PersonExtended newPersonExtended )
	{
		if ( newPersonExtended == null )
		{
			_log.warn( "Bad parameter! (null person)" );
			return;
		}
		try
		{
			this.insertPerson(newPersonExtended);
			long persId = getPersonId( newPersonExtended.getLastName(), newPersonExtended.getFirstName(), newPersonExtended.getEmail(), newPersonExtended.getPhone() );

			for ( Event event : newPersonExtended.getEvents() )
			{
				Managers.getInstance().getEventManagerDaoIntf().insertEvent( persId, event );

			}

		}
		catch ( Exception e )
		{
		}
	}
	
	@Override
	public void updatePersonExtended( PersonExtended modifiedPersonExtended )
	{
		if ( modifiedPersonExtended == null )
		{
			_log.warn( "Bad parameter! (null person)" );
			return;
		}

		try
		{
			Managers.getInstance().getPersonManagerDaoIntf().updatePerson( (Person) modifiedPersonExtended );

			List<Event> dbEvents = Managers.getInstance().getEventManagerDaoIntf().getAllPersonEvents( modifiedPersonExtended.getId() );
			for ( Event dbEvent : dbEvents )
			{
				if ( !UpdateClass.compareFromEvent( dbEvent, modifiedPersonExtended ) )
				{
					Managers.getInstance().getEventManagerDaoIntf().deleteEvent( dbEvent.getId() );
				}
			}

			for ( Event event : modifiedPersonExtended.getEvents() )
			{
				switch ( UpdateClass.compareWithEvent( event, dbEvents ) )
				{
					case 0:
						Managers.getInstance().getEventManagerDaoIntf().insertEvent( modifiedPersonExtended.getId(), event );
						break;
					case 1:
						Managers.getInstance().getEventManagerDaoIntf().updateEvent( event );
						break;
				}
			}
		}
		catch ( Exception e )
		{
			_log.debug( "Exception:" + e.getMessage() );
		}
	}

	//Method for inserts into Persons table
	//call the insertMobilities method
	public void insertPerson( PersonExtended newPersonExtended )
	{
		String sql = SqlStatements.CVS_INSERT_PERSONS;
		MapSqlParameterSource params = new MapSqlParameterSource();

		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_ID, null );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_FIRST_NAME, newPersonExtended.getFirstName() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_LAST_NAME, newPersonExtended.getLastName() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_PHONE, newPersonExtended.getPhone() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_EMAIL, newPersonExtended.getEmail() );
		Date birthDate = newPersonExtended.getBirthDate();
		if ( birthDate != null )
		{
			params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_BIRTH_DATE, newPersonExtended.getBirthDate(), java.sql.Types.TIMESTAMP );
		}
		else
		{
			params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_BIRTH_DATE, null, java.sql.Types.NULL );
		}
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_SEX, newPersonExtended.getSex() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_GENERAL_OBSERVATIONS, newPersonExtended.getGeneralObservations() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_ADDRESS, newPersonExtended.getAddress() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_LOCALITY, newPersonExtended.getLocality() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_CNP, newPersonExtended.getCnp() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_SERIES, newPersonExtended.getSeries() );
		params.addValue( SqlNames.SQL_SET_PERSON_EXTENDED_INREGISTRATION_DATE, newPersonExtended.getInregistrationDate() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
			_log.info("de-a pl");
		}

	}
	
	public long getPersonId( String lastName, String firstName, String email, String phone )
	{
		long result = -1;

		String sql = SqlStatements.SQL_PERSONS_GET_PERSON_ID;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			sql = sql.replace( "${LAST_NAME}", lastName );
			sql = sql.replace( "${FIRST_NAME}", firstName );
			sql = sql.replace( "${EMAIL}", email );
			sql = sql.replace( "${PHONE}", phone );

			result = this.getNamedParameterJdbcTemplate().queryForLong( sql, params );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting person id. Exception is: " + e.getMessage() );
		}

		return result;
	}

static class UpdateClass
{

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	
	public static boolean compareFromEvent( Event dbEvent, PersonExtended pe )
	{
		for ( Event event : pe.getEvents() )
		{
			if ( dbEvent.getEventId() == event.getEventId() )
			{
				return true;
			}
		}
		return false;
	}

	public static int compareWithEvent( Event event, List<Event> dbEvents )
	{

		for ( Event dbEvent : dbEvents )
		{
			if ( event.getEventId() == dbEvent.getEventId() )
			{
				if ( event.getEventDate().compareTo( dbEvent.getEventDate() ) != 0 )
				{
					return 1;
				}
				if ( event.getAppliedFromId() != dbEvent.getAppliedFromId() )
				{
					return 1;
				}
				return 2;
			}
		}
		return 0;
	}
}
}
