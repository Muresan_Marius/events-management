package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.Position;
import ro.ancorstudio.eventsmanagement.service.PositionsService;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.PositionMapper;

/**
 * Service for processing Positions.
 **/
@Repository("_positionsService")
public class PositionsServiceDaoImpl extends SimpleJdbcDaoSupport implements PositionsService
{

	@Override
	public int countPositions()
	{
		String sql = SqlStatements.SQL_POSITION_COUNT_POSITIONS;

		int count = getSimpleJdbcTemplate().queryForInt( sql, new MapSqlParameterSource() );

		return count;
	}

	@Override
	public List<Position> getAllPositions( String sqlSort, String sqlLimit )

	{
		String sql = SqlStatements.SQL_POSITION_GET_ALL_POSITIONS + sqlSort + " " + sqlLimit;

		List<Position> result = getSimpleJdbcTemplate().query( sql, new PositionMapper() );
		return result;
	}
}
