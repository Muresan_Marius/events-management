package ro.ancorstudio.eventsmanagement.service.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.CvManagerDaoIntf;

@Repository("_cvManager")
public class CvManagerDaoImpl extends NamedParameterJdbcDaoSupport implements CvManagerDaoIntf
{
	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( CvManagerDaoImpl.class.getName() );

	@Override
	public String getCVPathByPersonID( long personID )
	{
		String result = new String();
		String sql = SqlStatements.GET_CV_PATH_BY_PERSON_ID;
		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue( "PERSON_ID", personID );
			result = this.getNamedParameterJdbcTemplate().queryForObject( sql, params, String.class );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}

	@Override
	public void uploadCVPathByPersonID( long personID, String cvPath )
	{
		String sql = SqlStatements.SQL_UPDATE_CV_PATH_BY_PERSON_ID;
		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue( "PERSON_ID", personID );
			params.addValue( "LAST_CV_PATH", cvPath );
			this.getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}
	}

}
