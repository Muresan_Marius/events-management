package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.ModelConstants;
import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.model.ApplicationSource;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.UserManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.ApplicationSourceMapper;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.UserMapper;

/**
 * MySql implementation for UserManagerDaoIntf
 * 
 * @author Muresan Marius
 *
 */

@Repository("_userManager")
public class UserManagerDaoImpl extends NamedParameterJdbcDaoSupport implements UserManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( UserManagerDaoImpl.class.getName() );
	
	@Override
	public List<User> getAllUsers()
	{
		List<User> result = new ArrayList<User>();

		try
		{
			result = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_USERS_GET_ALL, new MapSqlParameterSource(), new UserMapper() );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}

	@Override
	public int countUsers()
	{
		int result = getNamedParameterJdbcTemplate().queryForInt( SqlStatements.SQL_USERS_COUNT, new MapSqlParameterSource() );
		return result;
	}


	public List<Integer> getPersonUsersIds( Long personEventId )
	{
		List<Integer> result = new ArrayList<Integer>();

		String sql = SqlStatements.SQL_GET_PERSON_ADDITIONAL_SERVICES_IDS;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue( "PERSONS_EVENTS_ID", personEventId );
			result = this.getNamedParameterJdbcTemplate().queryForList( sql, params, Integer.class );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting technologies ids list. Exception is: " + e.getMessage() );
		}

		return result;
	}
	
	@Override
	public User authenticate( String username, String password )
	{

		if ( username == null || username.isEmpty() || password == null || password.isEmpty() )
		{
			_log.warn( "Username or password : NULL or empty!" );
			return null;
		}

		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_USERS_USERNAME, username );
		params.addValue( SqlNames.SQL_USERS_PASSWORD_APP, password );

		List<User> usersAuthentificated = null;
		User currentUser = null;
		try
		{
			usersAuthentificated = this.getNamedParameterJdbcTemplate().query( SqlStatements.USERS_GET_AUTHENTIFICATE_BY_USERNAME_AND_PASSWORD, params, new UserMapper() );
			if ( usersAuthentificated != null && !usersAuthentificated.isEmpty() )
			{
				currentUser = usersAuthentificated.get( 0 );
			}
		}
		catch ( Exception e )
		{
			_log.debug( e.getMessage() );
		}

		return currentUser;
	}

	@Override
	public void insertUser( User newUser )
	{
		if ( newUser == null )
		{
			_log.warn( "Bad parameter! (null user)" );
			return;
		}

		String sql = SqlStatements.SQL_USERS_INSERT_USER;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_USERS_USER_ID, null );
		params.addValue( SqlNames.SQL_USERS_FIRST_NAME, newUser.getFirstName() );
		params.addValue( SqlNames.SQL_USERS_LAST_NAME, newUser.getLastName() );
		params.addValue( SqlNames.SQL_USERS_USERNAME, newUser.getUsername() );
		params.addValue( SqlNames.SQL_USERS_PASSWORD_APP, newUser.getPassword() );
		params.addValue( SqlNames.SQL_USERS_USER_TYPE, newUser.getUserType() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
		}
	}

	@Override
	public void updateUser( User modifiedUser )
	{
		_log.info( "Inside updateUser method from: " + this.getClass().getName() );

		if ( modifiedUser == null || ModelConstants.NEW_USER_TYPE == modifiedUser.getUserType() )
		{
			_log.warn( "Bad parameter! (null user or no type)" );
			return;
		}

		MapSqlParameterSource params = new MapSqlParameterSource();

		params.addValue( SqlNames.SQL_USERS_FIRST_NAME, modifiedUser.getFirstName() );
		params.addValue( SqlNames.SQL_USERS_LAST_NAME, modifiedUser.getLastName() );
		params.addValue( SqlNames.SQL_USERS_USERNAME, modifiedUser.getUsername() );
		params.addValue( SqlNames.SQL_USERS_USER_TYPE, modifiedUser.getUserType() );
		params.addValue( SqlNames.SQL_USERS_USER_ID, modifiedUser.getUserID() );
		if ( ("").equals( modifiedUser.getPassword() ) )
		{
			getNamedParameterJdbcTemplate().update( SqlStatements.SQL_USERS_UPDATE_USER, params );
		}
		else
		{
			params.addValue( SqlNames.SQL_USERS_PASSWORD_APP, modifiedUser.getPassword() );
			getNamedParameterJdbcTemplate().update( SqlStatements.SQL_USERS_UPDATE_USER_WITH_PASSWORD, params );
		}
	}

	@Override
	public void updateCheckTypeUser( long userId, int userType )
	{
		_log.info( "Inside updateCheckTypeUser method from: " + userId + userType );

		if ( ModelConstants.ERRORNUMBER == userId || ModelConstants.ERRORNUMBERINT == userType )
		{
			_log.warn( "Bad parameter! (userId or userType is -1)" );
			return;
		}

		MapSqlParameterSource params = new MapSqlParameterSource();

		params.addValue( SqlNames.SQL_USERS_USER_ID, userId );
		params.addValue( SqlNames.SQL_USERS_USER_TYPE, userType );

		getNamedParameterJdbcTemplate().update( SqlStatements.SQL_USERS_UPDATE_TYPE, params );

	}
	
	public void insertUserEvent( long personEventId, User user )
	{

		String sql = SqlStatements.CVS_INSERT_USERS;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_PERSONS_EVENTS_ID_ADDITIONAL_SERVICES, personEventId );
		params.addValue( SqlNames.SQL_SET_PERSONS_EVENTS_USER_ID, user.getUserID() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}

	}

	public void deleteUserEvent( Integer id )
	{
		String sql = SqlStatements.SQL_DELETE_USER;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_USER_ID, id );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}

}
