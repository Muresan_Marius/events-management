package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.ApplicationSource;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.ApplicationSourceManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.ApplicationSourceMapper;

/**
 * MySql implementation for ApplicationSourceManagerDaoIntf
 * 
 * @author Muresan Marius
 * 
 */

@Repository("_applicationSourceManager")
public class ApplicationSourceManagerDaoImpl extends NamedParameterJdbcDaoSupport implements ApplicationSourceManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( ApplicationSourceManagerDaoImpl.class.getName() );

	@Override
	public List<ApplicationSource> getAllApplicationSources()
	{
		List<ApplicationSource> result = new ArrayList<ApplicationSource>();

		try
		{
			result = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_APPLIED_FROM_GET_ALL_APPLICATION_SOURCES, new MapSqlParameterSource(), new ApplicationSourceMapper() );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}

	@Override
	public int countApplicationSources()
	{
		int result = getNamedParameterJdbcTemplate().queryForInt( SqlStatements.SQL_APPLIED_FROM_COUNT_APPLICATION_SOURCES, new MapSqlParameterSource() );
		return result;
	}

}
