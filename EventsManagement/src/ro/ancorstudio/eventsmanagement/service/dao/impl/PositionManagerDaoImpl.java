package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.Position;
//import ro.ancorstudio.eventsmanagement.model.PositionRow;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.PositionManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.PositionMapper;
//import ro.ancorstudio.eventsmanagement.service.dao.mapper.PositionRowMapper;

/**
 * MySql implementation for PositionManagerDaoIntf
 * 
 * @author Muresan Marius
 * 
 */

@Repository("_positionManager")
public class PositionManagerDaoImpl extends NamedParameterJdbcDaoSupport implements PositionManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( PositionManagerDaoImpl.class.getName() );

	@Override
	public List<Position> getAllPositions()
	{
		List<Position> result = new ArrayList<Position>();

		try
		{
			result = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_PACKAGE_GET_ALL_PACKAGES, new MapSqlParameterSource(), new PositionMapper() );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}

	@Override
	public int countPositions()
	{
		int result = getNamedParameterJdbcTemplate().queryForInt( SqlStatements.SQL_PACKAGE_COUNT_PACKAGES, new MapSqlParameterSource() );
		return result;
	}


	public void insertOnlyPosition( Position newPosition )
	{
		String sql = SqlStatements.INSERT_ONLY_PACKAGE;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_PACKAGE_ID, null );
		params.addValue( SqlNames.SQL_PACKAGE_NAME, newPosition.getName() );
		params.addValue( SqlNames.SQL_PACKAGE_DESCRIPTION, newPosition.getDescription() );
		params.addValue( SqlNames.SQL_PACKAGE_PRICE, newPosition.getPrice() );
		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}


	public void updateOnlyPosition( Position modifiedPosition )
	{
		String sql = SqlStatements.UPDATE_ONLY_PACKAGE;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_PACKAGE_NAME, modifiedPosition.getName() );
		params.addValue( SqlNames.SQL_PACKAGE_DESCRIPTION, modifiedPosition.getDescription() );
		params.addValue( SqlNames.SQL_PACKAGE_PRICE, modifiedPosition.getPrice() );
		params.addValue( SqlNames.SQL_PACKAGE_ID, modifiedPosition.getId() );
		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}

}
