package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.service.dao.SqlNames;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.AdditionalServiceManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.AdditionalServiceMapper;

/**
 * MySql implementation for AdditionalServiceManagerDaoIntf
 * 
 * @author Muresan Marius
 * 
 */

@Repository("_additionalServiceManager")
public class AdditionalServiceManagerDaoImpl extends NamedParameterJdbcDaoSupport implements AdditionalServiceManagerDaoIntf
{

	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( AdditionalServiceManagerDaoImpl.class.getName() );

	@Override
	public List<AdditionalService> getAllAdditionalServices()
	{
		List<AdditionalService> result = new ArrayList<AdditionalService>();

		try
		{
			result = this.getNamedParameterJdbcTemplate().query( SqlStatements.SQL_ADDITIONAL_SERVICES_GET_ALL, new MapSqlParameterSource(), new AdditionalServiceMapper() );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}

		return result;
	}

	public List<Integer> getPersonAdditionalServicesIds( Long personEventId )
	{
		List<Integer> result = new ArrayList<Integer>();

		String sql = SqlStatements.SQL_GET_PERSON_ADDITIONAL_SERVICES_IDS;

		try
		{
			MapSqlParameterSource params = new MapSqlParameterSource();
			params.addValue( "PERSONS_EVENTS_ID", personEventId );
			result = this.getNamedParameterJdbcTemplate().queryForList( sql, params, Integer.class );
		}
		catch ( Exception e )
		{
			_log.error( "Error getting technologies ids list. Exception is: " + e.getMessage() );
		}

		return result;
	}

	@Override
	public int countAdditionalServices()
	{
		int result = getNamedParameterJdbcTemplate().queryForInt( SqlStatements.ADDITIONAL_SERVICES_COUNT, new MapSqlParameterSource() );
		return result;
	}

	public void insertAdditionalService( long personEventId, AdditionalService additionalService )
	{

		String sql = SqlStatements.CVS_INSERT_ADDITIONAL_SERVICES;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_SET_PERSONS_EVENTS_ID_ADDITIONAL_SERVICES, personEventId );
		params.addValue( SqlNames.SQL_SET_PERSONS_EVENTS_ADDITIONAL_SERVICE_ID, additionalService.getId() );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );

		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}

	}

	@Override
	public void deleteAdditionalService( Integer id )
	{
		String sql = SqlStatements.SQL_DELETE_ADDITIONAL_SERVICE;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue( SqlNames.SQL_ADDITIONAL_SERVICE_ID	, id );

		try
		{
			getNamedParameterJdbcTemplate().update( sql, params );
		}
		catch ( Exception e )
		{
			_log.warn( "invalid  " + e.getMessage() );
		}
	}
}
