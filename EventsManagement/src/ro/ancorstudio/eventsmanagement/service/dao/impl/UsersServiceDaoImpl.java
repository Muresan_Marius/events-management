package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.UsersService;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.UserMapper;

/**
 * Service for processing Users.
 **/
@Repository("_usersService")
public class UsersServiceDaoImpl extends SimpleJdbcDaoSupport implements UsersService
{

	@Override
	public int countUsers()
	{
		String sql = SqlStatements.SQL_USERS_COUNT;

		int count = getSimpleJdbcTemplate().queryForInt( sql, new MapSqlParameterSource() );

		return count;
	}

	@Override
	public List<User> getAllUsers( String sqlSort, String sqlLimit )

	{
		String sql = SqlStatements.SQL_USERS_GET_ALL + sqlSort + " " + sqlLimit;

		List<User> result = getSimpleJdbcTemplate().query( sql, new UserMapper() );

		return result;
	}
}
