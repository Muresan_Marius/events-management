package ro.ancorstudio.eventsmanagement.service.dao.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import ro.ancorstudio.eventsmanagement.model.ResultSetPerson;
import ro.ancorstudio.eventsmanagement.service.Managers;
import ro.ancorstudio.eventsmanagement.service.dao.SqlStatements;
import ro.ancorstudio.eventsmanagement.service.dao.intf.ResultSetPersonManagerDaoIntf;
import ro.ancorstudio.eventsmanagement.service.dao.mapper.ResultSetPersonMapper;

/**
 * MySql implementation for PersonManagerDaoIntf
 * 
 * @author Muresan Marius
 * @version 1.0
 *
 */

@Repository("_ResultSetPersonManager")
public class ResultSetPersonManagerDaoImpl extends NamedParameterJdbcDaoSupport implements ResultSetPersonManagerDaoIntf
{
	// logger for this class
	private static Logger	_log	= LoggerFactory.getLogger( ResultSetPersonManagerDaoImpl.class.getName() );

	/**
	 * Merge all rows of a Person from ResultSetPerson to a single one
	 * @param sqlFilters
	 * @param sqlSort
	 * @return
	 */
	private List<ResultSetPerson> getResultSet( String sqlFilters, String sqlSort, String sqlLimit )
	{
		_log.trace( "Entering method ResultSetPersonManagerDaoImpl.getResultSet()" );

		String filter = "";
		if ( !("").equals( sqlFilters ) )
		{
			filter = SqlStatements.SQL_SEARCH_CV_PAGE_FILTERS + sqlFilters + ") ";
		}
		String sql = SqlStatements.SQL_SEARCH_CV_PAGE + " " + filter + sqlSort + " " + sqlLimit;
		List<ResultSetPerson> result = this.getNamedParameterJdbcTemplate().query( sql, new MapSqlParameterSource(), new ResultSetPersonMapper() );


		//toResultSetPerson( partialResult );
		return result;
	}

	@Override
	public List<ResultSetPerson> getAllPersons( String SqlSort, String SqlLimit )
	{
		_log.trace( "Entering method ResultSetPersonManagerDaoImpl.getAllPersons()" );

		List<ResultSetPerson> finalResult = new LinkedList<ResultSetPerson>();
		//finalResult.
		try
		{
			finalResult = this.getResultSet( "", SqlSort, SqlLimit );
		}
		catch ( Exception e )
		{
			_log.debug( "Exception occured : " + e.getMessage() );
		}
		_log.trace( "Exiting method ResultSetPersonManagerDaoImpl.getAllPersons()" );

		return finalResult;
	}

	@Override
	public int countPersons( String sqlFilters )
	{
		_log.trace( "Entering method ResultSetPersonManagerDaoImpl.countPersons()" );

		String filter = "";
		if ( !("").equals( sqlFilters ) )
		{
			filter = SqlStatements.SQL_SEARCH_CV_PAGE_FILTERS + " where " + sqlFilters + ") ";
		}
		String sql = SqlStatements.SQL_COUNT_SEARCH_CV_PAGE + " " + filter;
		int finalResult = getNamedParameterJdbcTemplate().queryForInt( sql, new MapSqlParameterSource() );

		_log.trace( "Exiting method ResultSetPersonManagerDaoImpl.countPersons()" );

		return finalResult;

	}

	@Override
	public void executeTransactionalQueryList( List<String> queries )
	{
	}

	@Override
	public List<ResultSetPerson> getFilteredPersons( String SqlFilters, String SqlSort, String SqlLimit )
	{
		_log.trace( "Entering method ResultSetPersonManagerDaoImpl.getFilteredPersons()" );

		List<ResultSetPerson> finalResult = new LinkedList<ResultSetPerson>();
		try
		{
			finalResult = this.getResultSet( " where " + SqlFilters, SqlSort, SqlLimit );
		}
		catch ( Exception e )
		{
			_log.warn( "Exception occured : " + e.getMessage() );
		}
		_log.trace( "Exiting method ResultSetPersonManagerDaoImpl.getFilteredPersons()" );

		return finalResult;
	}
}
