package ro.ancorstudio.eventsmanagement.service;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.User;

/**
 * This interface defines database operations related to users objects.
 */

public interface UsersService
{

	public int countUsers();

	public List<User> getAllUsers( String sqlSort, String sqlLimit );

}
