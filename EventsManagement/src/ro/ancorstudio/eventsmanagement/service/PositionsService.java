package ro.ancorstudio.eventsmanagement.service;

import java.util.List;

import ro.ancorstudio.eventsmanagement.model.Position;

/**
 * This interface defines database operations related to positions objects.
 */

public interface PositionsService
{

	public int countPositions();

	public List<Position> getAllPositions( String sqlSort, String sqlLimit );

}
