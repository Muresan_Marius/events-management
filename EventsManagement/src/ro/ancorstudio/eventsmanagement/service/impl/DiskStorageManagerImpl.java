package ro.ancorstudio.eventsmanagement.service.impl;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import ro.ancorstudio.eventsmanagement.Constants;
import ro.ancorstudio.eventsmanagement.service.intf.DiskStorageManager;

/**
 * Implementation for DiskStorageManager 
 * 
 * @author Muresan Marius
 *
 */

@Component
@Repository("_diskStorageManager")
public class DiskStorageManagerImpl extends SimpleJdbcDaoSupport implements DiskStorageManager, ResourceLoaderAware
{

	// logger for this class
	private static Logger	_log			= Logger.getLogger( DiskStorageManagerImpl.class.getName() );

	// resource loader (helps to load projectconfig.properties)
	private ResourceLoader	_resourceLoader;

	// the root folder for keeping the documents
	private String			_rootDirectory	= "CV_Documents";

	/**
	 * @return the _rootDirectory
	 */
	public String getRootDirectory()
	{
		return _rootDirectory;
	}

	/**
	 * @param _rootDirectory the _rootDirectory to set
	 */
	public void setRootDirectory( String _rootDirectory )
	{
		this._rootDirectory = _rootDirectory;
	}

	@Override
	public void uploadFile( MultipartFile file, String fileName )
	{

		File destination = new File( this.getBaseFolder() + "/" + fileName );

		if ( !destination.isFile() )
		{
			// file doesn't exist, it can be created
			try
			{
				file.transferTo( destination );
			}
			catch ( Exception e )
			{
				_log.warn( "Error uploading file : " + file.getOriginalFilename() + " to " + destination.getName() + ". Message is: " + e.getMessage() );
			}
			return;
		}

		// File exists, it won't be overriden
		_log.warn( "File : " + file.getOriginalFilename() + " already exists!" );
	}

	@Override
	public void setResourceLoader( ResourceLoader resourceLoader )
	{
		this._resourceLoader = resourceLoader;

		// there is a valid resource loader, so it can be used
		Properties properties = new Properties();
		// read configuration from properties file
		try
		{
			Resource resource = this.getResource( "classpath:" + Constants.PROJECT_PROPERTIES );
			InputStream is = resource.getInputStream();
			properties.load( is );
			is.close();
		}
		catch ( Exception exception )
		{
			_log.error( exception.getMessage() );
			return;
		}
		_rootDirectory = properties.getProperty( Constants.FILE_SERVER_ROOT_DIR );

		if ( isFolderExistent( getBaseFolder() ) )
		{
			_log.debug( "Initial folder structure is ok!" );
		}

	}

	protected Resource getResource( String location )
	{
		return this._resourceLoader.getResource( location );
	}

	/**
	 * Return the base folder 
	 */
	@Override
	public String getBaseFolder()
	{

		String result = System.getProperty( "catalina.base" ) + "/" + this._rootDirectory;

		return result;
	}

	/**
	 * Method to test and create the folders in the folderPath
	 */
	@Override
	public boolean isFolderExistent( String folderPath )
	{
		boolean exists = false;
		File testing = new File( folderPath );
		try
		{
			exists = testing.exists();
		}
		catch ( SecurityException exception )
		{
			_log.info( "Error testing folder existence of " + folderPath + ". Message is: " + exception.getMessage() );
		}

		if ( !exists )
		{
			try
			{
				exists = testing.mkdirs();
			}
			catch ( SecurityException exception )
			{
				_log.info( "Error trying to create " + folderPath + ". Message is: " + exception.getMessage() );
			}
		}

		return exists;
	}

	public void addDatabase()
	{
		String sql = "CREATE DATABASE `CVMANAGEMENT` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
		getSimpleJdbcTemplate().getJdbcOperations().execute( sql );
	}

}
