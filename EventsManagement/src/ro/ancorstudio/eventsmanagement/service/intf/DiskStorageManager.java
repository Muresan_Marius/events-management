package ro.ancorstudio.eventsmanagement.service.intf;

import org.springframework.web.multipart.MultipartFile;

/**
 * Interface used for specific disk operations
 * 
 * @author Muresan Marius
 *
 */

public interface DiskStorageManager
{

	public void uploadFile( MultipartFile file, String fileName );

	public String getBaseFolder();

	public boolean isFolderExistent( String folderPath );

	public void addDatabase();

}
