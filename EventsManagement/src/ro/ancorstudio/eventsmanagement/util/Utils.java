package ro.ancorstudio.eventsmanagement.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;

public class Utils
{

	private static Logger	_log	= LoggerFactory.getLogger( Utils.class.getName() );

	/**
	 * 
	 * @param end - the final Date
	 * @param start - the initial Date
	 * @return a formatted string
	 */

	public static String formatTimeDiff( Date end, Date start )
	{
		if ( end == null || start == null )
		{
			return "";
		}
		long miliseconds = end.getTime() - start.getTime();
		if ( miliseconds < 0 )
		{
			return "";
		}
		String format = String.format( "%%0%dd", 2 );
		String formatMs = String.format( "%%0%dd", 3 );
		long seconds = miliseconds / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		miliseconds = miliseconds % 1000;
		seconds = seconds % 60;
		minutes = minutes % 60;
		String milis = String.format( formatMs, miliseconds );
		String secs = String.format( format, seconds );
		String mins = String.format( format, minutes );
		String hrs = String.format( format, hours );
		String time = hrs + ":" + mins + ":" + secs + ":" + milis;
		return time;
	}

	public static Locale getLocale( HttpServletRequest request )
	{

		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver( request );
		if ( localeResolver == null )
		{
			return new Locale( Locale.ENGLISH.getLanguage() );
		}
		Locale locale = localeResolver.resolveLocale( request );
		return locale;
	}

	public static boolean isSupportedBrowser( HttpServletRequest request )
	{
		String userAgentHeader = request.getHeader( "User-Agent" );
		if ( userAgentHeader == null || userAgentHeader.isEmpty() )
		{
			return false;
		}
		UserAgent userAgent = new UserAgent( userAgentHeader );

		Browser browser = userAgent.getBrowser();
		_log.trace( "Browser: " + browser.toString() );

		if ( (browser.getGroup() != Browser.FIREFOX) && (browser.getGroup() != Browser.CHROME) )
		{
			return false;
		}
		int majorVersion = -1;

		try
		{
			majorVersion = Integer.valueOf( userAgent.getBrowserVersion().getMajorVersion() );
		}
		catch ( Exception e )
		{
			return false;
		}

		_log.debug( "Browser version: " + majorVersion );
		if ( majorVersion < 16 )
		{
			//FIREFOX less than 16
			return false;
		}

		//all OK
		return true;
	}

	public static Date createDate( String date, String hour )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );
		Date result = new Date();
		try
		{
			result = sdf.parse( date + " " + hour );
		}
		catch ( ParseException e )
		{
		}
		return result;
	}

	public static Date createDate( String date )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss" );
		Date result = new Date();
		try
		{
			result = sdf.parse( date + " 00:00:00" );
		}
		catch ( ParseException e )
		{
		}
		return result;
	}
}
