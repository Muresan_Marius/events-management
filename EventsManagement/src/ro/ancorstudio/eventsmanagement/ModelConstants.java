package ro.ancorstudio.eventsmanagement;

/**
 * This class stores constants values for the model in the Spring MVC
 * @author Muresan Marius
 * @version 1.0
 *
 */

public interface ModelConstants
{
	//constants for persons
	public static final long	NEW_PERSON_ID		= -1;
	//how many persons will we insert for testing?
	public static final int		NUM_PERSONS			= 10000;

	//constants for users
	public static final String	NUMBER_OF_USERS		= "count";
	public static final String	LIST_OF_USERS		= "users";
	public static final long	NEW_USER_TYPE		= -1;
	public static final String	MODEL_CURRENT_USER	= "currentUser";

	//constants for cvs
	public static final long	NEW_ID				= -1;

	public static final String	YEAR				= "year";
	public static final String	ERRORMESSAGE		= "error_message";

	public static final int		ERRORNUMBER			= -1;
	public static final int		ERRORNUMBERINT		= -1;
}
