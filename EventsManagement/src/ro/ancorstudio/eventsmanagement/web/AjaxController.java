package ro.ancorstudio.eventsmanagement.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import ro.ancorstudio.eventsmanagement.model.Event;
import ro.ancorstudio.eventsmanagement.model.Location;
import ro.ancorstudio.eventsmanagement.model.PersonExtendedRow;
import ro.ancorstudio.eventsmanagement.model.ApplicationSource;
import ro.ancorstudio.eventsmanagement.service.Managers;


@RemoteProxy(name = "dwrService")
public class AjaxController
{
	// logger for this controller
	private static Logger	_log	= Logger.getLogger( AjaxController.class.getName() );

	public AjaxController()
	{
		_log.debug( "Ajax controller created" );
	}
	
	@RemoteMethod
	public List<Event> getPersonEvents( int personId )
	{
		List<Event> events = Managers.getInstance().getEventManagerDaoIntf().getAllPersonEvents( personId );
		_log.warn(events);
		return events;
	}

	@RemoteMethod
	public long getPersonId( String lastName, String firstName, String email, String phone )
	{
		long result = Managers.getInstance().getPersonManagerDaoIntf().getPersonId( lastName, firstName, email, phone );

		return result;
	}

	@RemoteMethod
	public boolean cleanExportDirectory()
	{
		_log.debug( "Entering method cleanDirectory()" );
		String directory = Managers.getInstance().loadProperty( "FILE_EXPORT_NAME" );
		try
		{
			CleanDirectory( directory );
		}
		catch ( Exception e )
		{
			_log.debug( "Error at clean:" + e.getMessage() );
			return false;
		}
		_log.debug( "Exiting method cleanDirectory()" );
		return true;
	}

	private void CleanDirectory( String directory )
			throws Exception
	{
		String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
		String applicationPath = Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/";
		String path = absolutePath + applicationPath + directory;
		File folder = new File( path );
		File[] listOfFiles = folder.listFiles();
		if ( listOfFiles != null ){
			for ( int i = 0; i < listOfFiles.length; i++ )
			{
				if ( listOfFiles[i].isFile() )
				{
					listOfFiles[i].delete();
				}
			}
		}
	}

	@RemoteMethod
	public boolean exportDatabase()
	{
		_log.debug( "Entering method exportDatabase()" );
		try
		{
			//String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
			//String applicationPath = Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/";
			Path path = Paths.get("D:/Workspace eclipse2/EventsManagement/DataBase");
			String username = Managers.getInstance().loadProperty( "jdbc.username", "jdbc.properties" );
			String password = Managers.getInstance().loadProperty( "jdbc.password", "jdbc.properties" );
			String address = this.getAddress( Managers.getInstance().loadProperty( "jdbc.url", "jdbc.properties" ) );
			// 7z -oC:\apa e C:\export-data.zip
			//String filePath = absolutePath + applicationPath + "/" + Managers.getInstance().loadProperty( "FILE_EXPORT_NAME" ) + "/" + Managers.getInstance().loadProperty( "FILE_SQL_NAME" );
			String command = "C:/Program Files/MySQL/MySQL Server 5.6/bin/mysqldump --user= " + username + " -h " + address + " --password=[" + password +"] --max_allowed_packet=64M --add-drop-table --disable-keys --extended-insert --quick eventsmanagement > D:/Workspace eclipse2/EventsManagement/DataBase/db.sql";
			//String command = "mysqldump -u " + username + " -h " + address + " -p " + password + " -- eventsmanagement";
		//	+" --max_allowed_packet=64M --add-drop-table --disable-keys --extended-insert --quick cvmanagement"
			Process p = Runtime.getRuntime().exec( command );
		}
		catch ( Exception e )
		{
			_log.debug( "Error at export:" + e.getMessage() );
			return false;
		}
		_log.debug( "Exiting method exportDatabase()" );
		return true;

	}

	@RemoteMethod
	public boolean moveCvFiles()
	{
		_log.debug( "Entering method moveCvFiles()" );
		String directory = Managers.getInstance().loadProperty( "FILE_EXPORT_NAME" );
		try
		{
			String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
			String applicationPath = Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/";
			String path = absolutePath + applicationPath + Managers.getInstance().loadProperty( "FILE_SERVER_CVS_DIR" );
			File folder = new File( path );
			File[] listOfFiles = folder.listFiles();

			for ( int i = 0; i < listOfFiles.length; i++ )
			{
				if ( listOfFiles[i].isFile() )
				{
					File newFile = new File( absolutePath + applicationPath + directory + "/" + listOfFiles[i].getName() );
					InputStream in = new FileInputStream( listOfFiles[i] );
					OutputStream out = new FileOutputStream( newFile );

					byte[] moveBuff = new byte[1024];

					int butesRead;

					while ( (butesRead = in.read( moveBuff )) > 0 )
					{
						out.write( moveBuff, 0, butesRead );
					}

					in.close();
					out.close();
				}
			}
		}
		catch ( Exception e )
		{
			_log.debug( "Error at clean:" + e.getMessage() );
			return false;
		}
		_log.debug( "Exiting method moveCvFiles()" );
		return true;
	}

	@RemoteMethod
	public boolean archiveDirectory()
	{
		_log.debug( "Entering method archiveDirectory()" );

		Path path = Paths.get("D:/Workspace eclipse2/EventsManagement/DataBase/db");
		String command = "C:/7z.exe a " + path +  ".zip " + path + "/*";
		try
		{
			Process process = Runtime.getRuntime().exec( command );
			process.waitFor();
		}
		catch ( Exception e )
		{
			_log.debug( "Error at clean:" + e.getMessage() );
			return false;
		}
		_log.debug( "Exiting method archiveDirectory()" );
		return true;
	}

	@RemoteMethod
	public boolean extractArchive()
	{
		_log.debug( "Entering method extractArchive()" );

		String directory = Managers.getInstance().loadProperty( "FOLDER_IMPORT_NAME" );
		String filename = Managers.getInstance().loadProperty( "FILE_EXPORT_NAME" ) + ".zip";

		String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
		String applicationPath = Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/";
		String path = absolutePath + applicationPath;
		String command = path + "WEB-INF/" + "lib/7z.exe -o" + path + "/" + directory + " e " + path + "/" + filename;
		try
		{
			Process process = Runtime.getRuntime().exec( command );
			process.waitFor();
		}
		catch ( Exception e )
		{
			_log.debug( "Error at clean:" + e.getMessage() );
			return false;
		}
		_log.debug( "Exiting method extractArchive()" );
		return true;
	}

	@RemoteMethod
	public boolean removeUnusedFiles()
	{
		_log.debug( "Entering method removeUnusedFiles()" );
		String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
		String applicationPath = Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/";
		String path = absolutePath + applicationPath + Managers.getInstance().loadProperty( "FILE_SERVER_CVS_DIR" );

		File f = new File( path + "/" + Managers.getInstance().loadProperty( "FILE_SQL_NAME" ) );
		f.delete();
		File f2 = new File( path + "/" + Managers.getInstance().loadProperty( "FILE_BATCH_IMPORT_NAME" ) );
		f2.delete();
		_log.debug( "Exiting method removeUnusedFiles()" );
		return true;
	}
	
	private String getAddress( String url )
	{
		String[] result = new String[1];
		String[] partialResult = url.split( "/" );
		if ( partialResult != null )
		{
			if ( partialResult.length > 2 )
			{
				result = partialResult[2].split( ":" );
			}
		}
		return (result != null) ? ((result.length > 1) ? result[0] : "") : "";
	}
}


