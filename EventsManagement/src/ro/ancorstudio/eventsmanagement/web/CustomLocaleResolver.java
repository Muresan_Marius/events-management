package ro.ancorstudio.eventsmanagement.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.i18n.CookieLocaleResolver;

/**
 * This is the CustomLocaleResolver
 * 
 * @author  Muresan Marius
 * @version 1.0
 * 
 */

public class CustomLocaleResolver extends CookieLocaleResolver
{

	/**
	    * Determine the default locale for the given request,
	     * Called if no locale cookie has been found.
	     * <p>The default implementation returns the specified default locale,
	     * if any, else falls back to the request's accept-header locale.
	     * @param request the request to resolve the locale for
	     * @return the default locale (never <code>null</code>)
	     * @see #setDefaultLocale
	 * @see javax.servlet.http.HttpServletRequest#getLocale()
	     */

	@Override
	public Locale determineDefaultLocale( HttpServletRequest request )
	{
		String browserLanguage = request.getHeader( "accept-language" );

		if ( browserLanguage.startsWith( Locale.ENGLISH.getLanguage()) )
		{
			return new Locale( Locale.ENGLISH.getLanguage() );
		}
		else if ( browserLanguage.startsWith( "RO" ) )
		{
			return new Locale( "RO" );
		}

		return new Locale( Locale.ENGLISH.getLanguage() );

	}
}
