package ro.ancorstudio.eventsmanagement.web.controller.data;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ro.ancorstudio.eventsmanagement.ModelConstants;
import ro.ancorstudio.eventsmanagement.RequestConstants;
import ro.ancorstudio.eventsmanagement.model.CustomResponse;
import ro.ancorstudio.eventsmanagement.model.Position;
import ro.ancorstudio.eventsmanagement.service.Managers;
import ro.ancorstudio.eventsmanagement.util.Utils;

/**
 * 
 * Data Controller implemented using annotations
 * 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

@Controller
@RequestMapping("/positionData.htm")
public class PositionData
{

	// logger for this controller
	private static Logger	_log	= Logger.getLogger( UserData.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	CustomResponse<Position> getAll( @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx, @RequestParam(value = "sord", required = false) String sord, HttpServletResponse response )
	{

		_log.trace( "Entering method PositionData.getAll(). Requested page is: >" + page + "<" );

		CustomResponse<Position> result = new CustomResponse<Position>();
		Date startDate = new Date();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession( true );

		insertData( attr.getRequest() );

		try
		{
			Integer limit = rows;
			if ( limit < 0 )
			{
				limit = 0;
			}
			Integer start = limit * (page - 1);
			if ( start < 0 )
			{
				start = 0;
			}

			int countData = Managers.getInstance().getPositionsService().countPositions();
			//int countData = Managers.getInstance().getPositionManagerDaoIntf().countPositions();
			
			String sqlSort = "";
			String sqlLimit = "LIMIT " + start.toString() + ", " + limit.toString();

			if ( !("").equals( sidx ) )
			{
				sqlSort = "order by " + sidx + " " + sord;
			}

			List<Position> results = Managers.getInstance().getPositionsService().getAllPositions( sqlSort, sqlLimit );

//			List<Position> results = Managers.getInstance().getPositionManagerDaoIntf().getAllPositions(sqlSort, sqlLimit);
			
			// Assign the result from the service to this response
			result.setRows( results );
			//Assign the total number of records found. This is used for paging
			result.setRecords( Integer.toString( countData ) );
			result.setPage( Integer.toString( page ) );
			double totalPages = Math.ceil( countData / rows );
			int pages = (int) totalPages + 1;
			if ( countData % rows == 0 )
			{
				pages = (int) totalPages;
			}

			result.setTotal( Integer.toString( pages ) );

		}
		catch ( Exception e )
		{
			//_log.error( 
			e.getStackTrace();// );
		}
		Date endDate = new Date();
		_log.trace( "Exiting method getAll(). Time spent: " + Utils.formatTimeDiff( endDate, startDate ) );

		return result;

	}

	public void insertData( HttpServletRequest request )
	{
		_log.debug( "Entering method processSubmit" );
		//check the action
		String action = ServletRequestUtils.getStringParameter( request, RequestConstants.ACTION_NAME, "" );
		//if append an error
		if ( action.isEmpty() )
		{
			//nothing to do, no action received !!
			_log.error( "No action received in 'positiont.htm' screen! " );
		}
		else if ( action.equalsIgnoreCase( RequestConstants.ACTION_INSERT_POSITION ) )
		{
			String positionName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_NAME, "" );
			String positionDescription = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_DESCRIPTION, "" );
			String positionPrice = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_PRICE, "" );

			Position newPosition = new Position();
			newPosition.setName(positionName);
			newPosition.setDescription( positionDescription );
			newPosition.setPrice(positionPrice);
			
			Managers.getInstance().getPositionManagerDaoIntf().insertOnlyPosition( newPosition );
		}
		else if ( action.equalsIgnoreCase( RequestConstants.ACTION_UPDATE_POSITION ) )
		{
			//if the action is to update the form above
			long position_id = ServletRequestUtils.getLongParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_NAME, ModelConstants.ERRORNUMBER );
			String positionName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_NAME, "" );
			String positionDescription = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_DESCRIPTION, "" );
			String positionPrice = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PACKAGE_PRICE, "" );
			
			Position modifiedPosition = new Position();
			modifiedPosition.setId(position_id);
			modifiedPosition.setName(positionName);
			modifiedPosition.setDescription( positionDescription );
			modifiedPosition.setPrice(positionPrice);

			Managers.getInstance().getPositionManagerDaoIntf().updateOnlyPosition( modifiedPosition );
		}
		else
		{
			//unexpected case
			_log.error( "Unexpected action in 'userst.htm' screen! " + action );
		}
		_log.debug( "Exiting method processSubmit" );

	}

}