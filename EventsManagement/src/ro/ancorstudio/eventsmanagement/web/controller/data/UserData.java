package ro.ancorstudio.eventsmanagement.web.controller.data;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ro.ancorstudio.eventsmanagement.ModelConstants;
import ro.ancorstudio.eventsmanagement.RequestConstants;
import ro.ancorstudio.eventsmanagement.model.CustomResponse;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.Managers;
import ro.ancorstudio.eventsmanagement.service.UsersService;
import ro.ancorstudio.eventsmanagement.util.Utils;

/**
 * 
 * Data Controller implemented using annotations
 * 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

@Controller
@RequestMapping("/userData.htm")
public class UserData
{

	// logger for this controller
	private static Logger	_log			= Logger.getLogger( UserData.class.getName() );
	private UsersService	usersService	= null;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	CustomResponse<User> getAll( @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx, @RequestParam(value = "sord", required = false) String sord, HttpServletResponse response )
	{

		_log.trace( "Entering method UserData.getAll(). Requested page is: >" + page + "<" );

		CustomResponse<User> result = new CustomResponse<User>();
		Date startDate = new Date();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession( true );

		this.insertData( attr.getRequest() );

		try
		{
			Integer limit = rows;
			if ( limit < 0 )
			{
				limit = 0;
			}

			Integer start = limit * (page - 1);
			if ( start < 0 )
			{
				start = 0;
			}

			int countData = usersService.countUsers();

			String sqlSort = "";
			String sqlLimit = "LIMIT " + start.toString() + ", " + limit.toString();

			if ( !("").equals( sidx ) )
			{
				sqlSort = "order by " + sidx + " " + sord;
			}

			List<User> results = usersService.getAllUsers( sqlSort, sqlLimit );

			// Assign the result from the service to this response
			result.setRows( results );
			//Assign the total number of records found. This is used for paging
			result.setRecords( Integer.toString( countData ) );
			result.setPage( Integer.toString( page ) );
			double totalPages = Math.ceil( countData / rows );
			int pages = (int) totalPages + 1;
			if ( countData % rows == 0 )
			{
				pages = (int) totalPages;
			}

			result.setTotal( Integer.toString( pages ) );

		}
		catch ( Exception e )
		{
			_log.error( e.getMessage() );
		}
		Date endDate = new Date();
		_log.trace( "Exiting method getAll(). Time spent: " + Utils.formatTimeDiff( endDate, startDate ) );

		return result;

	}

	public void insertData( HttpServletRequest request )
	{
		_log.debug( "Entering method processSubmit" );
		//check the action
		String action = ServletRequestUtils.getStringParameter( request, RequestConstants.ACTION_NAME, "" );
		//if append an error
		if ( action.isEmpty() )
		{
			//nothing to do, no action received !!
			_log.error( "No action received in 'userst.htm' screen! " );
		}
		else if ( action.equalsIgnoreCase( RequestConstants.ACTION_UPDATE_USER_RIGHTS ) )
		{
			//if the action is to update User Rights 
			//set the user_id and check_type with the new values for the id and user type
			long user_id = ServletRequestUtils.getLongParameter( request, RequestConstants.ID_NAME, ModelConstants.ERRORNUMBER );
			int check_type = ServletRequestUtils.getIntParameter( request, RequestConstants.CHECK_TYPE, ModelConstants.ERRORNUMBERINT );

			Managers.getInstance().getUserManagerDaoIntf().updateCheckTypeUser( user_id, check_type );
		}
		else if ( action.equalsIgnoreCase( RequestConstants.ACTION_INSERT_USER ) )
		{
			//if the action is to insert the form above
			String lName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LNAME, "" );
			String fName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_FNAME, "" );
			String username = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_USERNAME, "" );
			String password = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PASSWORD, "" );
			String userTypeString = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_USER_TYPE, "" );
			int userType;

			// 1 = full rights, 2 = read only
			if ( ("fullRights").equals( userTypeString ) )
			{
				userType = 1;
			}
			else
			{
				userType = 2;
			}

			User user = new User();
			user.setFirstName( fName );
			user.setLastName( lName );
			user.setPassword( password );
			user.setUsername( username );
			user.setUserType( userType );

			Managers.getInstance().getUserManagerDaoIntf().insertUser( user );
		}
		else if ( action.equalsIgnoreCase( RequestConstants.ACTION_UPDATE_USER ) )
		{
			//if the action is to update the form above
			long user_id = ServletRequestUtils.getLongParameter( request, RequestConstants.UPDATE_ID, ModelConstants.ERRORNUMBER );
			String lName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LNAME, "" );
			String fName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_FNAME, "" );
			String username = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_USERNAME, "" );
			String password = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PASSWORD, "" );
			String userTypeString = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_USER_TYPE, "" );
			int userType;

			// 1 = full rights, 2 = read only
			if ( ("fullRights").equals( userTypeString ) )
			{
				userType = 1;
			}
			else
			{
				userType = 2;
			}

			User modifiedUser = new User();
			modifiedUser.setUserID( user_id );
			modifiedUser.setFirstName( fName );
			modifiedUser.setLastName( lName );
			modifiedUser.setUsername( username );
			modifiedUser.setUserType( userType );
			if ( "" != password )
			{
				modifiedUser.setPassword( password );
			}
			else
			{
				modifiedUser.setPassword( "" );
			}
			Managers.getInstance().getUserManagerDaoIntf().updateUser( modifiedUser );
		}
		else
		{
			//unexpected case
			_log.error( "Unexpected action in 'userst.htm' screen! " + action );
		}
		_log.debug( "Exiting method processSubmit" );

	}

	@Autowired
	public void setPersonService( UsersService service )
	{
		this.usersService = service;
	}
}
