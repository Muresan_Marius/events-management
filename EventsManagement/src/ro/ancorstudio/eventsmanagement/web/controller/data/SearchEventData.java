package ro.ancorstudio.eventsmanagement.web.controller.data;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.model.CustomResponse;
import ro.ancorstudio.eventsmanagement.model.ResultSetPerson;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.Managers;
import ro.ancorstudio.eventsmanagement.util.Utils;

/**
 * 
 * Data Controller implemented using annotations
 * 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

@Controller
@RequestMapping("/search_eventData.htm")
public class SearchEventData
{

	// logger for this controller
	private static Logger	_log	= Logger.getLogger( SearchEventData.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody
	CustomResponse<ResultSetPerson> getAll( @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx, @RequestParam(value = "sord", required = false) String sord, HttpServletResponse response )
			throws ParsePropertyException, IOException
	{

		_log.trace( "Entering method SearchEventData.getAll(). Requested page is: >" + page + "<" );
		CustomResponse<ResultSetPerson> result = new CustomResponse<ResultSetPerson>();
		Date startDate = new Date();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession( true );
		try
		{
			Integer limit = rows;
			if ( limit < 0 )
			{
				limit = 0;
			}

			Integer start = limit * (page - 1);
			if ( start < 0 )
			{
				start = 0;
			}

			String sqlFilters = ServletRequestUtils.getStringParameter( attr.getRequest(), "SqlFilters", "" );
			int countData = Managers.getInstance().getResultSetPersonManagerDaoIntf().countPersons( sqlFilters );
			//String exportExcel = ServletRequestUtils.getStringParameter( attr.getRequest(), "export_excel", "" );
			String sqlSort = "";
			String sqlLimit = "LIMIT " + start.toString() + "," + limit.toString();
			if ( !("").equals( sidx ) )
			{
				sqlSort = "order by " + sidx + " " + sord;
			}

			List<ResultSetPerson> results = new ArrayList<ResultSetPerson>();
			if ( !sqlFilters.equals( "" ) )
			{
				results = Managers.getInstance().getResultSetPersonManagerDaoIntf().getFilteredPersons( sqlFilters, sqlSort, sqlLimit );
			}
			else
			{
				results = Managers.getInstance().getResultSetPersonManagerDaoIntf().getAllPersons( sqlSort, sqlLimit );

			}

			// Assign the result from the service to this response
			result.setRows( results );

			//Assign the total number of records found. This is used for paging
			result.setRecords( Integer.toString( countData ) );
			result.setPage( Integer.toString( page ) );
			double totalPages = Math.ceil( countData / rows );
			int pages = (int) totalPages + 1;
			if ( countData % rows == 0 )
			{
				pages = (int) totalPages;
			}

			result.setTotal( Integer.toString( pages ) );

		}
		catch ( Exception e )
		{
			_log.error( e.getMessage() );
		}
		Date endDate = new Date();
		_log.trace( "Exiting method getAll(). Time spent: " + Utils.formatTimeDiff( endDate, startDate ) );

		return result;

	}
}
