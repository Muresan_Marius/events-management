package ro.ancorstudio.eventsmanagement.web.controller.main;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.service.Managers;

/**
 * 
 * Simple Controller implemented using annotations
 * 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

@Controller
@RequestMapping("/export.htm")
public class MainExportController
{
	private static Logger	_log	= LoggerFactory.getLogger( MainExportController.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException, IOException
	{

		_log.debug( "Entering method handleRequest" );
		ModelAndView modelView = new ModelAndView( "export" );
		modelView.addObject( "type", Managers.getInstance().loadProperty( "FILE_SERVER_TYPE" ) );
		if ( session.getAttribute( SessionConstants.SESSION_AUTHENTIFICATED_USER ) == null )
		{
			response.sendRedirect( "login.htm" );
		}
		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException
	{

		_log.debug( "Entering method processSubmit" );
		ModelAndView modelView = new ModelAndView( "export" );

		_log.debug( "Exiting method processSubmit" );
		return modelView;
	}

}
