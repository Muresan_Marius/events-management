package ro.ancorstudio.eventsmanagement.web.controller.main;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.nio.file.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
/*
import ro.bgsoft.cvmanagement.RequestConstants;
import ro.bgsoft.cvmanagement.SessionConstants;
import ro.bgsoft.cvmanagement.model.Blacklist;
import ro.bgsoft.cvmanagement.model.Interview;
import ro.bgsoft.cvmanagement.model.Interviewer;
import ro.bgsoft.cvmanagement.model.Language;
import ro.bgsoft.cvmanagement.model.PersonExtended;
import ro.bgsoft.cvmanagement.model.Position;
import ro.bgsoft.cvmanagement.model.Technology;
import ro.bgsoft.cvmanagement.model.User;
import ro.bgsoft.cvmanagement.service.Managers;
import ro.bgsoft.cvmanagement.util.Utils;*/












import ro.ancorstudio.eventsmanagement.RequestConstants;
import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.model.AdditionalService;
import ro.ancorstudio.eventsmanagement.model.Event;
import ro.ancorstudio.eventsmanagement.model.PersonExtended;
import ro.ancorstudio.eventsmanagement.model.Position;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.Managers;


/**
 * This is the controller for the "Insert EVENT" screen sub-menu
 * 
 * @author  Muresan Marius
 * @version 1.0
 * 
 */

@Controller
@RequestMapping("/insert_event.htm")
public class MainInsertEventController
{

	// logger for this controller
	private static Logger	_log	= LoggerFactory.getLogger( MainInsertEventController.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam(value = "personID", required = false) Integer personID,
			@RequestParam(value = "tab", required = false) Integer tab )
			throws ServletRequestBindingException, IOException
	{

		_log.debug( "Entering method handleRequest" );
		
		User user = (User) session.getAttribute( SessionConstants.SESSION_AUTHENTIFICATED_USER );
		if ( user == null )
		{
			response.sendRedirect( "login.htm" );
			return null;
		}

		ModelAndView modelView = new ModelAndView( "insert_event" );

		if ( tab != null )
		{
			modelView.addObject( "tab", tab );
		}
		else
		{
			modelView.addObject( "tab", 1 );
		}

		if ( personID != null )
		{
			modelView.addObject( "id", personID );
			modelView.addObject( "personExtended", Managers.getInstance().getPersonExtendedManagerDaoIntf().getPersonExtended( personID ) );
			//updatePerson( personID );
		}
		else
		{
			modelView.addObject( "id", "-1" );
		}

		modelView.addObject( "positionList", Managers.getInstance().getPositionManagerDaoIntf().getAllPositions() );
		modelView.addObject( "userList", Managers.getInstance().getUserManagerDaoIntf().getAllUsers() );
		modelView.addObject("eventList" , Managers.getInstance().getEventManagerDaoIntf().getAllEventsTypes() );
		modelView.addObject( "additionalServiceList", Managers.getInstance().getAdditionalServiceManager().getAllAdditionalServices() );
		modelView.addObject( "applicationSourceList", Managers.getInstance().getApplicationSourceManagerDaoIntf().getAllApplicationSources() );
		_log.debug( "Exiting method handleRequest" );
		return modelView;
		
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestParam(value = "cv_upload", required = false) CommonsMultipartFile cv_upload )
			throws ServletRequestBindingException, IOException, ParseException, JSONException
	{

		_log.debug( "Entering method processSubmit" );
		ModelAndView modelView = new ModelAndView( "insert_event" );

		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
		// update person
		String personId = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PERSON_ID, "-1" );
		String tab = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_TAB, "1" );

		String action = ServletRequestUtils.getStringParameter( request, "action", "" );

		if ( !personId.equals( "-1" ) )
		{
			try
			{ 
				if ( action.equals( "update_selections" ) )
				{
					this.updateSelections( request, Integer.parseInt( personId ) );
				}

				else
				{
					this.updatePerson( request, Integer.parseInt( personId ) );
					this.UpdateCvOnDisc( request, cv_upload, Integer.parseInt( personId ) );
				}
				response.sendRedirect( "insert_event.htm?personID=" + personId + "&tab=" + tab );
				return null;
			}
			catch ( Exception e )
			{
				_log.debug( "Exception occured:" + e.getMessage() );
			}
			return handleRequest( request, response, session, Integer.parseInt( personId ), 1 );
		}
	
		// Insert person
		String lastName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LAST_NAME, "" );
		String firstName = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_FIRST_NAME, "" );
		Date birthDate = null;
		try
		{
			birthDate = sdf.parse( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_BIRTH_DATE, "" ) );
		}
		catch ( Exception e )
		{
		}
		String email = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_EMAIL, "" );
		String phone = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PHONE_NUMBER, "" );
		String sex = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_SEX, "" );
		String observations = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PERSON_OBSERVATIONS ); 
		String address = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_ADDRESS ); 
		String locality = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LOCALITY ); 
		String cnp = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_CNP ); 
		String series = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_SERIES ); 
		Date inregistrationDate = null;
		try
		{
			inregistrationDate = sdf.parse( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_INREGISTRATION_DATE, "" ) );
		}
		catch ( Exception e )
		{
		}

		//Build the JSON Object
		String json = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_JSON_OBJECT );
		JSONObject objJSON = new JSONObject( json );

		//Build the PersonExtended object
		PersonExtended person = new PersonExtended();
		person.setLastName( lastName );
		person.setFirstName( firstName );
		person.setBirthDate(birthDate);
		person.setEmail( email );
		person.setPhone( phone );
		person.setSex( sex );
		person.setGeneralObservations( observations );
		person.setAddress(address);
		person.setLocality(locality);
		person.setCnp(cnp);
		person.setSeries(series);
		person.setInregistrationDate(inregistrationDate);
		person.setEvents( this.createEvent( request ) );

		Managers.getInstance().getPersonExtendedManagerDaoIntf().insertPersonExtended( person );
	
		int receivedPersonId = (int) Managers.getInstance().getPersonManagerDaoIntf().getPersonId( lastName, firstName, email, phone );
		
		// Contract upload
		this.UpdateCvOnDisc( request, cv_upload, receivedPersonId );

		modelView.addObject("eventList" , Managers.getInstance().getEventManagerDaoIntf().getAllEventsTypes() );
		modelView.addObject( "applicationSourceList", Managers.getInstance().getApplicationSourceManagerDaoIntf().getAllApplicationSources() );
		
		String submit = ServletRequestUtils.getStringParameter( request, "button", "1" );
		if ( submit.equals( "1" ) )
		{
			response.sendRedirect( "insert_event.htm?personID=" + receivedPersonId + "&tab=1" );
			return null;
		}
		modelView.addObject( "id", "-1" );
		modelView.addObject( "tab", "1" );
		return modelView;
	}
	
	private void updatePerson( HttpServletRequest request, int personId )
			throws ParseException, ServletRequestBindingException, JSONException
	{
		PersonExtended personExtended = new PersonExtended();
		personExtended.setId( personId );
		personExtended.setLastName( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LAST_NAME, "" ) );
		personExtended.setFirstName( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_FIRST_NAME, "" ) );
		personExtended.setPhone( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PHONE_NUMBER, "" ) );
		personExtended.setEmail( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_EMAIL, "" ) );
		personExtended.setSex( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_SEX, "" ) );
		personExtended.setGeneralObservations( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PERSON_OBSERVATIONS, "" ) );

		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );
		personExtended.setBirthDate( sdf.parse( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_BIRTH_DATE, "" ) ) );
		personExtended.setAddress(ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_ADDRESS, "" ) );
		personExtended.setLocality(ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LOCALITY, "" ) );
		personExtended.setCnp(ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_CNP, "" ) );
		personExtended.setSeries(ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_SERIES, "" ) );
		personExtended.setInregistrationDate(sdf.parse( ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_INREGISTRATION_DATE, "" ) ));

		personExtended.setEvents( this.createEvent( request ) );

		Managers.getInstance().getPersonExtendedManagerDaoIntf().updatePersonExtended( personExtended );
	}
	
	private Set<Event> createEvent( HttpServletRequest request )
			throws ServletRequestBindingException, JSONException
	{
		String json = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_JSON_OBJECT );
		SimpleDateFormat sdf = new SimpleDateFormat( "dd/MM/yyyy" );

		JSONObject objJSON = new JSONObject( json );

		Set<Event> eventsList = new HashSet<Event>();
		JSONArray arrayEvents = objJSON.getJSONArray( "events" );
		for ( int i = 0; i < arrayEvents.length(); i++ )
		{
			Event event = new Event();
			event.setId( arrayEvents.getJSONObject( i ).getInt( RequestConstants.REQUEST_PARAM_PERSONS_EVENT_ID ) );
			event.setEventId( arrayEvents.getJSONObject( i ).getInt( RequestConstants.REQUEST_PARAM_EVENT_ID ) );
			event.setDescriptionId(arrayEvents.getJSONObject( i ).getInt( RequestConstants.REQUEST_PARAM_EVENT_ID ));
			
			Date eventDate = null;
			try
			{
				eventDate = sdf.parse( arrayEvents.getJSONObject( i ).getString( RequestConstants.REQUEST_PARAM_EVENT_DATE ) );
			}
			catch ( Exception e )
			{
			}
			event.setEventDate(eventDate);

			event.setAppliedFromId( arrayEvents.getJSONObject( i ).getInt( RequestConstants.REQUEST_PARAM_POSITION_APPLICATION_SOURCE ) );
			event.setAppliedFromId(arrayEvents.getJSONObject( i ).getInt( RequestConstants.REQUEST_PARAM_EVENT_APPLICATION_SOURCE ));
			
			eventsList.add( event );
		}
		return eventsList;
	}
	
	private void updateSelections( HttpServletRequest request, int parseInt )
			throws JSONException
	{
		String json = ServletRequestUtils.getStringParameter( request, "selections_json", "" );
		JSONArray arrayEventsSelection = new JSONArray( json );
		Event event = new Event();
		for ( int i = 0; i < arrayEventsSelection.length(); i++ )
		{
			JSONObject obj = arrayEventsSelection.getJSONObject( i );

			
			event.setId( obj.getLong( "id" ) );
			event.setEventLocation(obj.getString( "eventLocation" ) );
			event.setPrice(obj.getString( "price" ) );
			event.setAdvance(obj.getString( "advance" ) );
			event.setDetails(obj.getString( "details" ) );
			Position position = new Position();
			position.setId(obj.getInt( "positionId" ) );
			event.setPosition(position );
			String ListAdditionaServices = obj.getString("additionalServicesIds");
			event.setAdditionalServices(this.createAdditionalServices( ListAdditionaServices ));
			String ListUsers = obj.getString("usersIds");
			event.setUsers(this.createUsers( ListUsers ));
			//String listAS = obj.getString("additionalServices");
			
			
			Managers.getInstance().getEventManagerDaoIntf().updateEventSelection(event);
		}
	}
	
	private HashSet<AdditionalService> createAdditionalServices( String ListAdditionalServices )
	{

		//String list_additionalServices = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LIST_ADDITIONAL_SERVICES, "" );
		HashSet<AdditionalService> additionalServicesList = new HashSet<AdditionalService>();
		if ( !"".equals( ListAdditionalServices ) )
		{
			String[] additionalServices = ListAdditionalServices.split( "," );
			for ( String id : additionalServices )
			{
				AdditionalService additionalService = new AdditionalService();
				additionalService.setId(Long.parseLong(id));
				//additionalService.setDescription(id);
				additionalServicesList.add( additionalService );
			}
		}
		return additionalServicesList;
	}
	

	private HashSet<User> createUsers( String ListUsers )
	{
		//String list_users = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LIST_USERS, "" );
		HashSet<User> usersList = new HashSet<User>();
		if ( !"".equals( ListUsers ) )
		{
			String[] users = ListUsers.split( "," );
			for ( String id : users )
			{
				User user = new User();
				user.setUserID(Long.parseLong(id));
				usersList.add( user );
			}
		}
		return usersList;
	}
	

	private void UpdateCvOnDisc( HttpServletRequest request, CommonsMultipartFile cv_upload, long personId )
			throws IllegalStateException, IOException
	{
		String cvPath = this.UploadFile( request, cv_upload, personId );
		if ( !cvPath.equals( "" ) )
		{

			Path document = Paths.get("D:/Workspace eclipse2/EventsManagement/documents");
			Managers.getInstance().getCvManagerDaoIntf().uploadCVPathByPersonID( personId, cvPath );
			File file = new File( document + "/" +cvPath );
			cv_upload.transferTo( file );

		}
	}

	private String UploadFile( HttpServletRequest request, CommonsMultipartFile cv_upload, long personId )
			throws IllegalStateException, IOException
	{
		System.out.println( "Saving file: " + cv_upload.getOriginalFilename() + " with the extension " + cv_upload.getContentType() );

		if ( !cv_upload.getOriginalFilename().equals( "" ) )
		{
			// Get first name, last name and replace spaces with underscores
			String cvFirstName = request.getParameter( "first_name" ).replaceAll( " ", "_" ).toLowerCase();
			String cvLastName = request.getParameter( "last_name" ).replaceAll( " ", "_" ).toLowerCase();

			String cvFileName = cv_upload.getOriginalFilename();

			// Get the CV extension
			String cvExtension = "";
			int i = cvFileName.lastIndexOf( '.' );
			if ( i > 0 )
			{
				cvExtension = cvFileName.substring( i + 1 );
			}

			// Build CV file name using a StringBuilder object
			StringBuilder sb = new StringBuilder();
			sb.append( cvLastName );
			sb.append( "_" );
			sb.append( cvFirstName );
			sb.append( "_" );
			sb.append( personId );
			sb.append( "." );
			sb.append( cvExtension );

			String cvFileNameFormatted = sb.toString();

			return cvFileNameFormatted;
		}
		return "";
	}

	private String getCvDirectory()
	{
		String absolutePath = System.getProperty( "catalina.base" ).replace( '\\', '/' ) + '/';
		Properties m = System.getProperties();
		String destinationPath = absolutePath + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT_DIR" ) + "/" + Managers.getInstance().loadProperty( "FILE_SERVER_PROJECT" ) + "/"
									+ Managers.getInstance().loadProperty( "FILE_SERVER_CVS_DIR" ) + "/";
		return destinationPath;
	}
}
