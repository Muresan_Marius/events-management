package ro.ancorstudio.eventsmanagement.web.controller.main;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.LocaleEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import ro.ancorstudio.eventsmanagement.ModelConstants;
import ro.ancorstudio.eventsmanagement.RequestConstants;
import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.Managers;

/**
 * 
 * This controller is used to authenticate the users.
 * 
 * @author Muresan Marius
 *
 */

@Controller
@RequestMapping("/login.htm")
public class MainLoginController
{

	private static Logger	_log	= LoggerFactory.getLogger( MainLoginController.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
	{
		_log.debug( "Entering method handleRequest" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView( "login" );

		//SET LOCALE
		// get the locale resolver
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver( request );
		String newLocaleName = ServletRequestUtils.getStringParameter( request, "language", "" );
		Locale fromRequest = localeResolver.resolveLocale( request );
		try
		{
			newLocaleName = fromRequest.getLanguage().toUpperCase();
			_log.debug( "Locale = " + newLocaleName );
			localeResolver.setLocale( request, response, fromRequest );
		}
		catch ( Exception e )
		{
			String language = "EN";
			newLocaleName = language;
		}

		modelView.addObject( SessionConstants.SESSION_LANGUAGE, newLocaleName );

		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws IOException
	{
		//processSubmit(HttpServletRequest request, HttpServletResponse response, HttpSession session, @RequestParam ("enseigneID") long enseigneID) throws ServletRequestBindingException {
		_log.debug( "Entering method processSubmit" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView();

		//SET LOCALE
		// get the locale resolver
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver( request );
		String newLocaleName = ServletRequestUtils.getStringParameter( request, "language", "" );
		modelView.setViewName( "login" );
		if ( !newLocaleName.equals( "" ) )
		{
			LocaleEditor localeEditor = new LocaleEditor();
			localeEditor.setAsText( newLocaleName );
			// set the new locale
			localeResolver.setLocale( request, response, (Locale) localeEditor.getValue() );
			modelView.addObject( SessionConstants.SESSION_LANGUAGE, newLocaleName );
		}
		else
		{
			String language = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_LANGUAGE_AUTH, "" );

			String username = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_USERNAME, "" );
			String password = ServletRequestUtils.getStringParameter( request, RequestConstants.REQUEST_PARAM_PASSWORD, "" );

			User currentUser = Managers.getInstance().getUserManagerDaoIntf().authenticate( username, password );
			
			if ( currentUser == null )
			{
				if ( ("RO").equals( language ) )
				{
					//String errors = "currentUser";
					String errors = Managers.getInstance().loadProperty( "MESSAGE_NOT_AUTHENTICATED", "message_ro.properties" );
					modelView.addObject( ModelConstants.ERRORMESSAGE, errors );
				}
				else
				{
					String errors = Managers.getInstance().loadProperty( "MESSAGE_NOT_AUTHENTICATED", "message_en.properties" );
					modelView.addObject( ModelConstants.ERRORMESSAGE, errors );
				}
				_log.debug( "Exiting method processSubmit: user " + username + " NOT authenticated" );
				return modelView;
			}
			else
			{
				session.setAttribute( SessionConstants.SESSION_AUTHENTIFICATED_USER, currentUser );
				response.sendRedirect( "search_events.htm" );
				return null;
			}
		}
		return modelView;
	}
}
