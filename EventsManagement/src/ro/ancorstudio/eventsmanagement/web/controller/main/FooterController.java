package ro.ancorstudio.eventsmanagement.web.controller.main;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ro.ancorstudio.eventsmanagement.ModelConstants;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is the controller for the "footer" 
 * @author Muresan Marius
 * @version 1.0
 * 
 */

@Controller
public class FooterController
{

	// logger for this controller
	private static Logger	_log	= LoggerFactory.getLogger( FooterController.class.getName() );

	@RequestMapping("/footer.htm")
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException
	{

		_log.debug( "Entering method handleRequest" );
		ModelAndView modelView = new ModelAndView( "footer" );

		Date data = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy" );
		String year = sdf.format( data );

		modelView.addObject( ModelConstants.YEAR, year );

		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

}
