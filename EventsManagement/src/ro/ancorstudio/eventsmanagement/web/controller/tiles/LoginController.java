package ro.ancorstudio.eventsmanagement.web.controller.tiles;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * This controller is used to authenticate the users.
 * 
 * @author Muresan Marius
 *
 */

@Controller
@RequestMapping("/logint.htm")
public class LoginController
{

	private static Logger	_log	= LoggerFactory.getLogger( LoginController.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
	{
		_log.debug( "Entering method handleRequest" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView( "logint" );

		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session )
	{
		_log.debug( "Entering method processSubmit" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView();
		//no name for the view, we never should be here...

		_log.debug( "Exiting method processSubmit" );
		return modelView;

	}

}
