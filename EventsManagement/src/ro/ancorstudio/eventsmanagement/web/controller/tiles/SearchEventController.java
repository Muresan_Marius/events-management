package ro.ancorstudio.eventsmanagement.web.controller.tiles;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.service.Managers;

/**
 * Controller for the "Search EVENTS" tile
 * 
 * @author Muresan Marius
 * @version 1.0
 *
 */

@Controller
@RequestMapping("/search_eventst.htm")
public class SearchEventController
{

	// logger for this controller
	private static Logger	_log	= LoggerFactory.getLogger( SearchEventController.class.getName() );

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException, IOException
	{

		_log.debug( "Entering method handleRequest" );
		ModelAndView modelView = new ModelAndView( "search_eventst" );

		if ( session.getAttribute( SessionConstants.SESSION_AUTHENTIFICATED_USER ) == null )
		{
			response.sendRedirect( "login.htm" );
		}

		this.addMultiselectLists( modelView );

		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException
	{

		_log.debug( "Entering method processSubmit" );
		ModelAndView modelView = new ModelAndView( "search_eventst" );

		this.addMultiselectLists( modelView );

		_log.debug( "Exiting method processSubmit" );
		return modelView;
	}

	private void addMultiselectLists( ModelAndView modelView )
	{

		modelView.addObject( "positionList", Managers.getInstance().getPositionManagerDaoIntf().getAllPositions() );
		modelView.addObject( "applicationSourceList", Managers.getInstance().getApplicationSourceManagerDaoIntf().getAllApplicationSources() );
		modelView.addObject( "userList", Managers.getInstance().getUserManagerDaoIntf().getAllUsers() );
		modelView.addObject("eventList" , Managers.getInstance().getEventManagerDaoIntf().getAllEventsTypes() );

	}
}
