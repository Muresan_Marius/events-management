package ro.ancorstudio.eventsmanagement.web.controller.tiles;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.LocaleEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import ro.ancorstudio.eventsmanagement.SessionConstants;
import ro.ancorstudio.eventsmanagement.model.User;
import ro.ancorstudio.eventsmanagement.service.UsersService;

/**
 * 
 * This controller is used to display users from database.
 * 
 * @author Muresan Marius
 *
 */

@Controller
@RequestMapping("/userst.htm")
public class UsersController
{
	private static Logger	_log			= LoggerFactory.getLogger( UsersController.class.getName() );

	private UsersService	usersService	= null;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws IOException
	{
		_log.debug( "Entering method handleRequest" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView( "userst" );

		User currentUser = (User) session.getAttribute( SessionConstants.SESSION_AUTHENTIFICATED_USER );
		if ( currentUser == null )
		{
			response.sendRedirect( "./login.htm" );
			return null;
		}

		//SET LOCALE
		// get the locale resolver
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver( request );
		String newLocaleName = ServletRequestUtils.getStringParameter( request, "language", "" );
		if ( !newLocaleName.equals( "" ) )
		{
			LocaleEditor localeEditor = new LocaleEditor();
			localeEditor.setAsText( newLocaleName );
			// set the new locale
			localeResolver.setLocale( request, response, (Locale) localeEditor.getValue() );
		}
		else
		{
			Locale fromRequest = localeResolver.resolveLocale( request );
			try
			{
				newLocaleName = fromRequest.getLanguage().toUpperCase();
				_log.debug( "Locale = " + newLocaleName );
				localeResolver.setLocale( request, response, fromRequest );
			}
			catch ( Exception e )
			{
				String language = "EN";
				newLocaleName = language;
			}

		}

		modelView.addObject( SessionConstants.SESSION_LANGUAGE, newLocaleName );

		_log.debug( "Exiting method handleRequest" );
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws ServletRequestBindingException
	{

		_log.debug( "Entering method processSubmit" );
		ModelAndView modelView = new ModelAndView( "users" );

		_log.debug( "Exiting method processSubmit" );
		return modelView;
	}

	@Autowired
	public void setPersonService( UsersService service )
	{
		this.usersService = service;
	}
}