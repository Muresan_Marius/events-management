package ro.bgsoft.cvmanagement.web.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.LocaleEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import ro.bgsoft.cvmanagement.SessionConstants;

/**
 * 
 * Simple Controller implemented using annotations
 * 
 * @author Lucian Vrabiescu
 * @version 1.0
 * 
 */

@Controller
public class IndexController
{

	private static Logger	_log	= LoggerFactory.getLogger( IndexController.class.getName() );

	/**
	 * This method handles the request and forwards to welcome page
	 * 
	 * @return {@link ModelAndView} containing the welcome page and the data to display
	 * @throws IOException 
	 */
	@RequestMapping("/index.htm")
	public ModelAndView indexHandler( HttpServletRequest request, HttpServletResponse response, HttpSession session )
			throws IOException
	{
		_log.debug( "Entering method indexHandler" );

		// create Model And View object
		ModelAndView modelView = new ModelAndView();

		//SET LOCALE
		// get the locale resolver
		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver( request );
		String newLocaleName = ServletRequestUtils.getStringParameter( request, "language", "" );
		if ( !newLocaleName.equals( "" ) )
		{
			LocaleEditor localeEditor = new LocaleEditor();
			localeEditor.setAsText( newLocaleName );
			// set the new locale
			localeResolver.setLocale( request, response, (Locale) localeEditor.getValue() );
		}
		else
		{
			Locale fromRequest = localeResolver.resolveLocale( request );
			try
			{
				newLocaleName = fromRequest.getLanguage().toUpperCase();
				_log.debug( "Locale = " + newLocaleName );
				localeResolver.setLocale( request, response, fromRequest );
			}
			catch ( Exception e )
			{
				String language = "FR";
				newLocaleName = language;
			}

		}

		modelView.addObject( SessionConstants.SESSION_LANGUAGE, newLocaleName );

		modelView.setViewName( "login" );
		_log.debug( "Exiting method indexHandler" );

		return modelView;
	}

}
