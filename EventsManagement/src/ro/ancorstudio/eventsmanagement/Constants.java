package ro.ancorstudio.eventsmanagement;

import java.text.SimpleDateFormat;

/**
 * This class stores constants values.
 * @author Muresan Marius
 *
 */
public class Constants
{

	//sdf for all the app
	public static SimpleDateFormat	sdf						= new SimpleDateFormat( "dd.MM.yyyy HH:mm:ss:SSS" );

	public static final String		JDBC_PROPERTIES			= "jdbc.properties";

	public static final String		RESPONSE_PARAM_SHOWLIST	= "showList";
	public static final String		RESPONSE_CV_LIST		= "cvList";

	// Date input format
	public static SimpleDateFormat	dateInputFormat			= new SimpleDateFormat( "dd/MM/yyyy" );
	public static SimpleDateFormat	dateSqlFormat			= new SimpleDateFormat( "yyyy-MM-dd" );

	public static final String		SQL_DATE_RECEIVED_FROM	= "DATE_RECEIVED_FROM";
	public static final String		SQL_DATE_RECEIVED_TO	= "DATE_RECEIVED_TO";

	public static final String		PROJECT_PROPERTIES		= "projectconfig.properties";

	public static final String		FILE_SERVER_ROOT_DIR	= "FILE_SERVER_ROOT_DIR";

}
