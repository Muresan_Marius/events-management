package ro.ancorstudio.eventsmanagement;

/**
 * This class stores request constants values.
 * @author Muresan Marius
 * @version 1.0
 *
 */

public interface RequestConstants
{
	//Action related constants
	public static final String	ACTION_NAME									= "action";
	public static final String	ACTION_UPDATE_USER_RIGHTS					= "updateUserRights";
	public static final String	ACTION_INSERT_USER							= "insertUser";
	public static final String	ACTION_UPDATE_USER							= "updateUser";
	public static final String	ACTION_INSERT_POSITION						= "insertPosition";
	public static final String	ACTION_UPDATE_POSITION						= "updatePosition";

	//Users related constants
	public static final String	REQUEST_PARAM_LNAME							= "lName";
	public static final String	REQUEST_PARAM_FNAME							= "fName";
	public static final String	REQUEST_PARAM_USERNAME						= "username";
	public static final String	REQUEST_PARAM_PASSWORD						= "password";
	public static final String	REQUEST_PARAM_USER_TYPE						= "userType";

	//PACKAGE related constants
	public static final String	REQUEST_PARAM_PACKAGE_ID					= "positionID";
	public static final String	REQUEST_PARAM_PACKAGE_NAME					= "pName";
	public static final String	REQUEST_PARAM_PACKAGE_DESCRIPTION			= "pDescription";
	public static final String	REQUEST_PARAM_PACKAGE_PRICE					= "pPrice";
	
	public static final String	REQUEST_PARAM_POSITION_NAME					= "positionName";
	public static final String	ID_NAME										= "updateId";
	public static final String	UPDATE_ID									= "userID";
	public static final String	CHECK_TYPE									= "updateCheckBox";

	//EventS related constants
	public static final String	REQUEST_PARAM_ID							= "id";
	public static final String	REQUEST_PARAM_FIRST_NAME					= "first_name";
	public static final String	REQUEST_PARAM_LAST_NAME						= "last_name";
	public static final String	REQUEST_PARAM_BIRTH_DATE					= "birth_date";
	public static final String	REQUEST_PARAM_EMAIL							= "email";
	public static final String	REQUEST_PARAM_PHONE_NUMBER					= "phone_number";
	public static final String	REQUEST_PARAM_SEX							= "sex";
	public static final String	REQUEST_PARAM_PERSON_OBSERVATIONS			= "person_observations";
	public static final String	REQUEST_PARAM_ADDRESS						= "address";
	public static final String	REQUEST_PARAM_LOCALITY						= "locality";
	public static final String	REQUEST_PARAM_CNP							= "cnp";
	public static final String	REQUEST_PARAM_SERIES						= "series";
	public static final String	REQUEST_PARAM_INREGISTRATION_DATE			= "inregistration_date";

	//event related constants
	public static final String	REQUEST_PARAM_EVENT_TYPE					= "eventDescription";
	public static final String	REQUEST_PARAM_EVENT_DATE					= "eventDate";
	public static final String	REQUEST_PARAM_EVENT_APPLICATION_SOURCE		= "applicationSource";
	public static final String	REQUEST_PARAM_EVENT_ID						= "eventId";
	public static final String	REQUEST_PARAM_PERSONS_EVENT_ID				= "id";

	public static final String	REQUEST_PARAM_DATE_RECEIVED					= "date_received";
	public static final String	REQUEST_PARAM_DATE_INSERTED					= "date_inserted";
	public static final String	REQUEST_PARAM_DOCUMENT_NAME_RO				= "document_name_ro";
	public static final String	REQUEST_PARAM_DOCUMENT_NAME_FR				= "document_name_fr";
	public static final String	REQUEST_PARAM_DOCUMENT_NAME_EN				= "document_name_en";
	public static final String	REQUEST_PARAM_LANGUAGE						= "language";
	public static final String	REQUEST_PARAM_LANGUAGE_AUTH					= "language_auth";

	public static final String	REQUEST_PARAM_DATE_FROM						= "date_received_from";
	public static final String	REQUEST_PARAM_DATE_TO						= "date_received_to";
	public static final String	REQUEST_PARAM_LIST_TECHNOLOGIES				= "technology_ids";
	public static final String	REQUEST_PARAM_LIST_ADDITIONAL_SERVICES		= "additionalServices";
	public static final String	REQUEST_PARAM_LIST_USERS					= "employees";
	
	public static final String	REQUEST_PARAM_JSON_OBJECT					= "add_JSON";
	public static final String	REQUEST_PARAM_JSON_OBJECT_SELECTION 		= "selections_json";
	public static final String	REQUEST_PARAM_LANGUAGE_ID					= "id";
	public static final String	REQUEST_PARAM_LANGUAGE_LEVEL				= "level";
	public static final String	REQUEST_PARAM_POSITION_APPLICATION_DATE		= "applicationDate";
	public static final String	REQUEST_PARAM_POSITION_APPLICATION_SOURCE	= "applicationSource";
	public static final String	REQUEST_PARAM_PERSON_ID						= "personId";
	public static final String	REQUEST_PARAM_TAB							= "tab";
}
