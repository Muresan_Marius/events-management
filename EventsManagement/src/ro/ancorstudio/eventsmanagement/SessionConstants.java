package ro.ancorstudio.eventsmanagement;

/**
 * This class stores constants values for session
 * @author Muresan Marius
 * @version 1.0
 *
 */

public interface SessionConstants
{
	//Constants related to Session
	public static final String	SESSION_AUTHENTIFICATED_USER	= "authentificatedUser";
	public static final String	SESSION_LANGUAGE				= "language";
}
