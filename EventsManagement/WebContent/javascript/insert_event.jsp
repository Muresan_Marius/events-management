<script type='text/javascript'>
// Javascript for the insert_event page

var personId = '${id}';

function CreateNavigationBar(id,wrapper){
	$("#"+id).jqxNavigationBar('destroy'); 
	$("#"+wrapper).append('<div id="'+id+'"></div>');	
	
}

function addEvent(item){
	
	// jqxComboBox position (package)	
   	var positionSource = new Array();
   	  <c:forEach items="${positionList}" var="position" varStatus="status">
	        	   var positionItem = new Object();
	        	   positionItem.value = "${position.id}";
	        	   positionItem.label = "${position.name}";
	        	   positionSource.push(positionItem);
   	  </c:forEach>
  	// jqxComboBox additionalService	
      var additionalServiceSource = new Array();
      	  <c:forEach items="${additionalServiceList}" var="additionalService" varStatus="status">
   	        	   	var additionalServiceItem = new Object();
   	        		additionalServiceItem.value = "${additionalService.id}";
   	        		additionalServiceItem.label = "${additionalService.description}";
   	        		additionalServiceSource.push(additionalServiceItem);
      	  </c:forEach>
	// jqxComboBox additionalService	
	   var employeeSource = new Array();
	   	  <c:forEach items="${userList}" var="user" varStatus="status">
		        	   	var userItem = new Object();
		        	   	userItem.value = "${user.userID}";
		        	   	userItem.label = "${user.username}";
		        		employeeSource.push(userItem);
	   	  </c:forEach>
	
 	$("#selections").append(SelectionItem(item));                	
	$("#selections").jqxNavigationBar({ width: 600, expandMode: 'multiple', theme: 'summer' });
	//if ( $('#interviews_'+item.value).children('div').length > 2){
	$("#selections").jqxNavigationBar('render');	
 	$("#packages_"+item.value).jqxComboBox({ selectedIndex: -1, source: positionSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 275, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
 	$("#additionalServices_"+item.value).jqxComboBox({ selectedIndex: -1, source: additionalServiceSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 275, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
 	$("#employees_"+item.value).jqxComboBox({ selectedIndex: -1, source: employeeSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 275, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});

}

function SelectionItem(item){
	return '<div>'+item.label+'</div> \
		<div id="selection_'+item.value+'"> \
			<div id="column_1">\
				<input type="hidden" id="event_selected_id_'+item.value+'" value="'+item.value+'"/> \
		    	<label for="location"><fmt:message key="LABEL_LOCATION"/><br></label>\
				<input type="text" name="location_'+item.value+'" id="location_'+item.value+'" placeholder="<fmt:message key='MESSAGE_LOCATION'/>"><br><br>\
		    	<label for="priceTotal"><fmt:message key="LABEL_PRICE_TOTAL"/><br></label>\
				<input type="text" name="priceTotal_'+item.value+'" id="priceTotal_'+item.value+'" placeholder="<fmt:message key='MESSAGE_PRICE_TOTAL'/>"><br><br>\
				<label for="priceAdvance"><fmt:message key="LABEL_PRICE_ADVANCE"/><br></label>\
				<input type="text" name="priceAdvance_'+item.value+'" id="priceAdvance_'+item.value+'" placeholder="<fmt:message key='MESSAGE_PRICE_ADVANCE'/>"><br><br>\
		        <label for="packages"><strong><fmt:message key="LABEL_PACKAGE"/></strong></label>\
		    	<div id="packages_'+item.value+'" class="rounded"></div><br> \
		        <label for="additionalServices"><strong><fmt:message key="LABEL_ADDITIONAL_SERVICE"/></strong></label>\
		    	<div id="additionalServices_'+item.value+'" class="rounded"></div><br> \
		    	<input type="hidden" name="additionalServices_ids" id="additionalServices_ids" />\
	    	</div>\
	    	<div id="column_2">\
		        <label for="employees"><strong><fmt:message key="LABEL_EMPLOYEE"/></strong></label>\
		    	<div id="employees_'+item.value+'" class="rounded"></div><br> \
		    	<input type="hidden" name="users_ids" id="users_ids" />\
		        <label for="details"><strong><fmt:message key="LABEL_DETAILS"/></strong></label><br>\
				<textarea cols="24" rows="7" name="details_'+item.value+'" id="details_'+item.value+'" height="50%" placeholder="<fmt:message key='MESSAGE_DETAILS'/>"></textarea><br><br>\
	    	</div>\
		</div>\
		</div>';
}


// Function for validating "Ajouter" form
function validateAddForm() {
 	// Form validation
 	errorField = new ErrorField($("#messageBox"),$("#submit"));
 	
 	jQuery.validator.addMethod(
 		    "multiemail",
 		     function(value, element) {
 		         if (this.optional(element)) // return true on optional element 
 		             return true;
 		         var emails = value.split(/[;,]+/); // split element by , and ;
 		         valid = true;
 		         for (var i in emails) {
 		             value = emails[i];
 		             valid = valid &&
 		                     jQuery.validator.methods.email.call(this, $.trim(value), element);
 		         }
 		         return valid;
 		     },
			"<fmt:message key="MESSAGE_VALIDATION_EMAIL"/>"
 		);
 	
 	jQuery.validator.addMethod("nodigits", function(value, element) { 
        var reg = /[0-9]/;
        if(reg.test(value)){
              return false;
        }else{
                return true;
        }
 	}); 
 	
 	jQuery.validator.addMethod("noletters", function(value, element) { 
        var reg = /[a-zA-Z]/;
        if(reg.test(value)){
              return false;
        }else{
                return true;
        }
 	}); 
 	
 	
 	$("#add_form").validate({
 		
 		messages: {
 			'first_name': {required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_FIRST_NAME"/>",
 							nodigits: "<fmt:message key="MESSAGE_VALIDATION_NODIGITS_FIRST_NAME"/>",
 							minlength: "<fmt:message key="MESSAGE_VALIDATION_FIRST_NAME"/>"
 			},
 			'last_name': {required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>",
 							nodigits: "<fmt:message key="MESSAGE_VALIDATION_NODIGITS_LAST_NAME"/>",
 							minlength: "<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>"
 			},
 			'birth_date': "&nbsp;<fmt:message key="MESSAGE_VALIDATION_BIRTH_DATE"/>",
 			'inregistration_date': "&nbsp;<fmt:message key="MESSAGE_VALIDATION_INREGISTRATION_DATE"/>",
			'email': {required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_EMAIL_REQUIRED"/>"},
			'phone_number': {required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_PHONE_REQUIRED"/>",
							noletters: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_PHONE_NOLETTERS"/>"},
			'address': {required :"&nbsp;<fmt:message key="MESSAGE_VALIDATION_ADDRESS_REQUIRED"/>"},
			'cnp': {required :"&nbsp;<fmt:message key="MESSAGE_VALIDATION_CNP_REQUIRED"/>",
					noletters: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_CNP_NOLETTERS"/>",
					minlength: "<fmt:message key="MESSAGE_VALIDATION_CNP"/>",
					maxlength: "<fmt:message key="MESSAGE_VALIDATION_CNP"/>"},
			'series': {required :"&nbsp;<fmt:message key="MESSAGE_VALIDATION_SERIES_REQUIRED"/>"},
			'events_ids': {required: "<fmt:message key="MESSAGE_VALIDATION_ONE_EVENT"/><br><br>"}
 		},
 		rules: {
 		   	'first_name':
 	   		{
 	   		required: true,
 	   		nodigits: true,
 	   		minlength: 3
 	   		},
 	   		'last_name':
 	   		{
 	   		required: true,
 	   		nodigits: true,
 	   		minlength: 3  		
 	   		},
 	   		'birth_date':
   			{
   			required: true
   			},
 	   		'inregistration_date':
   			{
   			required: true
   			},
 	   		'address':
   			{
   			required: true
   			},
   			'phone_number':
			{
 			required: true,
			noletters: true
			},
   			'email':
			{
 			required: true,
			multiemail: true
			},
   			'cnp':
			{
 			required: true,
			noletters: true,
			minlength: 13,
			maxlength: 13
			},
 	   		'series':
   			{
   			required: true
   			},
			'events_ids' :
			{
			required: true
			}	
 		},
	     onkeyup: false,
	     debug:true,
	     ignore: ".ignore",
	     errorClass: "field_input_error",
	     validClass: "field_input_success",  
	     invalidHandler:function(form, validator){
		     	$('html, body').animate({scrollTop:0}, 0);
		   		var errors = validator.numberOfInvalids();
		   		if (errors > 1) {
		    		var message = '<fmt:message key="MESSAGE_VALIDATION_ERRORS"/>';
		    		errorField.showError(message);
		   		}else{
		    		var errorList = validator.errorList;
		    		errorField.showError(errorList[0].message);
		   		}
		  },
		  // submitHandler
		   submitHandler:function(form){

				// add_JSON with data 
				var addJSON = "{ ";
								
				// Events list
				if ($.trim( $('#event_list').html() ).length) {
    				// JSON data for events
    				addJSON += '"events": [';

    				// Get information from each event div
    				$("#event_list > .event_item").each(function() {
    					var id 	  	   = $(this).find("input:eq(0)").val();
    					if (id == "")
    						id = "-1";
    					var eventId 	  	   = $(this).find("input:eq(1)").val();
    					var eventDate = $(this).find("input:eq(2)").val();
						var applicationSource = $(this).find("[id^='application_source_']").jqxComboBox('getSelectedItem').value;
    					
    					// alert("Position ID: " + id + ", Date: " + applicationDate + ",Source: " + applicationSource);
    					
    					addJSON += '{ \
									"id": '+id+', \
									"eventId": '+eventId+', \
									"eventDate": "'+eventDate+'", \
									"applicationSource": '+applicationSource+' \
								 },';

    				});
    				
    				// Remove extra comma
    				addJSON = addJSON.substring(0, addJSON.length-1);
    				addJSON += ']';
				}
				
				addJSON += '}';
				$("#add_JSON").val(addJSON);
				console.log(addJSON);

				form.submit();
		   }
		  
 	});
 	
 	$.validator.messages.digits = '<fmt:message key="MESSAGE_VALIDATION_DIGITS"/>';
 	$.validator.messages.required = '<fmt:message key="MESSAGE_VALIDATION_REQUIERED"/>';
 }

// Get jqxNavigationbar elemenent by value
function getByExpander(value){
  var result = -1;
  $(".jqx-expander-header-content").each(function(index,item){
   if ($(item).html() == value) 
    result = index;
  });
  return result;
}  

$(document).ready(function () {
	
	// Tabs functionality
	$("ul.tabs > li").click(function(){
		// Add class 'current' to the clicked tab
    	$(".current").removeClass('current');
		$(this).find("a").addClass('current');

		// Set 'display: block' to selected tab
		$("div.panes_item ").css("display", "none");
		$("div.panes").find('div.panes_item:eq('+$(this).index()+')').css("display", "block");
	});
	
	
	// Check if person already exists in the database 
	// If it exists, and the fields are error free get the person id and send it to the update page	
	var checked = 0;
	if (personId != -1){
		checked = 1;
	}
	
	//** Datepickers **
	// Birth date datepicker
	$("#birth_date").datepicker({ dateFormat: "dd/mm/yy", firstDay: 1, changeMonth: true, maxDate: "0", changeYear: true, showButtonPanel: true, closeText: "<fmt:message key='LABEL_CLOSE'/>", currentText: "<fmt:message key='LABEL_TODAY'/>", 
		  showOn: "button",
	      buttonImage: "images/calendar.gif",
	      buttonImageOnly: true});
	
	$("#inregistration_date").datepicker({ dateFormat: "dd/mm/yy", firstDay: 1, changeMonth: true, changeYear: true, showButtonPanel: true, closeText: "<fmt:message key='LABEL_CLOSE'/>", currentText: "<fmt:message key='LABEL_TODAY'/>", 
		  showOn: "button",
	      buttonImage: "images/calendar.gif",
	      buttonImageOnly: true});
	
	// Form validation
	validateAddForm();
	
	//** jqxComboBoxes **
	      	
   // jqxComboBox eventType	
   var eventSource = new Array();
   	  <c:forEach items="${eventList}" var="event" varStatus="status">
	        	   var eventItem = new Object();
	        	   eventItem.value = "${event.id}";
	        	   eventItem.label = "<fmt:message key='EVENT_DESCRIPTION_${event.id}'/>";
	        	   eventSource.push(eventItem);
   	  </c:forEach>
   	  $("#events").jqxComboBox({ selectedIndex: -1, source: eventSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 275, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
    
   // jqxComboBox POSITION (PACKAGE)
      var positionSource = new Array();
   	  <c:forEach items="${positionList}" var="position" varStatus="status">
	        	   var positionItem = new Object();
	        	   positionItem.value = "${position.id}";
	        	   positionItem.label = "${position.description}";
	        	   positionSource.push(positionItem);
   	  </c:forEach>
   	  $("#positions").jqxComboBox({ selectedIndex: -1, source: positionSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 275, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
   	  
    // jqxComboBox language levels	
     var langLevelSource = new Array();
    	  <c:forEach items="${languageLevelList}" var="languageLevel" varStatus="status">
        	   var langLevelItem = new Object();
        	   langLevelItem.value = "${languageLevel.id}";
        	   langLevelItem.label = "<fmt:message key='LANGUAGE_${languageLevel.id}'/>";
        	   langLevelSource.push(langLevelItem);
    	  </c:forEach> 
    
   	// jqxComboBox application source
   	var applicationSourceSource = new Array();
	<c:forEach items="${applicationSourceList}" var="applicationSource" varStatus="status">
			var applicationSourceItem = new Object();
			applicationSourceItem.value = "${applicationSource.id}";
			applicationSourceItem.label = "<fmt:message key='APPLICATION_SOURCE_${applicationSource.id}'/>";
			applicationSourceSource.push(applicationSourceItem);
	 </c:forEach>
      	  
   
    // jqxNavigationBar for events
    $("#events").on('checkChange', function (event) {
    	if (event.args) {
    		var item = event.args.item;
            if (item) {
            	
                if ( $("#event_" + item.value).length != 0 ){
                	// Remove position from list
                	$("#event_" + item.value  + " + br").remove();
                	$("#event_" + item.value).remove();
                	// Clear selection if there are no more positions selected
                	if ($("#events").jqxComboBox('getCheckedItems').length == 0) 
                	{
                		$("#events").jqxComboBox('clearSelection');
                	}
                }
                else {

       				$('#event_list').append('<div id="event_'+item.value+'" class="event_item"> \
       				<input type="hidden" id="person_event_id_'+item.value+'" name="person_event_id_'+item.value+'"\> \
       	       		<input type="hidden" id="event_id_'+item.value+'" name="event_id_'+item.value+'" value="'+item.value+'"\> \
       				<em>'+item.label+'</em><br><br> \
       				<div style="float: left; width: 250px" >\
       				<span class="required">*</span> \
					<label for="event_date_'+item.value+'"><fmt:message key="LABEL_EVENT_DATE"/>&nbsp;(dd/mm/yyyy)</label><br> \
					<input type="text" name="event_date_'+item.value+'" id="event_date_'+item.value+'" class="event_date required"> \
					</div><div style="float: left; width: 250px" >\
					<span class="required">*</span> \
					<label for="application_source_'+item.value+'"><fmt:message key="LABEL_APPLICATION_SOURCE"/></label><br> \
					<div id="application_source_'+item.value+'" name="application_source_'+item.value+'" class="rounded"></div> \
					</div><br class="clear"><br class="clear">');
                	
       		   	    $("#application_source_"+item.value).jqxComboBox({ selectedIndex: -1, source: applicationSourceSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 100, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
                	
    				// Datepicker for application date
					$("#event_date_"+item.value).datepicker({ dateFormat: "dd/mm/yy", firstDay: 1, changeMonth: true, changeYear: true, showButtonPanel: true, closeText: "<fmt:message key='LABEL_CLOSE'/>", currentText: "<fmt:message key='LABEL_TODAY'/>" , 
						  showOn: "button",
					      buttonImage: "images/calendar.gif",
					      buttonImageOnly: true});
            	}         	
        	}
        }
    	
     	// Replace list of positions with "Select more..." text
    	if (($("#events").find("input").val() != "<fmt:message key='LABEL_SELECT_MORE'/>") && ($("#events").jqxComboBox('getCheckedItems').length != 0))
   		{
    		setTimeout(function() {
        		$("#events").find("input").val("<fmt:message key='LABEL_SELECT_MORE'/>");	
    		},201);
   		}
    	
       }); 
    
   	// Add hidden input fields for data from jqxComboboxes
 	$('#events').on('checkChange', function (event){
 		var items = $("#events").jqxComboBox('getCheckedItems'); 
 		var pos ='';

 		for (var i = 0; i < items.length; i++) {
 			if (pos == '')
 		    	pos = items[i].value; 
 			else 
 				pos = pos + ', ' + items[i].value; 
 		}
 		$("#event_ids").val(pos);
 	}); 
	 
       
   	// Add hidden input fields for data from jqxComboboxes
 	$('#positions').on('checkChange', function (event){
 		var items = $("#positions").jqxComboBox('getCheckedItems'); 
 		var pos ='';

 		for (var i = 0; i < items.length; i++) {
 			if (pos == '')
 		    	pos = items[i].value; 
 			else 
 				pos = pos + ', ' + items[i].value; 
 		}
 		$("#position_ids").val(pos);
 	}); 
 		
 	$("#selection_submit").on('click',function(e){
 	    var events = [];
 		$("input:hidden[id^='event_selected_id_']").each(function(index,item){
 			var event = {};
 			event.id = $(item).val();
			event.eventLocation = $("#location_"+event.id).val();
			event.price = $("#priceTotal_"+event.id).val();
			event.advance = $("#priceAdvance_"+event.id).val();
			event.positionId = $("#packages_"+event.id).jqxComboBox('getSelectedItem').value;
			event.details = $("#details_"+event.id).val(); 
			event.additionalServicesIds = [];
	 		// Populate hidden input field
	 		var items = $("#additionalServices_"+event.id).jqxComboBox('getCheckedItems'); 
	 		var pos ='';

	 		for (var i = 0; i < items.length; i++) {
	 			if (pos == '')
	 		    	pos = items[i].value; 
	 			else 
	 				pos = pos + ',' + items[i].value; 
	 		}
	 		$("#additionalServices_ids").val(pos);
	 		event.additionalServicesIds = $("#additionalServices_ids").val()
	 		
	 		event.usersIds = [];
	 		// Populate hidden input field
	 		var items = $("#employees_"+event.id).jqxComboBox('getCheckedItems'); 
	 		var pos ='';

	 		for (var i = 0; i < items.length; i++) {
	 			if (pos == '')
	 		    	pos = items[i].value; 
	 			else 
	 				pos = pos + ',' + items[i].value; 
	 		}
	 		$("#users_ids").val(pos);
	 		event.usersIds = $("#users_ids").val();
	 		
			switch(event.additionalServices ) {			
				case "Sedinta foto":
					event.additionalServicesId = 1;
					break;
				case "Album digital":
					event.additionalServicesId = 2;
					break;					
				case "Album foto":
					event.additionalServicesId = 3;
					break;			
			}
			
 	 		events.push(event);
 		});
		console.log(JSON.stringify(events));
 		$("#selections_json").val(JSON.stringify(events));
 	});
}); 

</script>