/**
 * 
 */

function SearchEVENTS(HIDE,SHOW,SELECTED,NONESELECTED,CHECKALL,UNCHECKALL,INTERVALERROR){
	var hidden = false;
	var error = false;
	var firstName = "";
	var lastName = "";
	this.ShowHideColumns = function(){
		var notSelectedColumns = $.map( $('#columns :not(:selected)'),
                function(e) { return $(e).val(); });
		$.each(notSelectedColumns ,function(index, value){
			$("#resultTable").jqGrid('hideCol', columns[value]);
		});
		
		var SelectedColumns = $.map( $('#columns :selected'),
                function(e) { return $(e).val(); });
		$.each(SelectedColumns ,function(index, value){
			$("#resultTable").jqGrid('showCol', columns[value]);
		});
	};
	
	this.ShowHideSearchMenu = function(){
		$("#slide").slideToggle('slow');
		if(hidden){
			$("#hide_search_options > a").html(HIDE);
			hidden = false;
		}else{
			$("#hide_search_options > a").html(SHOW);
			hidden = true;
		}
	};
	
	this.LoadComboBoxes = function(){
		$("#columns").children().each(function(index,item){
			if($.inArray($(item).val(),defaultColumns) >= 0){
				$(item).attr('selected','selected');
			}else{
				$(item).removeAttr('selected');
			}
			}
		);
	};
	
	this.createComboBox = function(){		
	};
	
	/** SQL GENERATORS FUNCTIONS **/
	this.getFieldsSql = function(){
		sql = "";
		if ($("#lastname").val() != ""){
			if (before)
				sql += " AND ";
			else
				before = true;
			sql += "PERSONS.LAST_NAME = '"+ $("#lastname").val()+"'";
		}
		if ($("#firstname").val() != ""){
			if (before)
				sql += " AND ";
			else
				before = true;
			sql += "PERSONS.FIRST_NAME = '"+ $("#firstname").val()+"'";
		}
		if ($("#email").val() != ""){
			if (before)
				sql += " AND ";
			else
				before = true;
			sql += "PERSONS.EMAIL = '"+ $("#email").val()+"'";
		}
		if ($("#phone").val() != ""){
			if (before)
				sql += " AND ";
			else
				before = true;
			sql += "PERSONS.PHONE = '"+ $("#phone").val()+"'";
		}
		if ($("#location").val() != ""){
			if (before)
				sql += " AND ";
			else
				before = true;
			sql += "PERSONS_EVENTS.LOCATION = '"+ $("#location").val()+"'";
		}

		return sql;
	};
	
	function getCheckedValues(select){
		var values = [];
		var array = select.jqxComboBox('getCheckedItems');
		
		if ( array == null)
			return null;
		
		$.each(array,function(index,item){
			if (item.value == -1){
				return;
			}
			values.push(item.value);
		});
		return values;
	}
	
	function getSelectedValue(select){
		var item = select.jqxComboBox('getSelectedItem');
		if (item != null)
			return item.value;
		return null;
	}
	
	this.getMultiColumnSelectSql = function(){
		error = false;
		sql = "";
		var events = {
				data: getCheckedValues($('#events')),
				column: 'PERSONS_EVENTS.EVENT_ID',
				relation: 'or'
			};
		var positions = {
				data: getCheckedValues($('#positions')),
				column: 'PERSONS_EVENTS.PACKAGE_ID',
				relation: 'or'
			};
		var applicationSource = {
				data: getSelectedValue($('#applicationSource')),
				column: 'PERSONS_EVENTS.APPLIED_FROM_ID'
			};

		var users = {
				data: getCheckedValues($('#users')),
				column: 'USERS_EVENTS.USER_ID',
				subquery: ' PERSONS_EVENTS.ID in ( SELECT USERS_EVENTS.PERSONS_EVENTS_ID from USERS_EVENTS join persons_events on ( persons_events.id = users_events.persons_events_id) where ',
				relation: 'and'
			};
		
		var dateEvent = { 
				interval: {
					between:$("#applieddate_between").val(),
					and:$("#applieddate_and").val()
				},
		   		column: 'PERSONS_EVENTS.DATE_EVENT'
			};
		
		
		sql += this.ArrayToSql(events);
		sql += this.ArrayToSql(positions);
		sql += this.ArrayToSql(applicationSource);
		sql += this.ArrayToSql(users);
		
		sql += parseSubArray(dateEvent);


		if (error == true){
			errorField.showError(INTERVALERROR);
			this.error = false;
			return "";
		}else
			return sql;
	};
	
	this.checkIfBefore = function(sql){
		if (sql != "")
			if (before == true) {
				return " AND "+sql;
			}else{
				before = true;
				return sql;
			}	
		else
			return "";
	};
	
	this.ArrayToSql = function(array){
		var sql = "";
		sql += parseArray(array);
		return this.checkIfBefore(sql);
	};
		
	function parseArray(array){ 
		var sql = "";
		if (array.data != null){
			if (array.data instanceof Array){
				if (array.data.length > 1 && array.subquery == undefined)
					sql += "(";
					
				$.each(array.data,function(index, value){
					if (index > 0)
						sql += " "+array.relation;
					if (array.subquery != undefined)
						sql += array.subquery;
					else
						sql += "(";
					sql += array.column+' = '+value;
					if (array.childrens != undefined){
						$.each(array.childrens , function(index,item){
							if (item.procedure == undefined)
								sql += parseSubArray(item,"and");
							else
								sql += parseSubArray(item,"and",value);
						});
					}
					sql += ")";
				});
				if (array.data.length > 1 && array.subquery == undefined)
					sql +=")";
			}else{
				sql += '(' + array.column + ' = ' + array.data + ')';
			}
		}
		return sql;
	};
	
	function parseSubArray(array,relation,dinamic_parameter){
		sql = "";
		if (array.data != null){
			if (!(array.data instanceof Array))
				return ' and (' + array.column + ' = ' + array.data + ')';
			sql += " "+relation+" ";
			if (array.data.length > 1)
				sql += "(";
			$.each(array.data,function(index,value){
				if (index > 0)
					sql += " "+array.relation+" ";
				if (array.procedure == undefined)
					sql += array.column+' = '+value;
				else{
					var parameters = array.parameters.join(',');
					sql += array.procedure + "("+parameters+","+dinamic_parameter+") >= "+value;
				}
			});
			if (array.data.length > 1)
				sql += ")";
		}
		if (array.interval != null){
			if (array.interval.between == "" && array.interval.and == "")
				return sql;
			if (before == true){
				sql +=" and ";
			}else{
				sql +="";
				before = true;
			}
			sql += array.column + IntervalToSql(array.interval.between, array.interval.and);
		}
		return sql;
	};
	
	function IntervalToSql(between, and){
		if (between != "" && and != "")
			return " BETWEEN "+toMinSqlDate(between)+" AND "+toMaxSqlDate(and);
		if (between != "")
			return " >= "+toMinSqlDate(between);
		if (and != "")
			return " <= "+toMaxSqlDate(and);
	};
	
	function ToMonths(date){
		var data = date.split("-");
		var year = data[0];
		var months = data[1];
		var result = 0;
		if (year != undefined){
			if (isNaN(year)){
		        error = true;
		        return 0;
		    }
		    result += 12*parseInt(year);
		}
		if (months != undefined){
			if (isNaN(year)){
		        error = true;
		        return 0;
		    }
		    result += parseInt(months);
        }
		return isNaN(result) ? 0 : result;
	}
	
	function valid(data){
		array = data.split("-");
		if (array.length == 1){
			if (isNaN(array[0])){
				error = true;
				return 0;
			}
			if (array == ""){
			    return 0;
			}
			return 1;
		}
		if (array[0]=="" || array[1]==""){
			error = true;
			return 0;
		}
		if (isNaN(array[0]) || isNaN(array[1])){
			error = true;
			return 0;
		}
		if (array[0] > 60 || array[1] > 11){
			error = true;
			return 0;
		}
		return 1;
	}
}

function toMinSqlDate(date){
	var newdate = date.split("/");
	return "'"+newdate[2]+"-"+newdate[1]+"-"+newdate[0]+" 00:00:00'";
}

function toMaxSqlDate(date){
	var newdate = date.split("/");
	return "'"+newdate[2]+"-"+newdate[1]+"-"+newdate[0]+" 23:59:59'";
}
