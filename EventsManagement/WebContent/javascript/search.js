function checkFields()
{	
	return true;
}

function checkDate(field)
{
	expr = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
	
	// Minimum year for compraison
	var minY = 1970;
	// Maximum year for compraison 
	var maxY = (new Date()).getFullYear()+50;
	// Error message
	var errMsg = "";
	
	if(field.valueOf() != '') {
		if(regs = field.valueOf().match(expr)){
			if(regs[1] < 1 || regs[1] > 31) {
				errMsg = "Valoare incorecta pentru zi : " + regs[1];
			} else if (regs[2] < 1 || regs[2] > 12) {
				errMsg = "Valoare incorecta pentru luna : " + regs[2];
			} else if (regs[3] < minY || regs[3] > maxY) {
				errMsg = "Valoare incorecta pentru an : " + regs[3] + " - ar trebui sa fie intre " + minY + " si " + maxY;
			}
		} else {
			errMsg = "Formatul de data este invalid: " + field.valueOf();
		}
	} 
	
	if(errMsg != "") {
		alert(errMsg);
		return false;
	}
	
	return true;
}