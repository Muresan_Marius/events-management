function checkMandatory()
{
	var lname = document.getElementById("last_name").value;
	var fname = document.getElementById("first_name").value;
	var datp = document.getElementById("date_received").value;
	var docRo = document.getElementById("document_name_ro").value;
	var docFr = document.getElementById("document_name_fr").value;
	var docEn = document.getElementById("document_name_en").value;
	var email = document.getElementById("email").value;
	
	// First name, last name, date received, date inserted and email are mandatory
	if (lname==null || fname == null || datp==null  || email==null || lname.trim()=='' || fname.trim() == '' || datp.trim()=='' ||  email.trim()=='')
	{
		alert("Va rugam sa introduceti toate campurile obligatorii");
		return false;
	}
	
	if (docRo=='' && docFr=='' && docEn=='')
	{
		alert("Pentru fiecare persoana introdusa, cel putin un CV este obligatoriu!");
		return false;
	}
	
	// A character '@' and "." are mandatory in the composition of an email.
	if (email.length <= 5)
	{
		alert("Lungimea campului email este prea mica!");
		return false;
	}
	else
	{
		if (email.indexOf('@') == -1 || email.indexOf('.') == -1)
		{
			alert("Emailul nu este formatat corespunzator! (de tipul xxx@yyy.zzz)");
			return false;
		}
	}
	return true;
	
}

function setVisibility(option)
{
	if (option.value=="Da")
	{
		document.getElementById("list").style.visibility="visible";
	}
	else
	{
		document.getElementById("list").style.visibility="hidden";
	}
}