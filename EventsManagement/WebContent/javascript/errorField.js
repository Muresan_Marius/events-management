var errorField;
function ErrorField(element,submit){
	this.element = element;
	this.submit = submit;
	this.element.html("begin");
	this.showError = function(message){
		this.element.html(message);
		this.element.slideToggle();
		this.element.animate({
			opacity: "1",
			backgroundColor: "#FF0000"
		},1000);
		// setTimeout(this.hideError ,4000, this);
		var _this = this;
		$(document).on('click',function(){
			_this.hideError(_this);
		});
		try
		{
			$(parent.document).on('click',function(){
				_this.hideError(_this);
			});
		}
		catch(e)
		{
			console.log(e);
		}
		submit.attr("disabled", "disabled");
	};
	
	this.hideError = function(_this){
		_this.element.animate({
			opacity: "0",
			backgroundColor: "#FFFFFF"	
		},1000);
		setTimeout(function (_this) {
			_this.element.slideUp();
			submit.removeAttr("disabled");
		} , 1000, _this);
	};
}