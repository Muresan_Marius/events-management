function checkLength()
{
	var user = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	
	if (user.length <= 3 && password.length <= 3 )
	{
		alert("Campurile Utilizator si Parola trebuie sa contina minim 4 caractere. Va rugam reincercati!");
		return false;
	}
	if (user.length <= 3)
	{
		alert("Campul utilizator trebuie sa contina minim 4 caractere. Va rugam reincercati!");
		return false;
	}
	if (password.length <= 3)
	{
		alert("Campul parola trebuie sa contina minim 4 caractere. Va rugam reincercati!");
		return false;
	}
	return true;
	
}

function changeLang()
{
	//get selected language
	var aux = document.getElementById('langues').options[document.getElementById('langues').selectedIndex].value;
	//set hidden to selected language
	document.getElementById('language').value = aux;
	document.getElementById('authform').submit();
}

