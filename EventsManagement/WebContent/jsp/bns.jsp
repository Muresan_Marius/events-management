<%@ include file="/jsp/include.jsp" %>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/bns.css">
<title><fmt:message key="MESSAGE_BROWSER_NOT_SUPPORTED_TITLE"/></title>
</head>
	<body>
		<br>
		<br>
		<table class="bns_hidden" align="center">
			<tbody>
				<tr>
					<td class="bns_message" color="260E05">
						<b><fmt:message key="MESSAGE_BROWSER_NOT_SUPPORTED_TITLE"/></b>
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<br>
		<table class="bns_tb" align="center">
			<tr >
				<td class="bns_title">
					<fmt:message key="MESSAGE_BROWSER_NOT_SUPPORTED_TITLE"/>
				</td>
				
			</tr>
			<tr >
				<td class="roundedCorners">
					<table width="300px" height="100px" align="center">
						<tr >
							<td align="left">
								<div class="btn_textarea required" style="width:300px">
									<fmt:message key="MESSAGE_BROWSER_NOT_SUPPORTED"/>
									<div>									
										<div>
											<a href='http://firefox.com'>
												<img src='images/firefox.png' style='border:none'>
											</a>
												<div>Firefox 16+</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>