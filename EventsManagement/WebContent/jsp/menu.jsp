<%@ include file="/jsp/include.jsp" %>

<!-- begin menu.htm -->
<header id = "main-header">

<table>
	<tr>
		<td id="header-left"><img src="./images/Events management.jpg" height="90" width="300" alt="Events Management"></td>
		<td id="header-middle">
			<nav id="main-nav">
				<ul>
					
					<c:choose>
						<c:when test="${menuName == 'search_events' }">
							<li class="menuSelected">
						</c:when>
						<c:otherwise>
							<li>
						</c:otherwise>
					</c:choose>
						<a href="./search_events.htm">
						<div><fmt:message key="SEARCH_EVENTS"/></div>
						<img src="./images/search.png" height="83" width="90">
						</a>
					</li>
					
					<c:if test="${currentUser.userType == 1 }">
						<c:choose>
							<c:when test="${menuName == 'insert_event' }">
								<li class="menuSelected">
							</c:when>
							<c:otherwise>
								<li>
							</c:otherwise>
						</c:choose>
							<a href="./insert_event.htm">
							<div><fmt:message key="INSERT_EVENT"/></div>
							<img src="./images/insert.png" height="83" width="90">
							</a>
						</li>
					</c:if>
					
 					<c:if test="${currentUser.userType == 1 }">
						<c:choose>
						<c:when test="${menuName == 'administration' }">
							<li class="menuSelected">
						</c:when>
						<c:otherwise>
							<li>
						</c:otherwise>
					</c:choose>
						<a href="./administration.htm">
						<div><fmt:message key="ADMINISTRATION"/></div>
						<img src="./images/tools.png" height="83" width="90">
						</a>
					</li>
					</c:if> 
					
				</ul>
			</nav>
		</td>
		<td id="header-right">
			<div id="welcome">
				<p>
					<fmt:message key="WELCOME"/>
					<em><c:out value="${currentUser.firstName}"></c:out> <c:out value="${currentUser.lastName}"></c:out></em> (<a href="./logout.htm"><fmt:message key="DISCONNECT"/></a>)
				</p>
			</div>
			<img src="./images/AncorStudio.png" height="102" width="150" alt="Ancor Studio">
		</td>
	</tr>
</table>

</header>
<p class="clear">
<!-- end menu.htm --> 