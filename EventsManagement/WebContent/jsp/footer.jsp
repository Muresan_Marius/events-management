<%@ include file="/jsp/include.jsp" %>
<!-- begin footer.htm -->
<footer id="footerCopyright">
<p><span id="copyright"> Copyright @<c:out value="${year}"/> </span> <span id="footerText">Ancor Studio | All rights reserved.</span></p>
</footer>
<!-- end footer.htm -->