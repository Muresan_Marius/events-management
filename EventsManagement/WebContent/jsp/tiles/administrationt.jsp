<%@ include file="/jsp/include.jsp" %>
<!-- begin administrationt.htm -->
<script >
$(document).ready(function(){
	errorField = new ErrorField($("#message_div"),$("#submit"));
});
</script>
<div id="message_div" class="errorField"></div>

<!-- the tabs --> 
<ul class="tabs">
	<li><a class="current"><fmt:message key="TAB_USERS"/></a></li>
	<li><a class=""><fmt:message key="TAB_EXPORT_BDD"/></a></li> 
	<li><a class=""><fmt:message key="TAB_POSITION"/></a></li>
</ul>


<!-- tab "panes" -->
<div class="panes">
	<div style="display: block; min-height: 500px" id="users" class="panes_item">
   		<iframe src='userst.htm' frameborder="0" width="100%" height="100%"></iframe>
	</div>
	
	<div style="display: none; min-height: 500px" id="export" class="panes_item">
   		<iframe src='exportt.htm' frameborder="0" width="100%" height="100%"></iframe>
	</div>
	
	<div style="display: none; min-height: 500px" id="position" class="panes_item">
   		<iframe src='positiont.htm' frameborder="0" width="100%" height="100%"></iframe>
	</div>
</div>