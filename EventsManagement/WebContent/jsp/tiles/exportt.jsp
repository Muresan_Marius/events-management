<%@ include file="/jsp/include.jsp" %>

<!-- STYLESHEET FILES -->
		<link rel="stylesheet" type="text/css" href="./css/style.css"/>
		<link rel="stylesheet" type="text/css" href="./css/export.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="css/plugins/jqGrid/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/plugins/jqGrid/jquery.ui.theme.css">		
		<!-- jQWidgets --> 
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.summer.css" type="text/css" />
<!-- END STYLESHEET FILES -->

<!-- JQUERY FILES -->
		<script type="text/javascript" src="./javascript/jquery/jquery.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.metadata.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/jquery.validate.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/additional-methods.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery-ui-1.9.2.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.multiselect.js"></script>
		<script  type="text/javascript" src="javascript/plugins/jqGrid/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="javascript/plugins/jqGrid/i18n/grid.locale-<fmt:message key="MESSAGE_LANG"/>.js"></script>
<!-- END JQUERY FILES -->
<!-- OUR JS FILES -->
		<script src="javascript/errorField.js"></script>
<!-- END OUR JS FILES -->
<!-- begin exportt_cvs.htm -->

<script type="text/javascript" src="/EventsManagement/dwr/engine.js"></script>
<script type="text/javascript" src="/EventsManagement/dwr/util.js"></script>
<script type="text/javascript" src="/EventsManagement/dwr/interface/dwrService.js"></script>

<script type="text/javascript" src="javascript/plugins/jqwidgets/scripts/gettheme.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxnavigationbar.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxwindow.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxtabs.js"></script>   
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcombobox.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxexpander.js"></script>
<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxprogressbar.js"></script>
        
<script>
	$(document).ready(function(){
        $("#jqxProgressBar").jqxProgressBar({ width: 250, height: 30, value: 0, showText:true,animationDuration:100});
        	//dwrService.cleanExportDirectory(CleanDestinationDirectoryExport);
        dwrService.exportDatabase(ExportDatabase);
	});
		
	function ExportDatabase(data){
		if (data){
			setTimeout(function (event) { 				
				$("#status").html('<fmt:message key="EXPORT_SUCCESSFULLY_EXPORTED_DATABASE"/>');
				$("#jqxProgressBar").jqxProgressBar({value: 33});
				dwrService.archiveDirectory(Archive);
				//dwrService.moveCvFiles(MoveCvFiles);
				
			},1000);
		}
	}
	
	
	function Archive(data){
		if (data){
			setTimeout(function (event) { 				
				dwrService.cleanExportDirectory(CleanAfterDirectory);
			},1000);
		}
	}
	
	function CleanAfterDirectory(data){
	    if (data){
			$("#status").html('<fmt:message key="EXPORT_SUCCESSFULLY_DELETED_TEMPORARY_FILES"/>');
			$("#jqxProgressBar").jqxProgressBar({value: 80});
			setTimeout(function (event) { 				
				$("#status").html('<fmt:message key="EXPORT_ARCHIVE_CREATED_SUCCESSFULLY"/>');
				$("#jqxProgressBar").jqxProgressBar({value: 100});
				$("#downloadDB").css('visibility', 'visible');
				$( "#downloadDB" ).click(function() {
					window.location.href="D:/";
				});
			},1000);
		}  
	}
	
	

</script>
<br><br><br>
<table class="bns_tb" align="center" >
			<tr >
				<td>
					<center>
						<p id="legenda">
							<fmt:message key="EXPORT_LABEL" />
			  			</p>
			  		</center>
				</td>
			</tr>
			<tr >
				<td class="roundedCorners">
					<section>
				        <div id='status'><fmt:message key="EXPORT_SUCCESSFULLY_CLEANED_DESTINATION_DIRECTORY"/></div>
						<div  id='jqxProgressBar' style='overflow: hidden;'></div>
						<c:if test="${type == 'aux_server'}">
							<IFRAME id='upload_frame' src='upload.htm' name="hidden-form" height='100' width='250 ' frameBorder='0'></IFRAME> 
						</c:if>
					</section>
				</td>
			</tr>
			<tr>
				<td>
					<center><input id="downloadDB" class="submit_bt orange" type="button" value="Download" style="visibility: hidden"></center><br>
				</td>
			</tr>
</table>

<!-- end exportt_cvs.htm -->