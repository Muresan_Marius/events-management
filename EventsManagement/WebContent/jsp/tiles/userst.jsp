<%@ include file="/jsp/include.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- STYLESHEET FILES -->
		<link rel="stylesheet" type="text/css" href="./css/style.css"/>
		<link rel="stylesheet" type="text/css" href="./css/users.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="css/plugins/jqGrid/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/plugins/jqGrid/jquery.ui.theme.css">
		
		<!-- jQWidgets --> 
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.summer.css" type="text/css" />
<!-- END STYLESHEET FILES -->

<!-- JQUERY FILES -->
		<script type="text/javascript" src="./javascript/jquery/jquery.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.metadata.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/jquery.validate.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/additional-methods.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery-ui-1.9.2.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.multiselect.js"></script>
		<script  type="text/javascript" src="javascript/plugins/jqGrid/jquery.jqGrid.min.js"></script>
		
		<script type="text/javascript" src="javascript/plugins/jqwidgets/scripts/gettheme.js"></script>
		<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcore.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxbuttons.js"></script>
        <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxlistbox.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcombobox.js"></script>
	    
		<script type="text/javascript" src="javascript/plugins/jqGrid/i18n/grid.locale-<fmt:message key="MESSAGE_LANG"/>.js"></script>
<!-- END JQUERY FILES -->
<!-- OUR JS FILES -->
		<script src="javascript/errorField.js"></script>
	 	<!-- END OUR JS FILES -->
<script type="text/Javascript">

var message = "";
  
function update(userId,cbox){
	
 	//vaType represents the value of the CheckBox: 2 = read only, 1 = full rights
 	var valType = 0;
	if (cbox.checked) valType = 2;
		else valType = 1; 

	$("#resultTable").jqGrid().setGridParam({
			url : 'userData.htm', 
			postData : { 
				updateCheckBox: valType,
				updateId: userId,
				action:'updateUserRights'
		} 
	}).trigger("reloadGrid");
}

$(document).on('click',function(){
	parent.errorField.hideError(parent.errorField);
});	

$(document).ready(function(){
	jqGridCreationComplete();
		
	jQuery.validator.addMethod("nodigits", function(value, element) { 
        var reg = /[0-9]/;
        if(reg.test(value)){
              return false;
        }else{
                return true;
        }
	 });
	
	$("#insertUpdateUserForm").validate({
		ignore: '.ignore',
		messages: 
		{
	  		'lName':{	required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>",
	  					nodigits: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_NODIGITS_LAST_NAME"/>",
	  					minlength: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>"
	  				},
	  		'fName':{	required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_FIRST_NAME"/>",
						nodigits: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_NODIGITS_FIRST_NAME"/>",
						minlength: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_FIRST_NAME"/>"
					},
		   	'username': "&nbsp;<fmt:message key="MESSAGE_VALIDATION_USER"/>",
		    'password': "&nbsp;<fmt:message key="MESSAGE_VALIDATION_PASSWORD"/>"
		},
		rules: 
	   	{
		'username': 
		{
			required: true,
	     	minlength: 6
	    },
	    'password':
	    	{
	    	required: true,
	    	minlength: 4
	    	},
	   	'lName':
	   		{
	   		required: true,
	   		minlength: 3,
	   		nodigits: true
	   		},
	   	'fName':
	   		{
	   		required: true,
	   		minlength: 3,
	   		nodigits: true
	   		}
	   	 },
	     	   	 
	     onkeyup: false,
	     debug:true,
	     
	     errorClass: "field_input_error",
	     validClass: "field_input_success",  
	     
	     submitHandler:function(form){
	    	$("#resultTable").jqGrid().setGridParam({
				url : 'userData.htm', 
				postData : { 
					userID:$("#userID").val(),
					lName:$("#lName").val(),
					fName:$("#fName").val(),
					username:$("#username").val(),
					password:$("#password").val(),
					userType:$("input[type=radio]:checked").val(),
					action:$("#submit").attr("name")
				} 
			}).trigger("reloadGrid");
	    	
	    	$("#resultTable").jqGrid().setGridParam({
				url : 'userData.htm', 
				postData : { 
					action:''
				} 
			}).trigger("reloadGrid");
	    		    	
	    	$("#insertUpdateUserForm")[0].reset();
	        $("#password").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	       	$("#username").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	       	$("#lName").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	       	$("#fName").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input"); 
	       	$('#submit').val('<fmt:message key="BUTTON_SUBMIT"/>');
	       	$('#password').removeClass('ignore');
	    	
	     },
	
	     invalidHandler:function(form, validator){
	     	$('html, body').animate({scrollTop:0}, 0);
	   		var errors = validator.numberOfInvalids();
	   		if (errors > 1) {
	    		var message = '<fmt:message key="MESSAGE_VALIDATION_ERRORS"/>';
	    		parent.errorField.showError(message);
	   		}else{
	    		var errorList = validator.errorList;
	    		parent.errorField.showError(errorList[0].message);
	   		}
	     },
	    
	     errorPlacement: function(){
	    	 
	      }
	 });
       
	
});
	
</script>

<center>
<br>
<div id="message_div" class="errorField"></div>
<form method="POST" id="insertUpdateUserForm" name="insertUpdateUserForm" action="users.htm">
		<input type="hidden" id="updateActionName_insert" name="updateActionName" value="insertUser"> 
<fieldset id="insertTable" class="insertTbl" align="center">
	<p id="legenda"><fmt:message key="LABEL_ADD"/></p><br>
	<table width="90%" height="100%" border="0" align="center" >
		
		<tr>
			<input type="hidden" id="userID" name="userID" value="">
			<td width="15%" align="right">
				<label for="lName"><fmt:message key="LABEL_LAST_NAME"/>: </label>
			</td>
			<td width="20%">
				<input class="field_input" type="text" id="lName" name="lName" placeholder="<fmt:message key='MESSAGE_LAST_NAME'/>"/>
			</td>
			<td width="20%" align="right">
				<label for="username"><fmt:message key="LABEL_USERNAME"/>: </label>
			</td>
			<td width="25%">
				<input class="field_input" type="text" id="username" name="username" autocomplete="off" placeholder="<fmt:message key='MESSAGE_USER'/>" />
			</td>
			<td width="20%" align="left">
				<input type="radio" id='radio1' name="radio1" value="readOnly"/><fmt:message key="LABEL_READ_ONLY"/>
			</td>
		</tr>
			
		<tr>
			<td width="15%" align="right">
				<label for="fName"><fmt:message key="LABEL_FIRST_NAME"/>: </label>
			</td>
			<td width="20%">
				<input class="field_input" type="text" id="fName" name="fName" placeholder="<fmt:message key='MESSAGE_FIRST_NAME'/>"/>
			</td>
			<td width="20%" align="right">
				<label for="password"><fmt:message key="LABEL_PASSWORD"/>: </label>
			</td>
			<td width="25%">
				<input class="field_input" type="password" id="password" name="password" autocomplete="off" placeholder="<fmt:message key='MESSAGE_PASSWORD'/>"/>
			</td>
			<td width="20%" align="left">
				<input type="radio" id='radio1' name="radio1" value="fullRights" checked="checked"/><fmt:message key="LABEL_FULL_RIGHTS"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="5">
			<center>
                <div>
                	<br>
                    <input type="reset" class="submit_bt orange" id="cancel" name="cancel" value="<fmt:message key="BUTTON_CANCEL"/>" />
                    <input type="submit" class="submit_bt orange" id="submit" name="insertUser" value="<fmt:message key="BUTTON_SUBMIT" />" />
               
                </div>
            </center>
            </td>
		</tr>
	</table>
	<div>
	
	</div>
</fieldset>
</form>

<br><br>

<div align='center' id="jqGrid">
			<table id="resultTable"></table>
			<div id="pager"></div>
		</div>
		
 <script>
$('#cancel').on('click', function(){
	$('#submit').val('<fmt:message key="BUTTON_SUBMIT"/>');
  	$('#submit').attr('name','insertUser');
    $('#password').removeClass('ignore');
   	$("#insertUpdateUserForm > input[type='text']").val("insertUser");
   	$("#password").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
   	$("#username").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
   	$("#lName").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
   	$("#fName").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
});


function jqGridCreationComplete(){
		$(function(){ 
			jQuery("#resultTable").jqGrid({
				url:'userData.htm',
				datatype: "json",
				height: 'auto',
				width: '70%',
				gridview: true,
				rowNum:10, 
				rowList:[5,10,20,30],
				mtype: "GET", 
	            rownumWidth: 20,
	            shrinkToFit: true,
	            pager: '#pager',
	            viewrecords: true,
	            sortable: true,
        		 
	            
	           	colNames:[
	           	          '<fmt:message key="LABEL_NUMBER"/>',
	           	          '<fmt:message key="LABEL_LAST_NAME"/>',
	           	          '<fmt:message key="LABEL_FIRST_NAME"/>',
	           	          '<fmt:message key="LABEL_USERNAME"/>',
	           	          '<fmt:message key="LABEL_READ_ONLY"/>',
	           	          '<fmt:message key="LABEL_LAST_UPDATE"/>',
	           	          ], 
	           	colModel:[ 
	           	       	   {name:'userID',index:'USER_ID', width:100, hidden:true}, 
	         		  	   {name:'lastName',index:'LAST_NAME', width:140, sortable:true}, 
	           	           {name:'firstName',index:'FIRST_NAME', width:140}, 
	           	           {name:'username',index:'USERNAME', width:150}, 
	           	           {name:'userType',index:'USER_TYPE', width:100, formatter:CheckBox, align:'center'}, 
	           	           {name:'lastUpdate',index:'LAST_UPDATE', width:150, align:'center'}, 
	           	        ],
	           	        
				jsonReader : {
				    root: "rows",
				    page: "page",
				    total: "total",
				    records: "records",
				    repeatitems: false,
				    cell: "cell",
				    id: "id"
				},
				
	           
	            serializeRowData: function (data) {
	            	return JSON.stringify(data);
	            },
	            onSelectRow: function(id) { 
	        	   var myGrid = $('#resultTable'),
	        	   selRowId = myGrid.jqGrid ('getGridParam', 'selrow'),
	        	   userID = myGrid.jqGrid ('getCell', selRowId, 'userID');
	        	   lastName = myGrid.jqGrid ('getCell', selRowId, 'lastName');
	        	   firstName = myGrid.jqGrid ('getCell', selRowId, 'firstName');
	        	   username = myGrid.jqGrid ('getCell', selRowId, 'username');
	        	   typeUser = myGrid.jqGrid ('getCell', selRowId, 'userType');
	        	   if($(typeUser).is(':checked')){
	        		   $('[value="readOnly"]').prop('checked',true);
	        	   }
	        	   else {
	        		   $('[value="fullRights"]').prop('checked',true);
	        	   }

	        	   jQuery("#userID").val(userID);
	        	   jQuery("#lName").val(lastName);
	        	   jQuery("#fName").val(firstName);
	        	   jQuery("#username").val(username);
	        	   
	        	   $('#submit').val('<fmt:message key="BUTTON_UPDATE"/>');
	        	   $('#submit').attr('name','updateUser');
	        	   $('#password').addClass('ignore');
	        	   $("#insertUpdateUserForm > input").val("updateUser");
	        	   
				},
	            
	            loadComplete: function () {                  
	            	ModifyResultTableStockDefaultStyles();                  
	            	}
			});//jqGrid
		}); //$(function()	
		$("#jqGrid").setGridParam({datatype: 'local'}); 		
		$("#resultTable ").setGridParam({datatype: 'json'}).trigger("reloadGrid");
	}//jqGridCreationComplete
	
	function CheckBox(cellvalue,options,rowObject){
		if (cellvalue == "2")
			return '<input type="checkbox" name="righs" id="rights" checked = "checked" onchange="update(\''+rowObject.userID+'\',this)" />';
	    else
			return '<input type="checkbox" name="righs" id="rights" onchange="update(\''+rowObject.userID+'\',this)" />';
	}
	
	function ModifyResultTableStockDefaultStyles() {  
		 $('#' + "resultTable" + ' tr').removeClass("ui-widget-content");
		 $('#' + "resultTable" + ' tr:nth-child(even)').addClass("evenTableRow");
		 $('#' + "resultTable" + ' tr:nth-child(odd)').addClass("oddTableRow");
	}
		
</script> 
<form method="POST" id="updateCheckForm" name="updateCheckForm" action="">
	<input type="hidden" id="updateActionName" name="updateActionName" value="updateUserRights"> 
	<input type="hidden" id="updateId" name="updateId" value="">
	<input type="hidden" id="updateCheckBox" name="updateCheckBox" value="">
	
</form>	
</center>