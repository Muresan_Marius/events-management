<%@ include file="/jsp/include.jsp" %>

<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<html>
    <head>
        <!-- STYLESHEET FILES -->
        
		<link rel="stylesheet" type="text/css" href="./css/style.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="css/plugins/jqGrid/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/plugins/jqGrid/jquery.ui.theme.css">
		
	<link rel="stylesheet" type="text/css" href="css/ui-lightness/jquery-ui-1.9.2.custom.css">
	
        <!-- Tabs -->
		<link rel="stylesheet" type="text/css" href="css/plugins/tabs/tabs.css">
		<link rel="stylesheet" type="text/css" href="css/plugins/tabs/tabs-panes.css">
		
		<!-- jQWidgets --> 
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
        <link rel="stylesheet" href="javascript/plugins/jqwidgets/jqwidgets/styles/jqx.summer.css" type="text/css" />
        
		<!-- END STYLESHEET FILES -->
		
		<!-- PAGE ATTRIBUTES -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="shortcut icon" href="./images/Ancor.png">
		<!-- END PAGE ATTRIBUTES -->
	    
		<!-- JQUERY FILES -->
		<script type="text/javascript" src="./javascript/jquery/jquery.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.metadata.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/jquery.validate.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/additional-methods.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery-ui-1.9.2.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.multiselect.js"></script>
		<script  type="text/javascript" src="javascript/plugins/jqGrid/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="javascript/plugins/jqGrid/i18n/grid.locale-<fmt:message key="MESSAGE_LANG"/>.js"></script>
		<!-- END JQUERY FILES -->
		
		<!-- OUR JS FILES -->
		<script src="javascript/errorField.js"></script>
	 	<!-- END OUR JS FILES -->
		<!-- COMMON JAVASCRIPTS -->
		<script type="text/javascript" src="./javascript/common/common.js"></script>
		<!-- END COMMON JAVASCRIPTS -->
        
        <link rel="stylesheet" type="text/css" href="./css/<tiles:getAsString name="action" />.css"/>
        <script type="text/javascript" src="./javascript/<tiles:getAsString name="action" />.js" ></script>
        <link rel="stylesheet" type="text/css" href="./css/jquery.multiselect.css"/>
          

        
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/scripts/gettheme.js"></script>
		<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcore.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxnavigationbar.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxwindow.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxtabs.js"></script>   
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxbuttons.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxlistbox.js"></script>
	    <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcombobox.js"></script>
        <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
    	<script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxexpander.js"></script>
        <script type="text/javascript" src="javascript/plugins/jqwidgets/jqwidgets/jqxprogressbar.js"></script>
        
        <title><tiles:insertAttribute name="title" ignore="true" /></title>
    </head>
    <body>
    	<div id="wrap">
	        <tiles:insertAttribute name="menu" />
	        <div id="main">
	        	<tiles:insertAttribute name="body" />
	        </div>
	   	</div>
   		<tiles:insertAttribute name="footer" />
   </body>
</html>