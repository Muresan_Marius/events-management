<%@ include file="/jsp/include.jsp" %>
<!-- begin search_events.htm -->
<!--DWR-->

<script>
	var before = false;
	var sql = "";
	var defaultColumns = [];
	var searchEVENTS;
	var errorField = null;
	
	function deselectFunctionality(event){
		if (event.args == undefined) 
			return;
   		if (event.args.item != undefined) {
	   		var item = event.args.item;
           	if (item.index == 0) {
          	 	$(this).jqxComboBox('clearSelection');
	        }
      	}
  	};
  	
  	function sourceArray(){
  		var source = [{value: -1, label:"<b><fmt:message key='MULTISELECT_UNCHECKALLTEXT'/></b>"}];
  		return source;
  	}
  	
	
	$(document).ready(function(){
	    errorField = new ErrorField($("#messageBox"),$("#submit"));
		var hidden = false;
		searchEVENTS = new SearchEVENTS(
				'<fmt:message key="BUTTON_HIDE"/>',
				'<fmt:message key="BUTTON_SHOW"/>',
				'<fmt:message key="MULTISELECT_SELECTEDTEXT"/>',
				'<fmt:message key="MULTISELECT_NONESELECTEDTEXT"/>',
				'<fmt:message key="MULTISELECT_CHECKALLTEXT"/>',
				'<fmt:message key="MULTISELECT_UNCHECKALLTEXT"/>',
				'<fmt:message key="MESSAGE_INVALID_INTERVAL"/>' 
		);
		
		/* Load Html Objects */
		jqGridCreationComplete();
		$('#left_container').find('select').next().css("width","230px");
		$('#right_container').find('select').next().css("width","229px");
		$("#left_container").find('input').css('width','211px');
		$("#right_container").hide();
		/* Handlers for page buttons */
		var odd = true;
		$("#advanced_search").on('click',function(){
			if (odd == true){
				$("#right_container").show("slide", { direction: "left" }, 1000);
				$("#advanced_search").text("<fmt:message key="LABEL_LESS_CRITERIAS"/>");
				odd = false;
			}
			else{
				$("#right_container").hide("slide", { direction: "left" }, 1000);
				$("#advanced_search").text("<fmt:message key="LABEL_MORE_CRITERIAS"/>");
				odd = true;
			}
		});
		$("#hide_search_options > a").on('click',searchEVENTS.ShowHideSearchMenu);
		jQuery("#jqGrid").hide();
		
		$("#abort").on('click',function(){
			searchEVENTS.LoadComboBoxes();
			searchEVENTS.ShowHideColumns();
	 		$('#slide input[type="text"]').val("");
	 		$(".rounded").jqxComboBox('clearSelection'); 
	 		$(".rounded").jqxComboBox('uncheckAll'); 
			$("#resultTable").jqGrid().setGridParam({url : 'search_eventData.htm', postData : {SqlFilters : "" } }).trigger("reloadGrid");
			sql ="";
			jQuery("#jqGrid").hide();
		});
		
		$("#submit").on('click',function(){
			sql = "";
			before = false;
			sql += searchEVENTS.getFieldsSql();
			sql += searchEVENTS.getMultiColumnSelectSql();
			$("#resultTable").jqGrid().setGridParam({ url : 'search_eventData.htm', postData : { SqlFilters : sql }, page:1 }).trigger("reloadGrid");
			searchEVENTS.ShowHideColumns();
			console.log(sql);
			$("#jqGrid").show();
		});
				
		//** jqxComboBoxes **  
		
	    // jqxComboBox events	
	    var eventSource = new Array();
		// Add 'Select all' item
		var eventItem = new Object();
		eventItem.value = "-1";
		eventItem.label = "<strong><fmt:message key='MULTISELECT_CHECKALLTEXT'/></strong>";
		eventSource.push(eventItem);
		<c:forEach items="${eventList}" var="event" varStatus="status">
		 	   var eventItem = new Object();
		 	   eventItem.value = "${event.id}";
		 	   eventItem.label = "<fmt:message key='EVENT_DESCRIPTION_${event.id}'/>";
		 	   eventSource.push(eventItem);
	  	</c:forEach>
		$("#events").jqxComboBox({ selectedIndex: -1, source: eventSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 225, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});	      
	    var enableUncheckEvents = true;
	    
  		$("#events").on('checkChange', function (event) {
	  	// 'Select all'
	  	if (event.args.item != undefined) {
			var item = event.args.item;
	      	if (item.index == 0) {
	      		if (item.checked == true){
	         	 	$("#events").jqxComboBox('checkAll');
	         	 	enableUncheckEvents = true;
	     		}
	      		else if (item.checked == false && enableUncheckEvents){
	      			$("#events").jqxComboBox('uncheckAll');	
	      		}
	 		}
	      	else {
	      		var selectAllItem = $("#events").jqxComboBox('getItem', 0 );	      	
	      		if (selectAllItem.checked == true ) {
	      			enableUncheckEvents = false;
	      			$("#events").jqxComboBox('uncheckIndex', 0); 
	      		}
	      	}	      
	 	  }
		   // Replace list with "Select more..." text
		  	if (($("#events").find("input").val() != "<fmt:message key='LABEL_SELECT_MORE'/>") && ($("#events").jqxComboBox('getCheckedItems').length > 1))
		 		{
		  		setTimeout(function() {
		      		$("#events").find("input").val("<fmt:message key='LABEL_SELECT_MORE'/>");	
		  		},201);
		 		}
		  	else if ($("#events").jqxComboBox('getCheckedItems').length == 0) 
		  	{
		  		// Clear selection if no item is selected
		  		$("#events").jqxComboBox('clearSelection');
		  	}
	 	}); 
      
	   // jqxComboBox users	
    	var userSource = new Array();
		// Add 'Select all' item
 		var userItem = new Object();
 		userItem.value = "-1";
 		userItem.label = "<strong><fmt:message key="MULTISELECT_CHECKALLTEXT"/></strong>";
 		userSource.push(userItem);
		// Add remaining items from the database
    	<c:forEach items="${userList}" var="user" varStatus="status">
 	 		var userItem = new Object();
 	 		userItem.value = "${user.userID}";
 	 		userItem.label = "${user.username}";
 	 		userSource.push(userItem);
        </c:forEach>
   		$("#users").jqxComboBox({ selectedIndex: -1, source: userSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 200, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
   
   		var enableUncheckUsers = true;
   	 	$("#users").on('checkChange', function (event) {
   			// 'Select all'
   			if (event.args.item != undefined) {
				var item = event.args.item;
	      		if (item.index == 0) {
		       		if (item.checked == true)
		      		{
		          	 	$("#users").jqxComboBox('checkAll');
		          	 	enableUncheckUsers = true;
		      		}
		       		else if (item.checked == false && enableUncheckUsers){
		       			$("#users").jqxComboBox('uncheckAll');	
		       		}
		  		}
		       	else {
		       		var selectAllItem = $("#users").jqxComboBox('getItem', 0 );       	
			       	if (selectAllItem.checked == true ) {
			       		enableUncheckUsers = false;
			       		$("#users").jqxComboBox('uncheckIndex', 0); 
			       	}
		      	 }     
 			}
		    // Replace list with "Select more..." text
		   	if (($("#users").find("input").val() != "<fmt:message key='LABEL_SELECT_MORE'/>") && ($("#users").jqxComboBox('getCheckedItems').length > 1))
		  		{
		   		setTimeout(function() {
		       		$("#users").find("input").val("<fmt:message key='LABEL_SELECT_MORE'/>");	
		   		},201);
		  		}
		   	else if ($("#users").jqxComboBox('getCheckedItems').length == 0) 
		   	{
		   		// Clear selection if no item is selected
		   		$("#users").jqxComboBox('clearSelection');
		   	}
 	 	});
    
	     // jqxComboBox packages	
	     var positionSource = new Array();
		// Add 'Select all' item
		var positionItem = new Object();
		positionItem.value = "-1";
		positionItem.label = "<strong><fmt:message key="MULTISELECT_CHECKALLTEXT"/></strong>";
		positionSource.push(positionItem);
	     	  <c:forEach items="${positionList}" var="position" varStatus="status">
	  	        	   var positionItem = new Object();
	  	        	   positionItem.value = "${position.id}";
	  	        	   positionItem.label = "${position.name}";
	  	        	   positionSource.push(positionItem);
	     	  </c:forEach>
	     $("#positions").jqxComboBox({ selectedIndex: -1, source: positionSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 125, checkboxes:true, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
     
   	    var enableUncheckPositions = true;
        $("#positions").on('checkChange', function (event) {
       	// 'Select all'
       	if (event.args.item != undefined) {
	   			var item = event.args.item;
	            if (item.index == 0) {
	           	if (item.checked == true)
	          		{
	              	 	$("#positions").jqxComboBox('checkAll');
	              	 	enableUncheckPositions = true;
	          		}
	           	else if (item.checked == false && enableUncheckPositions){
	           			$("#positions").jqxComboBox('uncheckAll');	
	           		}
	      		}
	           	else {
	           		var selectAllItem = $("#positions").jqxComboBox('getItem', 0 );           	
	           		if (selectAllItem.checked == true ) {
	           			enableUncheckPositions = false;
	           			$("#positions").jqxComboBox('uncheckIndex', 0); 
	           		}
	           	}       
      		}
        // Replace list with "Select more..." text
       	if (($("#positions").find("input").val() != "<fmt:message key='LABEL_SELECT_MORE'/>") && ($("#positions").jqxComboBox('getCheckedItems').length > 1))
      		{
       		setTimeout(function() {
           		$("#positions").find("input").val("<fmt:message key='LABEL_SELECT_MORE'/>");	
       		},201);
      		}
       	else if ($("#positions").jqxComboBox('getCheckedItems').length == 0) 
       		{
	       		// Clear selection if no item is selected
	       		$("#positions").jqxComboBox('clearSelection');
       		}
      	});
       
	     // jqxComboBox applicationSource	
	     var applicationSourceSource = sourceArray();
	 	  <c:forEach items="${applicationSourceList}" var="applicationSource" varStatus="status">
	     	   var applicationSourceItem = new Object();
	     	   applicationSourceItem.value = "${applicationSource.id}";
	     	   applicationSourceItem.label = "<fmt:message key='APPLICATION_SOURCE_${applicationSource.id}'/>";
	     	  applicationSourceSource.push(applicationSourceItem);
	 	  </c:forEach>
	  	 $("#applicationSource").jqxComboBox({ selectedIndex: -1, source: applicationSourceSource, displayMember: 'label', valueMember: 'value', width: 225, height: 25, dropDownHeight: 125, theme: 'summer', placeHolder: "<fmt:message key='LABEL_SELECT'/>"});
	  	 $("#applicationSource").on('select', deselectFunctionality);
	
		//** Datepickers **
		$("#applieddate_between").datepicker({ dateFormat: "dd/mm/yy", firstDay: 1, changeMonth: true, changeYear: true, showButtonPanel: true, closeText: "<fmt:message key='LABEL_CLOSE'/>", currentText: "<fmt:message key='LABEL_TODAY'/>", 
			  showOn: "button",
		      buttonImage: "images/calendar.gif",
		      buttonImageOnly: true});
	    
		$("#applieddate_and").datepicker({ dateFormat: "dd/mm/yy", firstDay: 1, changeMonth: true, changeYear: true, showButtonPanel: true, closeText: "<fmt:message key='LABEL_CLOSE'/>", currentText: "<fmt:message key='LABEL_TODAY'/>", 
			  showOn: "button",
		      buttonImage: "images/calendar.gif",
		      buttonImageOnly: true});
    
	});
	
</script>

<script type='text/javascript'>
	function jqGridCreationComplete(){
		$(function(){ 
			jQuery("#resultTable").jqGrid({
				url: 'search_eventData.htm',
				datatype: "json",
				height: 'auto',
				width: '70%',
				rowNum:10, 
				rowList:[5,10,20,30],
				mtype: "GET", 
	            rownumWidth: 20,
	            shrinkToFit: true,
	            pager: '#pager',
	            viewrecords: true,
	            sortname: "LAST_NAME",
	            sortorder: "asc",
	           	colNames:[
	           	          'Id',
	           	          '<fmt:message key="LABEL_LAST_NAME"/>',
	           	          '<fmt:message key="LABEL_FIRST_NAME"/>',
	           	          '<fmt:message key="LABEL_PHONE_NUMBER"/>',
	           	          '<fmt:message key="LABEL_EVENT"/>',
	           	          '<fmt:message key="LABEL_PACKAGE"/>',
	           	          '<fmt:message key="LABEL_DATE"/>',
	           	          '<fmt:message key="LABEL_LOCATION"/>',           	
	           	          ], 
	           	colModel:[ 
	           	           {name:'id',index:'PERSON_ID', width:100,sortable:true, hidden: true}, 
	           	           {name:'lastName',index:'LAST_NAME', width:100,sortable:true}, 
	           	           {name:'firstName',index:'FIRST_NAME', width:125,sortable:true}, 
	           	           {name:'phone',index:'PHONE', align:"center", width: 100}, 
	           	           {name:'eventDescription',index:'EVENT_DESCRIPTION', width:120, align:"left", sortable:true}, 
	           	           {name:'positionName',index:'NAME', width:200, align:"left", sortable:true}, 
	           	           {name:'eventDateStr',index:'DATE_EVENT', width:100, sortable:true}, 
	           	           {name:'location',index:'LOCATION', width:170, align:"left", sortable:true}, 	           	        
	           	        ],
				
				jsonReader : {
				    root: "rows",
				    page: "page",
				    total: "total",
				    records: "records",
				    repeatitems: false,
				    cell: "cell",
				    id:"index"
				},
				
				serializeRowData: function (data) {
	            	return JSON.stringify(data);
	            },
	            
	            beforeSelectRow: function(id,item) {
	               var row = $("#resultTable").jqGrid('getRowData',id);
	               var $item = $(item.target);
	               if ($item.attr('class') == undefined && !$item.find('input').length){
	            	   $("#update_personID").val(row.id);
			           $("#update").submit();
	               }
	        	},
	        	
	        	gridComplete: function()
	            {},
	            
	            loadComplete: function () {                  
	            	ModifyResultTableStockDefaultStyles();                  
	            	}
	        });//jqGrid
		}); //$(function()	
	}//jqGridCreationComplete
	
	// Add CSS classes for even/odd rows
	function ModifyResultTableStockDefaultStyles() {  
		 $('#' + "resultTable" + ' tr').removeClass("ui-widget-content");
		 $('#' + "resultTable" + ' tr:nth-child(even)').addClass("evenTableRow");
		 $('#' + "resultTable" + ' tr:nth-child(odd)').addClass("oddTableRow");
	}
	
	function currencyFmatter(cellvalue,options,rowObject){
	    return "<input class='submit_bt orange' type='button' value='<fmt:message key="LABEL_OPEN"/>' src='"+cellvalue+"'/>";
	}
</script>

	<div id='languages_new'></div>
	<div id='jqxNavigationBar'></div>
	<table class="bns_hidden" align="center">
		<tbody>
			<tr>
				<td class="bns_message" color="260E05">
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<br>
	<div id='messageBox' class='errorField'></div>
	<div id='slide'>
	<table class="bns_tb" align="center">
		<tr >
			<td class="roundedCorners">
				<a id='advanced_search' style='float:left'><fmt:message key="LABEL_MORE_CRITERIAS"/></a><br>
				<table width="300px" height="100px" align="center">
					<tr >
						<td>
							<table id='left_container'>
								<tr>
									<td>
										<table style='width: 430px'>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_EVENT_DATE"></fmt:message> <fmt:message key="LABEL_BETWEEN"></fmt:message></td>
												<td><input class='search_events_input hasDate' type='text' name="applieddate_between" id="applieddate_between" maxlength="5" placeholder="<fmt:message key="MESSAGE_DATE"/>" />
												<span class="search_and"><fmt:message key="LABEL_AND"></fmt:message></span><input class='search_events_input hasDate' type='text' name="applieddate_and" id="applieddate_and" maxlength="5" placeholder="<fmt:message key="MESSAGE_DATE"/>"/>
												</td>

											</tr>	
											<tr>
												<td class='userPass'><fmt:message key="LABEL_LOCATION"></fmt:message>: </td>
												<td><input class='search_events_input' type='text' name="location" id="location" maxlength="25" style="width: 227px" placeholder="<fmt:message key="MESSAGE_LOCATION"/>"/></td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_TYPE_EVENT"></fmt:message>: </td>
												<td>
												 <div id="events" class="rounded"></div>
												</td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_PACKAGES"></fmt:message>: </td>
												<td>
												 <div id="positions" class="rounded"></div>
												</td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_APPLICATION_SOURCE"></fmt:message>: </td>
												<td colspan="3">
												 <div id="applicationSource" class="rounded"></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table id='right_container'>
								<tr>
									<td >
										<table style='width: 430px'>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_LAST_NAME"></fmt:message>: </td>
												<td><input class='search_events_input' type='text' name="lastname" id="lastname" maxlength="25" style="width: 227px" placeholder="<fmt:message key="MESSAGE_LAST_NAME"/>"/></td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_FIRST_NAME"></fmt:message>: </td>
												<td><input class='search_events_input' type='text' name="firstname" id="firstname" maxlength="25" style="width: 227px" placeholder="<fmt:message key="MESSAGE_FIRST_NAME"/>"/></td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_PHONE_NUMBER"></fmt:message>: </td>
												<td><input class='search_events_input' type='text' name="phone" id="phone" maxlength="25" placeholder="<fmt:message key="LABEL_PHONE_NUMBER"/>"/></td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_EMAIL_ADDRESS"></fmt:message>: </td>
												<td><input class='search_events_input' type='text' name="email" id="email" maxlength="25" style="width: 227px" placeholder="<fmt:message key="LABEL_EMAIL_ADDRESS"/>"/></td>
											</tr>
											<tr>
												<td class='userPass'><fmt:message key="LABEL_EMPLOYEE"></fmt:message>: </td>
												<td>
												 <div id="users" class="rounded"></div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<div id='buttons_container'>
								<input class="submit_bt orange" type="submit" id="abort" value="<fmt:message key="BUTTON_CANCEL"/>"/>
								<input class="submit_bt orange" type="submit" id="submit" value="<fmt:message key="BUTTON_SUBMIT"/>"/>
							</div>
						</td>							
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</div>
	<div id='excel'>
		<input class="submit_bt orange" type='submit' id='export' value='<fmt:message key="BUTTON_EXPORT"/>'/>
	</div>
	<div id='hide_search_options'><a><fmt:message key="BUTTON_HIDE"/></a></div>
	<div align='center' id="jqGrid">
		<table id="resultTable"></table>
		<div id="pager"></div>
	</div>
	
	<form id='update' method='GET' action='insert_event.htm' target='_blank'>
		<input type='hidden' id='update_personID' name='personID'>
	</form>

<!-- end search_events.htm -->