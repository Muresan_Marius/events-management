<%@ include file="/jsp/include.jsp" %>
<br><br>
<!-- STYLESHEET FILES -->
		<link rel="stylesheet" type="text/css" href="./css/style.css"/>
		<link rel="stylesheet" type="text/css" href="./css/position.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="css/plugins/jqGrid/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/plugins/jqGrid/jquery.ui.theme.css">
<!-- END STYLESHEET FILES -->

<!-- JQUERY FILES -->
		<script type="text/javascript" src="./javascript/jquery/jquery.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.metadata.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/jquery.validate.js"></script>
		<script type="text/javascript" src="./javascript/jquery-validate/additional-methods.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery-ui-1.9.2.js"></script>
		<script type="text/javascript" src="./javascript/jquery/jquery.multiselect.js"></script>
		<script  type="text/javascript" src="javascript/plugins/jqGrid/jquery.jqGrid.min.js"></script>
		<script type="text/javascript" src="javascript/plugins/jqGrid/i18n/grid.locale-<fmt:message key="MESSAGE_LANG"/>.js"></script>
<!-- END JQUERY FILES -->
<!-- OUR JS FILES -->
		<script src="javascript/errorField.js"></script>
<!-- END OUR JS FILES -->
<!-- begin positiont.htm -->
<script type="text/javascript">

$(document).ready(function(){
	jqGridPositionCreationComplete();
	
	
	jQuery.validator.addMethod("nodigits", function(value, element) { 
        var reg = /[0-9]/;
        if(reg.test(value)){
              return false;
        }else{
                return true;
        }
	 });
	
	$("#insertPositionForm").validate({
		messages: 
		{
	  	'pname':{	required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>",
	  				nodigits: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_NODIGITS_LAST_NAME"/>",
	  				minlength: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_LAST_NAME"/>"
	  			},
	  	'pdescription':{
	  					required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_DESCRIPTION"/>",
	  					noletters: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_PRICE_NOLETTERS"/>"
	  					},
	  	'pprice':{
						required: "&nbsp;<fmt:message key="MESSAGE_VALIDATION_PRICE"/>"
						}
		},
		rules: 
	   	{
		'pname': 
			{
				required: true,
	     		minlength: 2
	   		 },
	   	'pdescription':
	   		{
			required: true
	   		 },
 	   	'pprice':
	   		{
			required: true,
			noletters: true
	   		 }
	   	 },
	     	   	 
	     onkeyup: false,
	     debug:true,
	     
	     errorClass: "field_input_error",
	     validClass: "field_input_success",  
	     
	     submitHandler:function(form){
	    	$("#resultTablePosition").jqGrid().setGridParam({
				url : 'positionData.htm', 
				postData : { 
					positionID:$("#positionID").val(),
					pName:$("#pname").val(),
					pDescription:$("#pdescription").val(),
					pPrice:$("#pprice").val(),
					action:$("#submit").attr("name")
				} 

	    	}).trigger("reloadGrid");
	    	
	    	$("#resultTablePosition").jqGrid().setGridParam({
				url : 'positionData.htm', 
				postData : { 
					action:''
				} 
			}).trigger("reloadGrid");
	    		    	
	    	$("#insertPositionForm")[0].reset();
	        $("#pname").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	        $("#pdescription").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	        $("#pprice").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
	       	$('#submit').val('<fmt:message key="BUTTON_SUBMIT"/>');
	    	
	     },
	
	     invalidHandler:function(form, validator){
	     	$('html, body').animate({scrollTop:0}, 0);
	   		var errors = validator.numberOfInvalids();
	   		if (errors > 1) {
	    		var message = '<fmt:message key="MESSAGE_VALIDATION_ERRORS"/>';
	    		parent.errorField.showError(message);
	   		}else{
	    		var errorList = validator.errorList;
	    		parent.errorField.showError(errorList[0].message);
	   		}
	     },
	    
	     errorPlacement: function(){
	    	 
	      } 
	 });
}); 

</script>
<center>
<div id="message_div" class="errorField"></div>
<form method="POST" id="insertPositionForm" name="insertPositionForm" action="position.htm">
	<input type="hidden" id="updateActionName_insert" name="updateActionName" value="insertPosition"> 
	<fieldset class="insertPackage" align="center">	
		<p id="legenda"><fmt:message key="LABEL_ADD_POSITION"/></p><br>
		<table width="100%" height="100%" border="0" align="center" >
			<input type="hidden" id="positionID" name="positionID" value="">
			<tr >
				<td width="15%"  align="right">
					<label for="pname"><fmt:message key="LABEL_POSITION"/>: </label>
				</td>
				<td width="25%" align="left">
					<input class="field_input" id="pname" name="pname" placeholder="<fmt:message key='MESSAGE_POSITION'/>"/><br><br>
				</td>
				<td width="25%"  heigh="20%" align="right">
					<label for="pdescription"><fmt:message key="LABEL_POSITION_DESCRIPTION"/>: </label>
				</td>
				<td rowspan="5" width="35%" align="left">
					<textarea class="field_input_description" id="pdescription" name="pdescription" placeholder="<fmt:message key='MESSAGE_POSITION_DESCRIPTION'/>"/></textarea><br><br>
				</td>

			</tr>
			<tr>
				<td width="25%" align="right">
					<label for="pprice"><fmt:message key="LABEL_POSITION_PRICE"/>: </label>
				</td>
				<td width="25%" align="left">
					<input class="field_input" id="pprice" name="pprice" placeholder="<fmt:message key='MESSAGE_POSITION_PRICE'/>"/><br><br>
				</td>
			</tr>
			<tr>
			</tr>
			<tr>
			</tr>
			<tr>
			</tr>
			<tr >
				<td colspan="4">
				<center>
		    	<div>  
		    		<br>    
			    	<input class="submit_bt orange" type="reset" id="cancel" name="cancel" value="<fmt:message key="BUTTON_CANCEL"/>" />
			        <input class="submit_bt orange" type="submit" id="submit" name="insertPosition" value="<fmt:message key="BUTTON_SUBMIT" />" />
		   		</div>
		   		</center>
		   		</td>
		   	</tr>
	   	</table>
	</fieldset>
</form>
<br>
<div align='center' id="jqGridPosition">
			<table id="resultTablePosition"></table>
			<div id="pagerPosition"></div>
		</div>

<script>

$('#cancel').on('click', function(){
	$('#submit').val('<fmt:message key="BUTTON_SUBMIT"/>');
  	$('#submit').attr('name','insertPosition');
   	$("#insertPositionForm > input[type='text']").val("insertPosition");
   	$("#pname").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
   	$("#pdescription").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
   	$("#pprice").removeClass("field_input_error").removeClass("field_input_success").addClass("field_input");
});

$(document).on('click',function(){
	parent.errorField.hideError(parent.errorField);
});	

function jqGridPositionCreationComplete(){
		$(function(){ 
			jQuery("#resultTablePosition").jqGrid({
				url:'positionData.htm',
				datatype: "json",
				height: 'auto',
				width: 'auto',
				gridview: true,
				rowNum:10, 
				rowList:[5,10,20,30],
				mtype: "GET", 
	            shrinkToFit: true,
	            pager: '#pagerPosition',
	            viewrecords: true,
	            sortable: true,
	            
	           	colNames:[
	           	          '<fmt:message key="LABEL_NUMBER"/>',
	           	          '<fmt:message key="LABEL_POSITION"/>',
	           	          '<fmt:message key="LABEL_POSITION_PRICE"/>',
	           	       	  '<fmt:message key="LABEL_POSITION_DESCRIPTION"/>',
	           	          ], 
	           	colModel:[ 
	           	       	   {name:'id',index:'PACKAGE_ID', width:100, hidden:true}, 
	         		  	   {name:'name',index:'NAME', width:280, sortable:true}, 
	           	           {name:'price',index:'PRICE', width:140, sortable:true}, 
	           	           {name:'description',index:'DESCRIPTION', width:100, hidden:true},
	           	        ],
	           	        
				jsonReader : {
				    root: "rows",
				    page: "page",
				    total: "total",
				    records: "records",
				    repeatitems: false,
				    cell: "cell",
				    id: "id"
				},
				
	           
	            serializeRowData: function (data) {
	            	return JSON.stringify(data);
	            },
	            onSelectRow: function(id) { 
	        	   var myGrid = $('#resultTablePosition'),
	        	   selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
	        	   positionID = myGrid.jqGrid('getCell', selRowId, 'id');
	        	   pName = myGrid.jqGrid('getCell', selRowId, 'name');
	        	   pPrice = myGrid.jqGrid('getCell', selRowId, 'price');
	        	   pDescription = myGrid.jqGrid('getCell', selRowId, 'description');

	        	   jQuery("#positionID").val(positionID);
	        	   jQuery("#pname").val(pName);
	        	   jQuery("#pprice").val(pPrice);
	        	   jQuery("#pdescription").val(pDescription);
	        	   
	        	   $('#submit').val('<fmt:message key="BUTTON_UPDATE"/>');
	        	   $('#submit').attr('name','updatePosition');
	        	   
				},
	            
	            loadComplete: function () {  
	            ModifyresultTablePositionStockDefaultStyles();                  
	            	}
			});//jqGridPosition
		}); //$(function()	
		//$("#jqGridPosition").setGridParam({datatype: 'local'}); 		
		$("#resultTablePosition").setGridParam({datatype: 'json'}).trigger("reloadGrid");
	}//jqGridPositionCreationComplete
	
 	function ModifyresultTablePositionStockDefaultStyles() {  
		 $('#' + "resultTablePosition" + ' tr').removeClass("ui-widget-content");
		 $('#' + "resultTablePosition" + ' tr:nth-child(even)').addClass("evenTableRow");
		 $('#' + "resultTablePosition" + ' tr:nth-child(odd)').addClass("oddTableRow");
	} 
</script>		
