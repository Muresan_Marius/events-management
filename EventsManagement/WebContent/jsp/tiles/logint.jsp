<%@ include file="/jsp/include.jsp" %>
	<script type="text/Javascript">
		var message = "";
		$(document).ready(
				function()
				   {
					$("#languages").change(function(){
						$("#language").val($(this).val());
						$("#language_form").submit();
					});
							
			$(":input:visible:first").focus();
			
			//verify language			
			if($("#language").val() == 'RO'){
				$('#languages option')[1].selected = true; // To select via index
			}
			else{
				$('#languages option')[0].selected = true;
			}
			
		});
		$(document).ready(function(){
			var errmsg = '${error_message}';
			errorField = new ErrorField($("#messageBox"),$(".submit_bt"));
			if (errmsg != ''){
				errorField.showError(errmsg);
				$(".login_input").removeClass("login_input").addClass("login_input_error");
			}
		});
	</script>

<div>
	<form method="post" name="language_form" id="language_form" action="login.htm">
	<input type="hidden" value="${language}" id="language" name="language"/>
	
		<table width="100%" id='login_header' cellspacing = 0>
			<tr valign="top">
				<td width="50%" align="left"> 
					<img src="./images/AncorStudio.png" alt="Ancor Studio!" width="130"/>
			 	</td>
			 	<td width="50%" align="right"> 
			 		<label for="lang"><fmt:message key="MESSAGE_SELECT_LANGUAGE"/></label>
					<select id="languages" class="roundedCornersSelect">
						<option value="EN"><fmt:message key="MESSAGE_LANGUAGE_ENGLISH"/></option>
						<option value="RO"><fmt:message key="MESSAGE_LANGUAGE_ROUMANIAN"/></option>
					</select>
			 	</td>
			</tr>
		</table>
		<br/><br/><br/>
	</form>
	
	<form method="post" name="authform" id="authform" action="login.htm">
	<input type="hidden" value="${language}" id="language_auth" name="language_auth"/>
		<div id="messageBox" class="errorField"></div>
		<table class="login_hidden" align="center">
			<tr>
				<td align="center">
					<img src="./images/Events management.jpg" height="90" width="300" alt="Events Management">
				</td>
			<tr>
				<td class="login_message">
					<b><fmt:message key="MESSAGE_GREETING"/></b>
				</td>
			</tr>

			</tr>
		</table>
		<br/><br/></br>
		<table class="login_tb" align="center">
			<tr >
				<td class="roundedCorners">
					<div id="messageWrapper">
					</div><br>
					
					<table width="360px" height="131px" align="center" cellspacing="0" cellpadding="0">
						<tr >
							<td height="33px" width="136px" align="right">
								<span style="width:100px; display:block" class="userPass" align="left"> <fmt:message key="MESSAGE_USER"/> </span>
							</td>
							<td height="33px" width="221px">
								<input class="login_input" type="text" name="username" id="username" maxlength="25" placeholder="<fmt:message key="MESSAGE_USER"/>"/>
							</td>
							
						</tr>
						
						<tr>
							<td height="61px" width="40%" align="right">
								<span style="width:100px; display:block" class="userPass" align="left"><fmt:message key="MESSAGE_PASSWORD"/> </span>
							</td>
							<td  height="61px">
								<input class="login_input" type="password" name="password" id="password" placeholder="<fmt:message key="MESSAGE_PASSWORD"/>"/>
							</td>
							
						</tr>
						<tr>
							<td height="34px" width="40%" align="right" colspan="2">
								<input class="submit_bt orange" type="submit" value="<fmt:message key="BUTTON_CONNECT"/>"/>
							</td>
						</tr>
					</table>
					<br>
				</td>
			</tr>
		</table>
	</form>
</div>
<br/>
