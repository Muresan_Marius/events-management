<%@ include file="/jsp/include.jsp" %>
<!-- begin insert_eventt.htm -->
<!--DWR-->
<script type="text/javascript" src="/EventsManagement/dwr/engine.js"></script>
<script type="text/javascript" src="/EventsManagement/dwr/util.js"></script>
<script type="text/javascript" src="/EventsManagement/dwr/interface/dwrService.js"></script>
		
<%@ include file="/javascript/insert_event.jsp" %>
<p class="clear"></p>
<script type="text/javascript">
	var tab = ${tab};
	var personId = ${id};
    function selectTab(i){
    	$(".current").removeClass('current');
    	$('.tabs li:nth-child('+i+')').addClass('current');
    	$('.panes').children().each(function(index,item){
    		if (index == parseInt(i-1)){
    			$(item).show();
    		}else
    			$(item).hide();
    	});
    }
    
    function CompleteForm(){
 		$("#last_name").val("${personExtended.lastName }");
 		$("#first_name").val("${personExtended.firstName }");
 		$("#birth_date").val("${personExtended.birthDateStr }");
 		$("#email").val("${personExtended.email }");
 		$("#phone_number").val("${personExtended.phone}");
 		$("#person_observations").val("${personExtended.generalObservations}");
 		
 		var sex = "${personExtended.sex}";
 		if (sex != ""){
 			if (sex == "m"){
 				$("#male").attr('checked',true);
 			}else{
 				$("#female").attr('checked',true);
 			}
 		}
 		$("#address").val("${personExtended.address}");
 		$("#locality").val("${personExtended.locality}");
 		$("#cnp").val("${personExtended.cnp}");
 		$("#series").val("${personExtended.series}");
 		$("#inregistration_date").val("${personExtended.inregistrationDateStr }");
 		
		
		<c:forEach items="${personExtended.events}" var="event" varStatus="status">
			var item = $("#events").jqxComboBox('getItemByValue', "${event.eventId}");
			$("#events").jqxComboBox('checkItem', item ); 
			$("#event_date_${event.eventId}").val("${event.eventDateStr}");
			$("#person_event_id_${event.eventId}").val("${event.id}");
			//document.getElementById("application_source_${position.positionId}").selectedIndex = "${position.appliedFromId-1}";
			var value = $("#application_source_${event.eventId}").jqxComboBox('getItemByValue', "${event.appliedFromId}");
			$("#application_source_${event.eventId}").jqxComboBox('selectItem',value);
		</c:forEach>

	
  		dwrService.getPersonEvents("${id}",function(events){
			$.each(events,function(index,event){
				addEvent({value: event.id, label: event.description });
				$("#location_"+ event.id).val(event.eventlocation);				
			});
		}); 					
		
    }
    
    function convertDateToString(date,separator){
    	var year = date.getFullYear(),
    		month = "" + (date.getMonth()+1), 
    		day = "" + (date.getDate()); 
    	if (month.length == 1) 
    	{ 
    		month = "0" + month; 
    	}
    	if (day.length == 1) 
    	{ 
    		day = "0" + day; 
    	}
    	return day+separator+month+separator+year;
    }
    
    function convertDateToHour(date,separator){
    	var hour = date.getHours(),
    		minutes = "" + (date.getMinutes()); 
    	if (minutes.length == 1) 
    	{ 
    		minutes = "0" + minutes; 
    	}
    	
    	return hour+separator+minutes;
    }
    
    $(document).ready(function () {
	 	document.getElementById("locality").selectedIndex="13";
        selectTab(tab);
        if(personId == -1)
        	$(".tabs > li").not('.current').hide();
	 	CompleteForm();
	 	$("#add_submit_and_continue").on('click',function(){
	 		$("#button").val(1);
	 	});
	 	
		$("#add_submit_and_new").on('click',function(){
	 		$("#button").val(2);
	 	});
    });   
</script>

<div id="messageBox" class="errorField"></div>

<!-- the tabs --> 
<ul class="tabs">
	<li><a class="current"><fmt:message key="TAB_ADD"/></a></li>
	<li><a class=""><fmt:message key="TAB_EVENT"/></a></li>
</ul>

<!-- tab "panes" -->
<div class="panes">
	<div style="display: block;" id="add_wrapper" class="panes_item">
	    <form id="add_form" name="add_form" action="insert_event.htm" method="POST" enctype="multipart/form-data">
		    <div id="column_1">
		    		<input type="hidden" name="personId" value=${id} />
		    		<span class="required">*</span>
					<label for="last_name"><fmt:message key="LABEL_LAST_NAME"/></label><br>
					<input type="text" name="last_name" id="last_name" placeholder="<fmt:message key='MESSAGE_LAST_NAME'/>" required><br><br>
					<span class="required">*</span>
					<label for="first_name"><fmt:message key="LABEL_FIRST_NAME"/></label><br>
					<input type="text" name="first_name" id="first_name" placeholder="<fmt:message key='MESSAGE_FIRST_NAME'/>" required><br><br>
					<span class="required">*</span>
					<label for="phone_number"><fmt:message key="LABEL_PHONE_NUMBER"/></label><br>
					<input type="text" name="phone_number" id="phone_number" placeholder="<fmt:message key='MESSAGE_PHONE'/>"><br><br>
					<span class="required">*</span>
					<label for="email"><fmt:message key="LABEL_EMAIL_ADDRESS"/></label><br>
					<input type="text" name="email" id="email" placeholder="<fmt:message key='MESSAGE_EMAIL'/>"><br><br>
					<span class="required">*</span>
					<label for="birth_date"><fmt:message key="LABEL_BIRTH_DATE"/>&nbsp;(dd/mm/yyyy)</label><br>
					<input type="text" name="birth_date" id="birth_date" autocomplete="off" placeholder="<fmt:message key='MESSAGE_BIRTH_DAY'/>"><br><br>
					<label for="sex"><fmt:message key="LABEL_SEX"/></label><br>
						<input type="radio" name="sex" id="male" value="m">
							<label for="male"><fmt:message key="LABEL_SEX_m"/></label>
						<input type="radio" name="sex" id="female" value="f">
							<label for="female"><fmt:message key="LABEL_SEX_f"/><br><br></label>					
					<label for="person_observations"><fmt:message key="LABEL_PERSON_OBSERVATIONS"/></label><br>
					<textarea rows="4" cols="50" id="person_observations" name="person_observations" maxlength="255" placeholder="<fmt:message key='MESSAGE_OBSERVATIONS'/>"></textarea><br><br>
					<label for="cv_upload">Contract</label><br>
					<input type="file" id="cv_upload" name="cv_upload" multiple/><br><br>
		    </div>
		    <div id="column_2">
		   		<div id="address_wrapper">
		   			<span class="required">*</span>
		   		    <label for="address"><strong><fmt:message key="LABEL_ADDRESS"/></strong></label><br>
					<input type="text" name="address" id="address" placeholder="<fmt:message key='MESSAGE_ADDRESS'/>" required><br><br>
			    </div>
		   		<div id="locality_wrapper">
		   		    <label for="locality"><strong><fmt:message key="LABEL_LOCALITY"/></strong></label><br>
					<select name="locality" id="locality">
						<option value="Alba">Alba</option>
						<option value="Arad">Arad</option>
						<option value="Argeș">Argeș</option>
						<option value="Bacău">Bacău</option>
						<option value="Bihor">Bihor</option>
						<option value="Bistrița-Năsăud">Bistrița-Năsăud</option>
						<option value="Botoșani">Botoșani</option>
						<option value="Brașov">Brașov</option>
						<option value="Brăila">Brăila</option>
						<option value="București">București</option>
						<option value="Buzău">Buzău</option>
						<option value="Caraș-Severin">Caraș-Severin</option>
						<option value="Călărași">Călărași</option>
						<option value="Cluj">Cluj</option>
						<option value="Constanța">Constanța</option>
						<option value="Covasna">Covasna</option>
						<option value="Dâmbovița">Dâmbovița</option>
						<option value="Dolj">Dolj</option>
						<option value="Galați">Galați</option>
						<option value="Giurgiu">Giurgiu</option>
						<option value="Gorj">Gorj</option>
						<option value="Harghita">Harghita</option>
						<option value="Hunedoara">Hunedoara</option>
						<option value="Ialomița">Ialomița</option>
						<option value="Iași">Iași</option>
						<option value="Ilfov">Ilfov</option>
						<option value="Maramureș">Maramureș</option>
						<option value="Mehedinți">Mehedinți</option>
						<option value="Mureș">Mureș</option>
						<option value="Neamț">Neamț</option>
						<option value="Olt">Olt</option>
						<option value="Prahova">Prahova</option>
						<option value="Satu-Mare">Satu-Mare</option>
						<option value="Sălaj">Sălaj</option>
						<option value="Sibiu">Sibiu</option>
						<option value="Suceava">Suceava</option>
						<option value="Teleorman">Teleorman</option>
						<option value="Timiș">Timiș</option>
						<option value="Tulcea">Tulcea</option>
						<option value="Vaslui">Vaslui</option>
						<option value="Vâlcea">Vâlcea</option>
						<option value="Vrancea">Vrancea</option>	
					</select>
			    </div><br>
			    
			    <span class="required">*</span>
				<label for="cnp"><fmt:message key="LABEL_CNP"/></label><br>
				<input type="text" name="cnp" id="cnp" placeholder="<fmt:message key='MESSAGE_CNP'/>" required><br><br>
				
				<span class="required">*</span>
				<label for="series"><fmt:message key="LABEL_SERIES"/></label><br>
				<input type="text" name="series" id="series" placeholder="<fmt:message key='MESSAGE_SERIES'/>" required><br><br>
				
				<span class="required">*</span>
				<label for="inregistration_date"><fmt:message key="LABEL_INREGISTRATION_DATE"/>&nbsp;(dd/mm/yyyy)</label><br>
				<input type="text" name="inregistration_date" id="inregistration_date" autocomplete="off" placeholder="<fmt:message key='MESSAGE_INREGISTRATION_DATE'/>"><br><br>
			    
		        <div id="event_wrapper" style="margin-top: 10px;">
		        	<span class="required">*</span>
			        <label for="events"><strong><fmt:message key="LABEL_EVENTS"/></strong></label><br><br>
			    	<div id='events' class="rounded"></div><br>
			    	<input type="hidden" name="event_ids" id="event_ids" />
					<div id="event_list" name="event_list"></div>
			    </div><br><br>
		    </div>
	    <input type="hidden" id="add_JSON" name="add_JSON" />
	    <input type="hidden" id="button" name="button" />
	    <p class="clear"></p>
	    <div id="add_submit_buttons">
		    <input type="submit" id="add_submit_and_continue" class="submit_bt orange_wide" name="add_submit_and_continue" value="<fmt:message key='LABEL_SUBMIT_AND_CONTINUE'/>">&nbsp;&nbsp;&nbsp;
		    <c:if test="${id==-1}">
		    <input type="submit" id="add_submit_and_new" class="submit_bt orange_wide" name="add_submit_and_new" value="<fmt:message key='LABEL_SUBMIT_AND_NEW'/>">
		    </c:if>
		</div>
	</form>
    </div>
    
    <div style="display: none;" id="selection_wrapper" class="panes_item">
   		<form id="selection_form" name="selection_form" action="insert_event.htm" method="POST">
   			<input type="hidden" name="action" value="update_selections" />
   			<input type="hidden" id="selections_json" name="selections_json" />
   			<input type="hidden" name="personId" value=${id} />
   			<input type="hidden" name="tab" value=2 />
	    	<div id='selections'></div><br>
			<input type="submit" id="selection_submit" class="submit_bt orange" name="selection_submit" value="<fmt:message key='LABEL_SUBMIT'/>">
		</form>
    </div>
    
<!-- end insert_eventt.htm -->